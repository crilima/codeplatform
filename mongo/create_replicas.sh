#!/bin/bash

NB_REPL=$1

[ ${NB_REPL} -gt 7 ] && echo -e "\033[0;33mWARNING\033[0m: The maximum \
number of nodes is \033[0;36m7\033[0m" && NB_REPL=7

REP_NAME=${REPLICA_NAME:-replica}
DB_NAME=${DB_NAME:-database}
CONTAINER_ID="`basename $(cat /proc/1/cpuset)`"
HOST_VOLUME_ROOT=`echo $(docker inspect ${CONTAINER_ID} | grep ${VOLUME_ROOT}: | cut -d ":" -f1 | tr '"' ' ' 2>/dev/null)`
HOST_DATA_VOLUME=""
HOST_LOGS_VOLUME=""

if [ ! -z "${HOST_VOLUME_ROOT}" ] ; then
    HOST_DATA_VOLUME="${HOST_VOLUME_ROOT}/$(basename ${DATA_VOLUME})"
    HOST_LOGS_VOLUME="${HOST_VOLUME_ROOT}/$(basename ${LOGS_VOLUME})"
fi

DATA=${HOST_DATA_VOLUME:-/volumes/data}
LOGS=${HOST_LOGS_VOLUME:-/volumes/logs}
COMPOSE_VERSION=${COMPOSE_VERSION:-2}

DOCKERFILE_NAME=${DOCKERFILE_NAME:-Dockerfile}
use_host=${USE_HOST:-true}

mkdir -p ${DATA_VOLUME} ${LOGS_VOLUME}/${REP_NAME}
chmod -R 777 ${DATA_VOLUME} ${LOGS_VOLUME}

compose_file=docker-compose.yml
template=docker-compose.template

start_port=${START_PORT:-27040}

compose_content="version: '${COMPOSE_VERSION}'
services:
"

for ((i=0;i<${NB_REPL};i++)) ; do
    mkdir -p ${DATA_VOLUME}/${REP_NAME}_${i}
    chmod 777 ${DATA_VOLUME}/${REP_NAME}_${i}
    touch ${LOGS_VOLUME}/${REP_NAME}/${DB_NAME}_${i}.log
    chmod 777 ${LOGS_VOLUME}/${REP_NAME}/${DB_NAME}_${i}.log

    port=$[start_port+i]
    content="$(cat ${template})"
    content="$(echo "${content}" | sed 's/<DOCKERFILE>/'${DOCKERFILE_NAME}'/g')"
    content="$(echo "${content}" | sed 's/<NUMBER>/'${i}'/g')"
    content="$(echo "${content}" | sed 's/<MONGO_PORT>/'${port}'/g')"
    content="$(echo "${content}" | sed 's/<HTTP_PORT>/'$[port+1000]'/g')"
    content="$(echo "${content}" | sed 's/<REPLICA>/'${REP_NAME}'/g')"
    content="$(echo "${content}" | sed 's/<DB_NAME>/'${DB_NAME}'/g')"
    content="$(echo "${content}" | sed 's!<DATA_VOLUME>!'${DATA}'!g')"
    content="$(echo "${content}" | sed 's!<LOGS_VOLUME>!'${LOGS}'!g')"

    if ${use_host} ; then
        content="${content}
        network_mode: \"host\"
        extra_hosts:
            - \"${HOSTNAME}:127.0.0.1\"
"
    fi

    if [ ${i} -eq 0 ] ; then
        commands=""
        for nb in $(seq $[NB_REPL-1]) ; do
            if ${use_host} ; then
                commands="${commands} \"${HOSTNAME}:$[port+nb]\","
            else
                commands="${commands} \"${REP_NAME}_${nb}\","
            fi
        done
        commands="$(echo "${commands}" | sed 's/,$/ /')"
        content="$(echo "${content}" | sed 's/# command: .*$/command: ['"${commands}"']/')"
    fi

    compose_content="${compose_content}
${content}"
done


if ! ${use_host} ; then
    network_name="mongodb-${REP_NAME}-network"
    compose_content="${compose_content}
networks:
    default:
        external:
            name: ${network_name}
"
fi

if ! docker network ls | grep -qe " ${network_name} " ; then
    echo "Create network '${network_name}'"
    docker network create ${network_name}
fi

echo "${compose_content}" > ${compose_file}

echo "Build replica image..."
docker-compose build 2>&1 1>/dev/null
echo "Start replicas..."
docker-compose up &

pid="$!"
trap "echo 'Stopping replicas'; kill -SIGTERM ${pid}" SIGINT SIGTERM

while kill -0 ${pid} > /dev/null 2>&1; do
    wait
done

docker-compose stop
exit 0
