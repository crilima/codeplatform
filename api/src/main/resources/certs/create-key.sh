#/bin/bash

export PASSWORD="${PASSWORD:-password}"
export SERVER_NAME="${SERVER_NAME:-$(hostname)}"

echo "Creating certificates for ${SERVER_NAME}"

rm -f ca-key.pem ca.pem server-cert.pem server-key.pem ca.srl cert.pem key.pem server.key \
  server.crt ca.crt keystore.p12

## Server CERT
echo "Create CA private and public..."
openssl genrsa -aes256 -passout env:PASSWORD -out ca-key.pem 4096
openssl req -subj "/CN=${SERVER_NAME}/C=FR/ST=France/L=Nantes/O=Capgemini/OU=FSSBU" \
  -new -passin env:PASSWORD -x509 -days 3650 -key ca-key.pem -sha256 -out ca.pem
echo "Create Server key..."
openssl genrsa -passout env:PASSWORD -out server-key.pem 4096
openssl req -subj "/CN=${SERVER_NAME}" -sha256 -new -passin env:PASSWORD -key server-key.pem \
  -out server.csr
echo "subjectAltName = DNS:${SERVER_NAME},IP:192.168.56.103,IP:127.0.0.1" > extfile.cnf
echo "Sign key..."
openssl x509 -req -passin env:PASSWORD -days 3650 -sha256 -in server.csr -CA ca.pem -CAkey \
  ca-key.pem -CAcreateserial -out server-cert.pem -extfile extfile.cnf
rm -f extfile.cnf

# JRE
echo "Create PKCS12 Keystore..."
openssl pkcs12 -export -in server-cert.pem -inkey server-key.pem -certfile server-cert.pem \
  -out openssl.p12 -passout env:PASSWORD
echo "Create JRE PKCS12 Keystore..."
keytool -importkeystore -srcalias 1  -srckeystore openssl.p12 -srckeypass "${PASSWORD}" \
  -srcstorepass "${PASSWORD}" -destkeystore keystore.p12 -destalias "${SERVER_NAME}" \
  -deststoretype pkcs12 -deststorepass "${PASSWORD}"
rm -f openssl.p12

## Client CERT
echo "Create Client key..."
openssl genrsa -passout env:PASSWORD -out key.pem 4096
openssl req -subj '/CN=client' -new  -passin env:PASSWORD -key key.pem -out client.csr
echo "extendedKeyUsage = clientAuth" > extfile.cnf
echo "Sign key..."
openssl x509 -req -passin env:PASSWORD -days 3650 -sha256 -in client.csr -CA ca.pem -CAkey \
  ca-key.pem -CAcreateserial -out cert.pem -extfile extfile.cnf
rm -f extfile.cnf

rm -f server.csr client.csr
chmod -v 0400 ca-key.pem key.pem server-key.pem
chmod -v 0444 ca.pem server-cert.pem cert.pem
cp -rp server-key.pem server.key
cp -rp server-cert.pem server.crt
cp -rp ca.pem ca.crt

exit 0
