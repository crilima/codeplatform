package fr.nemolovich.apps.codeplatform.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import fr.nemolovich.apps.codeplatform.api.model.enums.Language;
import fr.nemolovich.apps.codeplatform.api.model.enums.SubmissionType;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;
import org.springframework.validation.annotation.Validated;

/**
 * SubmissionRequest
 */
@Validated
@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.SpringCodegen",
    date = "2017-10-15T01:00:32.677+02:00")
public class SubmissionRequest {

    @JsonProperty("challenge")
    private String challenge = null;

    @JsonProperty("team")
    private String team = null;

    @JsonProperty("exercice")
    private String exercice = null;

    /**
     * Gets or Sets language
     */
    @JsonProperty("language")
    private Language language = null;

    @JsonProperty("type")
    private SubmissionType type = null;

    public SubmissionRequest challenge(String challenge) {
        this.challenge = challenge;
        return this;
    }

    /**
     * Get challenge
     *
     * @return challenge
     *
     */
    @ApiModelProperty(value = "")
    public String getChallenge() {
        return challenge;
    }

    public void setChallenge(String challenge) {
        this.challenge = challenge;
    }

    public SubmissionRequest team(String team) {
        this.team = team;
        return this;
    }

    /**
     * Get team
     *
     * @return team
     *
     */
    @ApiModelProperty(value = "")
    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public SubmissionRequest exercice(String exercice) {
        this.exercice = exercice;
        return this;
    }

    /**
     * Get exercice
     *
     * @return exercice
     *
     */
    @ApiModelProperty(value = "")
    public String getExercice() {
        return exercice;
    }

    public void setExercice(String exercice) {
        this.exercice = exercice;
    }

    public SubmissionRequest language(Language language) {
        this.language = language;
        return this;
    }

    /**
     * Get language
     *
     * @return language
     *
     */
    @ApiModelProperty(value = "")
    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    /**
     * Get submission type
     *
     * @return submission type
     *
     */
    @ApiModelProperty(value = "")
    public SubmissionType getType() {
        return type;
    }

    public void setType(SubmissionType type) {
        this.type = type;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SubmissionRequest submissionRequest = (SubmissionRequest) o;
        return Objects.equals(this.challenge, submissionRequest.challenge)
            && Objects.equals(this.team, submissionRequest.team)
            && Objects.equals(this.exercice, submissionRequest.exercice)
            && Objects.equals(this.language, submissionRequest.language)
            && Objects.equals(this.type, submissionRequest.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(challenge, team, exercice, language, type);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class SubmissionRequest {\n");

        sb.append("    challenge: ").append(toIndentedString(challenge))
            .append("\n");
        sb.append("    team: ").append(toIndentedString(team)).append("\n");
        sb.append("    exercice: ").append(toIndentedString(exercice))
            .append("\n");
        sb.append("    language: ").append(toIndentedString(language))
            .append("\n");
        sb.append("    type: ").append(toIndentedString(type)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     * 
     * @param o {@link Object}: Object to format.
     * @return {@link String}: String representation of object.
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
