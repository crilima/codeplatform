package fr.nemolovich.apps.codeplatform.api.webapi;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.SpringCodegen",
    date = "2017-10-01T18:58:34.924+02:00")
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ApiOriginFilter extends OncePerRequestFilter {

    private static final List<String> METHODS
        = Arrays.asList("GET", "POST", "DELETE", "PUT", "OPTIONS");
    private static final List<String> ALLOW_HEADERS = Arrays.asList(
        "Origin", "Accept", "X-Requested-With", "Content-Type",
        "Access-Control-Request-Method", "Access-Control-Request-Headers",
        "Authorization");
    private static final List<String> EXPOSE_HEADERS = Arrays.asList(
        "Access-Control-Allow-Origin", "Access-Control-Allow-Credentials",
        "Authorization");

    @Override
    public void doFilterInternal(HttpServletRequest request,
        HttpServletResponse response, FilterChain filterChain)
        throws IOException, ServletException {

        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods",
            METHODS.stream().collect(Collectors.joining(", ")));
        response.addHeader("Access-Control-Allow-Headers",
            ALLOW_HEADERS.stream().collect(Collectors.joining(", ")));
        response.addHeader("Access-Control-Expose-Headers",
            EXPOSE_HEADERS.stream().collect(Collectors.joining(", ")));
        response.addHeader("Access-Control-Allow-Credentials", "true");
        response.addIntHeader("Access-Control-Max-Age", 3600);
        filterChain.doFilter(request, response);
    }

}
