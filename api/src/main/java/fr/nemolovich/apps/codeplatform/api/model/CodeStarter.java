package fr.nemolovich.apps.codeplatform.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import fr.nemolovich.apps.codeplatform.api.model.enums.Language;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Submission
 */
@Document(collection = "codeplatform_codestarter")
public class CodeStarter {

    @JsonProperty("id")
    @Size(min = 0, max = 50)
    @Id
    private String id = null;

    @JsonProperty("language")
    @NotNull
    private Language language = null;

    @JsonProperty("content")
    @NotNull
    private String content = null;

    public CodeStarter id(String id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     *
     */
    @ApiModelProperty(value = "")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public CodeStarter language(Language language) {
        this.language = language;
        return this;
    }

    /**
     * Get language
     *
     * @return language
     *
     */
    @ApiModelProperty(value = "")
    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public CodeStarter content(String content) {
        this.content = content;
        return this;
    }

    /**
     * Get content
     *
     * @return content
     *
     */
    @ApiModelProperty(value = "")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CodeStarter codeStarter = (CodeStarter) o;
        return Objects.equals(this.language, codeStarter.language)
            && Objects.equals(this.content, codeStarter.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(language, content);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class CodeStarter {\n");
        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    language: ").append(toIndentedString(language))
            .append("\n");
        sb.append("    content: ").append(toIndentedString(content))
            .append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     * 
     * @param o {@link Object}: Object to format.
     * @return {@link String}: String representation of object.
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
