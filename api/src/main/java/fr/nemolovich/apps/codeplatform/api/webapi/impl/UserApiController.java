package fr.nemolovich.apps.codeplatform.api.webapi.impl;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.nemolovich.apps.codeplatform.api.model.Role;
import fr.nemolovich.apps.codeplatform.api.model.User;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.UserRegisterModel;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.UserViewModel;
import fr.nemolovich.apps.codeplatform.api.security.SecurityUtils;
import fr.nemolovich.apps.codeplatform.api.service.UserService;
import fr.nemolovich.apps.codeplatform.api.webapi.UserApi;
import fr.nemolovich.apps.codeplatform.api.webapi.exception.NotFoundException;
import io.swagger.annotations.ApiParam;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Ionut on 08/10/2017.
 */
@Controller
public class UserApiController implements UserApi {

    private final ObjectMapper objectMapper;
    private final UserService userService;

    private static final Logger LOGGER = LoggerFactory.getLogger(
        UserApiController.class);

    public UserApiController(ObjectMapper objectMapper,
        UserService userService) {
        this.objectMapper = objectMapper;
        this.userService = userService;
    }

    //request /api/admin/users
    @Override
    public ResponseEntity<UserViewModel> addUser(@ApiParam(
        value = "The user to create", required = true)
        @Valid @RequestBody User user,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        if (accept != null && accept.contains("application/json")
            && user.getId() == null) {
            User eUser = this.userService.getUserByLogin(user.getLogin());
            if (eUser != null) {
                return new ResponseEntity<>(new UserViewModel(
                    eUser), HttpStatus.NOT_MODIFIED);
            }
            return ResponseEntity.ok(new UserViewModel(
                userService.createUser(user)));
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<Object> deleteUser(
        @ApiParam(value = "login", required = true)
        @PathVariable("login") String login,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        if (accept != null && accept.contains("application/json")) {
            LOGGER.debug("REST request to delete User: {}", login);
            try {
                userService.deleteUser(login);
            } catch (NotFoundException ne) {
                return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
            }
            return ResponseEntity.ok(null);
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<UserViewModel> getAccount(
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("REST request to getAccount: {}",
            SecurityUtils.getCurrentUserLogin());
        if (accept != null && accept.contains("application/json")) {
            return ResponseEntity.ok(userService.getUserViewModelByLogin(
                SecurityUtils.getCurrentUserLogin()));
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<Set<UserViewModel>> getAllUsers(
        @ApiParam(value = "Page number of the requested page")
        @Valid @RequestParam(value = "page", required = false) Integer page,
        @ApiParam(value = "Size of a page")
        @Valid @RequestParam(value = "size", required = false) Integer size,
        @ApiParam(
            value = "Sorting criteria in the format: property(,asc|desc). "
            + "Default sort order is ascending. Multiple sort criteria "
            + "are supported.") @Valid @RequestParam(value = "sort",
            required = false) List<String> sort,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("REST request to getAllUsers with login: {}",
            SecurityUtils.getCurrentUserLogin());
        if (accept != null && accept.contains("application/json")) {
            return ResponseEntity.ok(new HashSet<>(
                userService.getAllUserViewModels()));
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<Set<Role>> getRoles(
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("REST request to getRoles with login: {}",
            SecurityUtils.getCurrentUserLogin());
        if (accept != null && accept.contains("application/json")) {
            return ResponseEntity.ok(userService.getRoles(
                SecurityUtils.getCurrentUserLogin()));
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<UserViewModel> getUserByID(
        @ApiParam(value = "login", required = true)
        @PathVariable("login") String login,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("REST request to getUserByID with login: {}", login);
        if (accept != null && accept.contains("application/json")) {
            UserViewModel userViewModel = userService.getUserViewModelByLogin(
                login);
            if (userViewModel != null) {
                return ResponseEntity.ok(userViewModel);
            }
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<UserViewModel> registerAccount(
        @ApiParam(value = "The user request to register", required = true)
        @Valid @RequestBody UserRegisterModel userRegisterModel,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("REST request to registerAccount with userRegisterModel: {}",
            userRegisterModel);
        if (accept != null && accept.contains("application/json")) {
            return ResponseEntity.ok(new UserViewModel(
                userService.createUser(userRegisterModel)));
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<UserViewModel> changePassword(
        @ApiParam(value = "The user to upate", required = true)
        @Valid @RequestBody UserRegisterModel userRegisterModel,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("REST request to changePassword with userViewModel: {}",
            userRegisterModel);
        if (accept != null && accept.contains("application/json")) {
            User user = userService.changePassword(userRegisterModel.getPassword());
            return ResponseEntity.ok(user != null ? new UserViewModel(user)
                : null);
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<UserViewModel> updateAccount(
        @ApiParam(value = "UserViewModel", required = true)
        @Valid @RequestBody UserViewModel userViewModel,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("REST request to updateAccount with userViewModel: {}",
            userViewModel);
        if (accept != null && accept.contains("application/json")) {
            userService.updateUser(userViewModel);
            return ResponseEntity.ok(userViewModel);
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<User> updateUser(
        @ApiParam(value = "User", required = true)
        @Valid @RequestBody User user,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("REST request to updateUser with user: {}", user);
        if (accept != null && accept.contains("application/json")) {
            userService.updateUser(user);
            return ResponseEntity.ok(user);
        }
        return ResponseEntity.badRequest().body(null);
    }
}
