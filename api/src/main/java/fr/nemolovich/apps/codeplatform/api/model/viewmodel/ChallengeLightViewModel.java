package fr.nemolovich.apps.codeplatform.api.model.viewmodel;

import com.fasterxml.jackson.annotation.JsonProperty;

import fr.nemolovich.apps.codeplatform.api.model.Challenge;
import fr.nemolovich.apps.codeplatform.api.model.enums.ChallengeScope;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;

/**
 *
 * @author bgohier
 */
@Validated
@javax.annotation.Generated(
    value = "bgohier",
    date = "2017-10-13T16:19:22.000+02:00")
public class ChallengeLightViewModel {

    @JsonProperty("id")
    private final String id;

    @JsonProperty("name")
    @NotNull
    private final String name;

    @JsonProperty("description")
    private final String description;

    @JsonProperty("scope")
    private final ChallengeScope scope;

    @JsonProperty("startDate")
    private final Instant startDate;

    @JsonProperty("endDate")
    private final Instant endDate;

    @JsonProperty("teams")
    private final int teams;

    @JsonProperty("exercices")
    private final int exercices;

    @JsonProperty("results")
    private final int results;

    @JsonProperty("score")
    private BigDecimal score;

    public ChallengeLightViewModel(Challenge challenge) {
        this.id = challenge.getId();
        this.name = challenge.getName();
        this.description = challenge.getDescription();
        this.scope = challenge.getScope();
        this.startDate = challenge.getStartDate();
        this.endDate = challenge.getEndDate();
        this.teams = challenge.getTeams().size();
        this.exercices = challenge.getExercices().size();
        this.results = challenge.getResults().size();
    }

    /**
     * Get id
     *
     * @return id
     *
     */
    @ApiModelProperty(value = "")
    public String getId() {
        return id;
    }

    /**
     * Get name
     *
     * @return name
     *
     */
    @ApiModelProperty(value = "")
    public String getName() {
        return name;
    }

    /**
     * Get description
     *
     * @return description
     *
     */
    @ApiModelProperty(value = "")
    public String getDescription() {
        return description;
    }

    /**
     * Get scope
     *
     * @return scope
     *
     */
    @ApiModelProperty(value = "")
    public ChallengeScope getScope() {
        return scope;
    }

    /**
     * Get startDate
     *
     * @return startDate
     *
     */
    @ApiModelProperty(value = "")
    public Instant getStartDate() {
        return startDate;
    }

    /**
     * Get endDate
     *
     * @return endDate
     *
     */
    @ApiModelProperty(value = "")
    public Instant getEndDate() {
        return endDate;
    }

    /**
     * Get score
     *
     * @return score
     *
     */
    @ApiModelProperty(value = "")
    public BigDecimal getScore() {
        return score;
    }

    /**
     * Set score
     *
     * @param score score to set
     *
     */
    public void setScore(BigDecimal score) {
        this.score = score;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ChallengeLightViewModel challenge = (ChallengeLightViewModel) o;
        return Objects.equals(this.id, challenge.id)
            && Objects.equals(this.name, challenge.name)
            && Objects.equals(this.description, challenge.description)
            && Objects.equals(this.scope, challenge.scope)
            && Objects.equals(this.startDate, challenge.startDate)
            && Objects.equals(this.endDate, challenge.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, scope, startDate, endDate,
            score);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Challenge {\n");
        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    description: ").append(toIndentedString(description))
            .append("\n");
        sb.append("    scope: ").append(toIndentedString(scope)).append("\n");
        sb.append("    startDate: ").append(toIndentedString(startDate))
            .append("\n");
        sb.append("    endDate: ").append(toIndentedString(endDate))
            .append("\n");
        sb.append("    score: ").append(toIndentedString(score)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     * 
     * @param o {@link Object}: Object to format.
     * @return {@link String}: String representation of object.
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
