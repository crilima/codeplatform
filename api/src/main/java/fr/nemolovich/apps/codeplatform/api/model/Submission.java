package fr.nemolovich.apps.codeplatform.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import fr.nemolovich.apps.codeplatform.api.model.enums.Language;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Submission
 */
@Document(collection = "codeplatform_submission")
public class Submission {

    @JsonProperty("id")
    @Size(min = 0, max = 50)
    @Id
    private String id = null;

    @JsonProperty("files")
    @DBRef
    private Set<CodeFile> files = new HashSet<>();

    @JsonProperty("user")
    @NotNull
    @DBRef
    private User user = null;

    @JsonProperty("language")
    @NotNull
    private Language language = null;

    @JsonProperty("date")
    @NotNull
    private Instant date = null;

    @JsonProperty("score")
    @NotNull
    private BigDecimal score = null;

    public Submission id(String id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     *
     */
    @ApiModelProperty(value = "")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Submission date(Instant date) {
        this.date = date;
        return this;
    }

    /**
     * Get date
     *
     * @return date
     *
     */
    @ApiModelProperty(value = "")
    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Submission score(BigDecimal score) {
        this.score = score;
        return this;
    }

    /**
     * Get score
     *
     * @return score
     *
     */
    @ApiModelProperty(value = "")
    @Valid
    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    public Submission files(Set<CodeFile> files) {
        this.files = files;
        return this;
    }

    public Submission addFileItem(CodeFile codeFile) {
        if (this.files == null) {
            this.files = new HashSet<>();
        }
        this.files.add(codeFile);
        return this;
    }

    public Submission removeFileItem(CodeFile codeFile) {
        if (this.files.contains(codeFile)) {
            this.files.remove(codeFile);
        }
        return this;
    }

    /**
     * Get files
     *
     * @return files
     *
     */
    @ApiModelProperty(value = "")
    public Set<CodeFile> getFiles() {
        return files;
    }

    public void setFiles(Set<CodeFile> files) {
        this.files = files;
    }

    public Submission user(User user) {
        this.user = user;
        return this;
    }

    /**
     * Get user
     *
     * @return user
     *
     */
    @ApiModelProperty(value = "")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Submission language(Language language) {
        this.language = language;
        return this;
    }

    /**
     * Get language
     *
     * @return language
     *
     */
    @ApiModelProperty(value = "")
    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Submission submission = (Submission) o;
        return Objects.equals(this.user, submission.user)
            && Objects.equals(this.score, submission.score)
            && Objects.equals(this.language, submission.language);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, score, language);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Submission {\n");
        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    date: ").append(toIndentedString(date)).append("\n");
        sb.append("    files: ").append(toIndentedString(files)).append("\n");
        sb.append("    user: ").append(toIndentedString(user)).append("\n");
        sb.append("    score: ").append(toIndentedString(score)).append("\n");
        sb.append("    language: ").append(toIndentedString(language))
            .append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     * 
     * @param o {@link Object}: Object to format.
     * @return {@link String}: String representation of object.
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
