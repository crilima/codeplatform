package fr.nemolovich.apps.codeplatform.api.service;

import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import fr.nemolovich.apps.codeplatform.api.model.Team;
import fr.nemolovich.apps.codeplatform.api.model.User;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.TeamViewModel;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.UserViewModel;
import fr.nemolovich.apps.codeplatform.api.repository.TeamRepository;

/**
 * Service class for managing team.
 */
@Service
public class TeamService {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        TeamService.class);

    private final UserService userService;

    private final TeamRepository teamRepository;

    public TeamService(UserService userService, TeamRepository teamRepository) {
        this.userService = userService;
        this.teamRepository = teamRepository;
    }

    public Team getTeamById(String teamId) {
        Team team = teamRepository.findOne(teamId);
        if (team != null) {
            LOGGER.debug("Team found with teamid [{}] - {}", teamId, team);
        } else {
            LOGGER.warn("Team is null, verify teamid [{}] ", teamId);
        }
        return team;
    }

    public Team getTeamByName(String teamName) {
        Team team = teamRepository.findOneByName(teamName).orElse(null);
        if (team != null) {
            LOGGER.debug("Team found with teamname [{}] - {}", teamName, team);
        } else {
            LOGGER.warn("Team is null, verify teamname [{}] ", teamName);
        }
        return team;
    }

    public Team addTeamByTeamViewModel(TeamViewModel teamViewModel) {
        if (teamViewModel.getId() != null && getTeamById(
            teamViewModel.getId()) != null) {
            return null;
        }
        Team team = new Team();
        return mapAndSaveTeamFromViewModel(teamViewModel, team);
    }

    /**
     *
     * Maps a teamViewModel to a team, new or existing
     *
     * @param teamViewModel {@link TeamViewModel}: The team model.
     * @param team {@link Team}: The team to link.
     * @return {@link Team}: The team linked.
     */
    private Team mapAndSaveTeamFromViewModel(TeamViewModel teamViewModel,
        Team team) {
        team.setName(teamViewModel.getName());
        // Creator
        String loginCreator = teamViewModel.getCreator().getLogin();
        team.setCreator(userService.getUserByLogin(loginCreator));
        // Members
        team.setMembers(teamViewModel.getMembers().stream()
            .map(u -> userService.getUserByLogin(u.getLogin()))
            .collect(Collectors.toSet()));
        team = teamRepository.save(team);
        return team;
    }

    /**
     * Persist a team.
     *
     * @param team {@link Team}: The team to save
     * @return {@link Team} - The persited team.
     */
    public Team saveTeam(Team team) {
        return teamRepository.save(team);
    }

    public Boolean isMemberOfTeamByIdAndUsername(String teamId, String username) {
        Team team = teamRepository.findOne(teamId);
        User user = userService.getUserByLogin(username);
        return isMemberOfTeam(team, user);
    }

    public Boolean isMemberOfTeamViewModel(TeamViewModel teamViewModel, String username) {
        if (teamViewModel != null && username != null && teamViewModel.getId() != null) {
            return isMemberOfTeamByIdAndUsername(teamViewModel.getId(), username);
        }
        return false;
    }

    public Boolean isMemberOfTeam(Team team, User user) {
        if (team != null && user != null) {
            if (team.getCreator().getId().equalsIgnoreCase(user.getId())) {
                LOGGER.debug("User username:{} is creator of Team name:{}", user.getLogin(), team.getName());
                return true;
            } else if (team.getMembers().stream().anyMatch(u -> u.getId().equalsIgnoreCase(user.getId()))) {
                LOGGER.debug("User username:{} is a member of Team name:{}", user.getLogin(), team.getName());
                return true;
            } else {
                LOGGER.debug("User username:{} is not a member of Team name:{}", user.getLogin(), team.getName());
                return false;
            }
        }
        return false;
    }

    public Team updateTeamByViewModel(TeamViewModel teamViewModel) {
        Team team = teamRepository.findOne(teamViewModel.getId());
        // @TODO: test if method works.
        return mapAndSaveTeamFromViewModel(teamViewModel, team);
    }

    public Team removeUserFromTeam(String teamId, UserViewModel userViewModel) {
        Team team = teamRepository.findOne(teamId);
        User user = userService.getUserByLogin(userViewModel.getLogin());
        if (user.getId().equalsIgnoreCase(team.getCreator().getId())) {
            // Remove the user from team
            if (team.getMembers().remove(user)) {
                if (team.getMembers().isEmpty()) {
                    // Reset the name of the team to be reused if needed
                    team.setName(user.getLogin());
                } else {
                    // If the team is now empty then get the first member to
                    // assign him as creator/owner
                    User newCreator = team.getMembers().stream().findFirst()
                        .orElse(null);
                    if (newCreator != null) {
                        team.setCreator(newCreator);
                    }
                }
                teamRepository.save(team);
            }
        } else {
            LOGGER.info("User id:{} need to leave team id:{}", user.getId(), team.getId());
            // @TODO verify if the remove works
            // user is remove if it's inside the team
            if (team.getMembers().remove(user)) {
                teamRepository.save(team);
            }
        }
        return team;
    }
}
