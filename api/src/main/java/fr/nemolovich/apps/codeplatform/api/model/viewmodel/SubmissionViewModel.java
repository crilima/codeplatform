package fr.nemolovich.apps.codeplatform.api.model.viewmodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.nemolovich.apps.codeplatform.api.model.CodeFile;
import fr.nemolovich.apps.codeplatform.api.model.Submission;
import fr.nemolovich.apps.codeplatform.api.model.enums.Language;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.Valid;

/**
 * SubmissionViewModel
 */
public class SubmissionViewModel {

    @JsonProperty("id")
    private String id = null;

    @JsonProperty("files")
    private Set<CodeFile> files = new HashSet<>();

    @JsonProperty("user")
    private UserViewModel user = null;

    @JsonProperty("language")
    private Language language = null;

    @JsonProperty("date")
    private Instant date = null;

    @JsonProperty("score")
    private BigDecimal score = null;

    public SubmissionViewModel date(Instant date) {
        this.date = date;
        return this;
    }

    public SubmissionViewModel(Submission submission) {
        this.id = submission.getId();
        this.files = submission.getFiles();
        this.user = new UserViewModel(submission.getUser());
        this.language = submission.getLanguage();
        this.date = submission.getDate();
        this.score = submission.getScore();
    }

    /**
     * Get date
     *
     * @return date
     *
     */
    @ApiModelProperty(value = "")
    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public SubmissionViewModel score(BigDecimal score) {
        this.score = score;
        return this;
    }

    /**
     * Get score
     *
     * @return score
     *
     */
    @ApiModelProperty(value = "")
    @Valid
    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    public SubmissionViewModel files(Set<CodeFile> files) {
        this.files = files;
        return this;
    }

    public SubmissionViewModel addFileItem(CodeFile codeFile) {
        if (this.files == null) {
            this.files = new HashSet<>();
        }
        this.files.add(codeFile);
        return this;
    }

    /**
     * Get files
     *
     * @return files
     *
     */
    @ApiModelProperty(value = "")
    public Set<CodeFile> getFiles() {
        return files;
    }

    public void setFiles(Set<CodeFile> files) {
        this.files = files;
    }

    public SubmissionViewModel user(UserViewModel user) {
        this.user = user;
        return this;
    }

    /**
     * Get user
     *
     * @return user
     *
     */
    @ApiModelProperty(value = "")
    public UserViewModel getUser() {
        return user;
    }

    public void setUser(UserViewModel user) {
        this.user = user;
    }

    public SubmissionViewModel language(Language language) {
        this.language = language;
        return this;
    }

    /**
     * Get language
     *
     * @return language
     *
     */
    @ApiModelProperty(value = "")
    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SubmissionViewModel submission = (SubmissionViewModel) o;
        return Objects.equals(this.files, submission.files)
            && Objects.equals(this.date, submission.date)
            && Objects.equals(this.user, submission.user)
            && Objects.equals(this.score, submission.score)
            && Objects.equals(this.language, submission.language);
    }

    @Override
    public int hashCode() {
        return Objects.hash(files, user, language);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Submission {\n");
        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    date: ").append(toIndentedString(date)).append("\n");
        sb.append("    files: ").append(toIndentedString(files)).append("\n");
        sb.append("    user: ").append(toIndentedString(user)).append("\n");
        sb.append("    score: ").append(toIndentedString(score)).append("\n");
        sb.append("    language: ").append(toIndentedString(language))
            .append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     * 
     * @param o {@link Object}: Object to format.
     * @return {@link String}: String representation of object.
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
