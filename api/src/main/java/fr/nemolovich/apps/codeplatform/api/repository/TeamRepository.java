package fr.nemolovich.apps.codeplatform.api.repository;

import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import fr.nemolovich.apps.codeplatform.api.model.Team;

/**
 * Team Repository
 */
@Repository
public interface TeamRepository extends MongoRepository<Team, String> {

    Optional<Team> findOneByName(String name);
}
