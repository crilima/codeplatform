package fr.nemolovich.apps.codeplatform.api.configuration.dbsetup;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;

import fr.nemolovich.apps.codeplatform.api.model.CodeFile;
import fr.nemolovich.apps.codeplatform.api.model.Exercice;
import fr.nemolovich.apps.codeplatform.api.model.Result;
import fr.nemolovich.apps.codeplatform.api.model.Submission;
import fr.nemolovich.apps.codeplatform.api.model.Team;
import fr.nemolovich.apps.codeplatform.api.model.User;
import fr.nemolovich.apps.codeplatform.api.model.enums.Language;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;

/**
 * Created by awattez on 10/10/2017.
 */
/**
 * Creates the initial database setup for user
 */
@ChangeLog(order = "004")
@Profile(value = "dev")
public class InitialSetupResult {

    @ChangeSet(order = "01", author = "initiator", id = "01-addResultss")
    public void addResults(MongoTemplate mongoTemplate) {

        Exercice exercice1 = mongoTemplate.findOne(new BasicQuery(
            "{ title : 'First simple test' }"), Exercice.class);
        Exercice exercice2 = mongoTemplate.findOne(new BasicQuery(
            "{ title : 'Title exercice2' }"), Exercice.class);
        Exercice exercice3 = mongoTemplate.findOne(new BasicQuery(
            "{ title : 'Title exercice3' }"), Exercice.class);

        Team team0 = mongoTemplate.findOne(new BasicQuery(
            "{ id : 'team-0' }"), Team.class);
        Team team1 = mongoTemplate.findOne(new BasicQuery(
            "{ id : 'team-1' }"), Team.class);
        Team team2 = mongoTemplate.findOne(new BasicQuery(
            "{ id : 'team-2' }"), Team.class);
        Team team3 = mongoTemplate.findOne(new BasicQuery(
            "{ id : 'team-3' }"), Team.class);

        User userS = mongoTemplate.findOne(new BasicQuery(
            "{ id : 'user-id-system' }"), User.class);
        User userA = mongoTemplate.findOne(new BasicQuery(
            "{ id : 'user-id-admin' }"), User.class);
        User user2 = mongoTemplate.findOne(new BasicQuery(
            "{ id : 'user-id-2' }"), User.class);
        User user3 = mongoTemplate.findOne(new BasicQuery(
            "{ id : 'user-id-3' }"), User.class);

        Map<Team, User> teamsUser = new HashMap<>();
        teamsUser.put(team0, userA);
        teamsUser.put(team1, user2);
        teamsUser.put(team2, userS);
        teamsUser.put(team3, user3);
        Map<Team, List<Exercice>> teamsExercice = new HashMap<>();
        teamsExercice.put(team0, Arrays.asList(exercice1, exercice3));
        teamsExercice.put(team1, Arrays.asList(exercice2));
        teamsExercice.put(team2, Arrays.asList(exercice1, exercice2, exercice3));
        teamsExercice.put(team3, Arrays.asList(exercice2));

        int resID = 0;
        int subID = 0;
        int cfID = 0;

        Random minutes = new Random();
        Random score = new Random();
        Result result;
        User user;

        List<Team> teams = teamsUser.keySet().stream()
            .collect(Collectors.toList()).stream()
            .sorted((a, b) -> a.getId().compareTo(b.getId()))
            .collect(Collectors.toList());

        for (Team team : teams) {
            user = teamsUser.get(team);
            for (Exercice exercice : teamsExercice.get(team)) {

                List<Submission> sub = new ArrayList<>();
                for (Language l : Language.values()) {
                    sub.add(new Submission()
                        .id(String.format("submission-%03d", subID++))
                        .date(Instant.now().minus(10 + minutes.nextInt(170),
                            ChronoUnit.MINUTES)).language(l)
                        .score(BigDecimal.valueOf(((float) Math.round(
                            score.nextFloat() * 1000.0f)) / 1000.0f)).user(user)
                        .addFileItem(new CodeFile()
                            .id(String.format("codefile-%03d", cfID++))
                            .name(String.format("main"))
                            .content(String.format("Code %s-%s", team.getId(),
                                l.toString()).getBytes())));
                }
                result = new Result()
                    .id(String.format("result-%03d", resID++))
                    .exercice(exercice).team(team).submissions(sub);
                result.getSubmissions().stream().forEach(s
                    -> {
                    s.getFiles().stream().forEach(f
                        -> mongoTemplate.save(f));
                    mongoTemplate.save(s);
                });
                mongoTemplate.save(result);
            }
        }
    }

}
