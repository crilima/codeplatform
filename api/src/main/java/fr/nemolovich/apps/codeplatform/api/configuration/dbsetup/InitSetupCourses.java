package fr.nemolovich.apps.codeplatform.api.configuration.dbsetup;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;

import fr.nemolovich.apps.codeplatform.api.model.Challenge;
import fr.nemolovich.apps.codeplatform.api.model.CodeHelper;
import fr.nemolovich.apps.codeplatform.api.model.Exercice;
import fr.nemolovich.apps.codeplatform.api.model.TestCase;
import fr.nemolovich.apps.codeplatform.api.model.enums.ChallengeScope;
import fr.nemolovich.apps.codeplatform.api.model.enums.ExerciceType;
import fr.nemolovich.apps.codeplatform.api.model.enums.Language;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 *
 * @author bgohier
 */
@ChangeLog(order = "003")
//@Profile("dev")
public class InitSetupCourses {

    @ChangeSet(order = "01", author = "initiator", id = "01-addCourses")
    public void addExercices(MongoTemplate mongoTemplate) {

        Exercice course1Ex1 = new Exercice();
        course1Ex1.setId("course1-ex1");
        course1Ex1.setTitle("Step 1");
        course1Ex1.setSubtitle("Learn to read variable");
        course1Ex1.setText("# Step 1\n"
            + "\n"
            + "Read a variable with following method:\n"
            + "\n"
            + "```{{CODE_HELPER_1}}\n"
            + "```\n"
            + "\n"
            + "Then you can use it like this:\n"
            + "\n"
            + "```{{CODE_HELPER_2}}\n"
            + "```\n"
            + "\n"
            + "Your goal is to read `variable` and print \"**Hello `varviale`!**\" \n"
            + "\n"
            + "\n");
        course1Ex1.setType(ExerciceType.FASTEST);

        course1Ex1.addTestCase(new TestCase().id("test-course1-ex1-1")
            .name("Course1_Exercice1").hidden(false).input("student")
            .output("Hello student!\n").order(1));

        course1Ex1.addTestCase(new TestCase().id("test-course1-ex1-2")
            .name("Course1_Exercice1_Private").hidden(true).input("othervar")
            .output("Hello othervar!\n").order(2));

        course1Ex1:
        {
            CodeHelper bashHelper = new CodeHelper()
                .id("help-course1-ex1-bash-1")
                .content("read varname\n"
                    + "\n")
                .language(Language.BASH)
                .order(1);
            course1Ex1.addCodeHelper(bashHelper);
            bashHelper = new CodeHelper().id("help-course1-ex1-bash-2")
                .content("echo \"varname = ${varname}\"\n"
                    + "\n")
                .language(Language.BASH)
                .order(2);
            course1Ex1.addCodeHelper(bashHelper);

            CodeHelper cHelper = new CodeHelper().id("help-course1-ex1-c-1")
                .content("char varname[2048];\n"
                    + "fgets(varname, 2048, stdin);\n"
                    + "\n")
                .language(Language.C)
                .order(1);
            course1Ex1.addCodeHelper(cHelper);
            cHelper = new CodeHelper().id("help-course1-ex1-c-2")
                .content("printf(\"varname = %s\\n\", varname);\n"
                    + "\n")
                .language(Language.C)
                .order(2);
            course1Ex1.addCodeHelper(cHelper);

            CodeHelper cppHelper = new CodeHelper().id("help-course1-ex1-cpp-1")
                .content("string varname;\n"
                    + "getline(cin, varname);\n"
                    + "\n")
                .language(Language.CPP)
                .order(1);
            course1Ex1.addCodeHelper(cppHelper);
            cppHelper = new CodeHelper().id("help-course1-ex1-cpp-2")
                .content("cout << \"varname = \" << varname << endl;\n"
                    + "\n")
                .language(Language.CPP)
                .order(2);
            course1Ex1.addCodeHelper(cppHelper);

            CodeHelper csHelper = new CodeHelper().id("help-course1-ex1-cs-1")
                .content("string varname = Console.ReadLine();\n"
                    + "\n")
                .language(Language.CS)
                .order(1);
            course1Ex1.addCodeHelper(csHelper);
            csHelper = new CodeHelper().id("help-course1-ex1-cs-2")
                .content("Console.WriteLine(\"varname = {0}\", varname);\n"
                    + "\n")
                .language(Language.CS)
                .order(2);
            course1Ex1.addCodeHelper(csHelper);

            CodeHelper javaHelper = new CodeHelper().id("help-course1-ex1-java-1")
                .content("Scanner in = new Scanner(System.in);\n"
                    + "String varname = in.nextLine();\n"
                    + "\n")
                .language(Language.JAVA)
                .order(1);
            course1Ex1.addCodeHelper(javaHelper);
            javaHelper = new CodeHelper().id("help-course1-ex1-java-2")
                .content("System.out.printf(\"varname = %s\\n\", varname);\n"
                    + "\n")
                .language(Language.JAVA)
                .order(2);
            course1Ex1.addCodeHelper(javaHelper);

            CodeHelper javascriptHelper = new CodeHelper()
                .id("help-course1-ex1-javascript-1")
                .content("var varname = readline();\n"
                    + "\n")
                .language(Language.JAVASCRIPT)
                .order(1);
            course1Ex1.addCodeHelper(javascriptHelper);
            javascriptHelper = new CodeHelper()
                .id("help-course1-ex1-javascript-2")
                .content("print(`varname = ${varname}`);\n"
                    + "\n")
                .language(Language.JAVASCRIPT)
                .order(2);
            course1Ex1.addCodeHelper(javascriptHelper);

            CodeHelper perlHelper = new CodeHelper()
                .id("help-course1-ex1-perl-1")
                .content("chomp(my $varname = <STDIN>);\n"
                    + "\n")
                .language(Language.PERL)
                .order(1);
            course1Ex1.addCodeHelper(perlHelper);
            perlHelper = new CodeHelper().id("help-course1-ex1-perl-2")
                .content("print \"varname = $varname\\n\";\n"
                    + "\n")
                .language(Language.PERL)
                .order(2);
            course1Ex1.addCodeHelper(perlHelper);

            CodeHelper phpHelper = new CodeHelper()
                .id("help-course1-ex1-php-1")
                .content("<?\n"
                    + "\n"
                    + "$varname = stream_get_line(STDIN, 2048, \"\\n"
                    + "\");\n"
                    + "\n"
                    + "?>\n"
                    + "\n")
                .language(Language.PHP)
                .order(1);
            course1Ex1.addCodeHelper(phpHelper);
            phpHelper = new CodeHelper().id("help-course1-ex1-php-2")
                .content("<?\n"
                    + "\n"
                    + "echo(\"varname = $varname\\n\");\n"
                    + "\n"
                    + "?>\n"
                    + "\n")
                .language(Language.PHP)
                .order(2);
            course1Ex1.addCodeHelper(phpHelper);

            CodeHelper pythonHelper = new CodeHelper()
                .id("help-course1-ex1-python-1")
                .content("varname = raw_input()\n"
                    + "\n")
                .language(Language.PYTHON)
                .order(1);
            course1Ex1.addCodeHelper(pythonHelper);
            pythonHelper = new CodeHelper().id("help-course1-ex1-python-2")
                .content("print \"varname = %s\" % varname\n"
                    + "\n")
                .language(Language.PYTHON)
                .order(2);
            course1Ex1.addCodeHelper(pythonHelper);

            CodeHelper python3Helper = new CodeHelper()
                .id("help-course1-ex1-python3-1")
                .content("varname = input()\n"
                    + "\n")
                .language(Language.PYTHON3)
                .order(1);
            course1Ex1.addCodeHelper(python3Helper);
            python3Helper = new CodeHelper().id("help-course1-ex1-python3-2")
                .content("print(\"varname = %s\" % varname)\n"
                    + "\n")
                .language(Language.PYTHON3)
                .order(2);
            course1Ex1.addCodeHelper(python3Helper);

            CodeHelper rubyHelper = new CodeHelper()
                .id("help-course1-ex1-ruby-1")
                .content("varname = gets.chomp\n"
                    + "\n")
                .language(Language.RUBY)
                .order(1);
            course1Ex1.addCodeHelper(rubyHelper);
            rubyHelper = new CodeHelper().id("help-course1-ex1-ruby-2")
                .content("puts \"varname = %s\" % varname\n"
                    + "\n")
                .language(Language.RUBY)
                .order(2);
            course1Ex1.addCodeHelper(rubyHelper);

        }

        course1Ex1.getTestCases().stream().forEach((testCase)
            -> mongoTemplate.save(testCase));
        course1Ex1.setCoeff(BigDecimal.ONE);
        course1Ex1.getCodeHelpers().stream().forEach((helper)
            -> mongoTemplate.save(helper));

        mongoTemplate.save(course1Ex1);

        Exercice course1Ex2 = new Exercice();
        course1Ex2.setId("course1-ex2");
        course1Ex2.setTitle("Step 2");
        course1Ex2.setSubtitle("Learn to read multiple variables");
        course1Ex2.setText("# Step 2\n"
            + "\n"
            + "Read multiple variables from a line with following method:\n"
            + "\n"
            + "```{{CODE_HELPER_1}}\n"
            + "```\n"
            + "\n"
            + "Then you can use them like this:\n"
            + "\n"
            + "```{{CODE_HELPER_2}}\n"
            + "```\n"
            + "\n"
            + "Your goal is to read a `firstname` and a `lastname` and print "
            + "\"**Hello `firstname` `lastname`!**\" \n"
            + "\n"
            + "\n");
        course1Ex2.setType(ExerciceType.FASTEST);

        course1Ex2.addTestCase(new TestCase().id("test-course1-ex2-1")
            .name("Course1_Exercice2").hidden(false).input(
            "firstname lastname").output("Hello firstname lastname!\n")
            .order(1));

        course1Ex2.addTestCase(new TestCase().id("test-course1-ex2-2")
            .name("Course1_Exercice2_Private").hidden(true).input("abc def")
            .output("Hello abc def!\n").order(2));

        course1Ex2:
        {
            CodeHelper bashHelper = new CodeHelper()
                .id("help-course1-ex2-bash-1")
                .content("read var1 var2 var3\n"
                    + "\n")
                .language(Language.BASH)
                .order(1);
            course1Ex2.addCodeHelper(bashHelper);
            bashHelper = new CodeHelper().id("help-course1-ex2-bash-2")
                .content("echo \"var1 = ${var1}\"\n"
                    + "echo \"var2 = ${var2}\"\n"
                    + "echo \"var3 = ${var3}\"\n"
                    + "\n")
                .language(Language.BASH)
                .order(2);
            course1Ex2.addCodeHelper(bashHelper);

            CodeHelper cHelper = new CodeHelper().id("help-course1-ex2-c-1")
                .content("char var1[2048];\n"
                    + "char var2[2048];\n"
                    + "char var3[2048];\n"
                    + "scanf(\"%s%s%s\", var1, var2, var3);\n"
                    + "\n")
                .language(Language.C)
                .order(1);
            course1Ex2.addCodeHelper(cHelper);
            cHelper = new CodeHelper().id("help-course1-ex2-c-2")
                .content("printf(\"var1 = %s\\nvar2 = %s\\nvar3 = %s\\n\", var1, var2, var3);\n"
                    + "\n")
                .language(Language.C)
                .order(2);
            course1Ex2.addCodeHelper(cHelper);

            CodeHelper cppHelper = new CodeHelper().id("help-course1-ex2-cpp-1")
                .content("string var1, var2, var3;\n"
                    + "cin >> var1;\n"
                    + "cin >> var2;\n"
                    + "cin >> var3;\n"
                    + "\n")
                .language(Language.CPP)
                .order(1);
            course1Ex2.addCodeHelper(cppHelper);
            cppHelper = new CodeHelper().id("help-course1-ex2-cpp-2")
                .content("printf(\"var1 = %s\\nvar2 = %s\\nvar3 = %s\\n\", var1.c_str(), var2.c_str(), var3.c_str());\n"
                    + "\n")
                .language(Language.CPP)
                .order(2);
            course1Ex2.addCodeHelper(cppHelper);

            CodeHelper csHelper = new CodeHelper().id("help-course1-ex2-cs-1")
                .content("string[] array = Console.ReadLine().Split(' ');\n"
                    + "string var1 = array[0];\n"
                    + "string var2 = array[1];\n"
                    + "string var3 = array[2];\n"
                    + "\n")
                .language(Language.CS)
                .order(1);
            course1Ex2.addCodeHelper(csHelper);
            csHelper = new CodeHelper().id("help-course1-ex2-cs-2")
                .content("Console.WriteLine(\"var1 = {0}\\nvar2 = {1}\\nvar3 = {2}\\n\",\n"
                    + "    var1, var2, var3);\n"
                    + "\n")
                .language(Language.CS)
                .order(2);
            course1Ex2.addCodeHelper(csHelper);

            CodeHelper javaHelper = new CodeHelper()
                .id("help-course1-ex2-java-1")
                .content("Scanner in = new Scanner(System.in);\n"
                    + "String var1 = in.next();\n"
                    + "String var2 = in.next();\n"
                    + "String var3 = in.next();\n"
                    + "\n")
                .language(Language.JAVA)
                .order(1);
            course1Ex2.addCodeHelper(javaHelper);
            javaHelper = new CodeHelper().id("help-course1-ex2-java-2")
                .content("System.out.printf(\"var1 = %s\\nvar2 = %s\\nvar3 = %s\\n\", var1, var2, var1);\n"
                    + "\n")
                .language(Language.JAVA)
                .order(2);
            course1Ex2.addCodeHelper(javaHelper);

            CodeHelper javascriptHelper = new CodeHelper()
                .id("help-course1-ex2-javascript-1")
                .content("var [var1, var2, var3] = readline().split(' ');\n"
                    + "\n")
                .language(Language.JAVASCRIPT)
                .order(1);
            course1Ex2.addCodeHelper(javascriptHelper);
            javascriptHelper = new CodeHelper()
                .id("help-course1-ex2-javascript-2")
                .content("print(`var1 = ${var1}\\nvar2 = ${var2}\\nvar3 = ${var3}\\n`);\n"
                    + "\n")
                .language(Language.JAVASCRIPT)
                .order(2);
            course1Ex2.addCodeHelper(javascriptHelper);

            CodeHelper perlHelper = new CodeHelper()
                .id("help-course1-ex2-perl-1")
                .content("chomp($array = <STDIN>);\n"
                    + "my ($var1, $var2, $var3) = split(/ /,$array);\n"
                    + "\n")
                .language(Language.PERL)
                .order(1);
            course1Ex2.addCodeHelper(perlHelper);
            perlHelper = new CodeHelper()
                .id("help-course1-ex2-perl-2")
                .content("print \"var1 = $var1\\nvar2 = $var2\\nvar3 = $var3\\n\";\n"
                    + "\n")
                .language(Language.PERL)
                .order(2);
            course1Ex2.addCodeHelper(perlHelper);

            CodeHelper phpHelper = new CodeHelper().id("help-course1-ex2-php-1")
                .content("<?\n"
                    + "\n"
                    + "fscanf(STDIN, \"%s %s %s\",\n"
                    + "    $var1,\n"
                    + "    $var2,\n"
                    + "    $var3\n"
                    + ");\n"
                    + "\n"
                    + "?>\n"
                    + "\n")
                .language(Language.PHP)
                .order(1);
            course1Ex2.addCodeHelper(phpHelper);
            phpHelper = new CodeHelper().id("help-course1-ex2-php-2")
                .content("<?\n"
                    + "\n"
                    + "echo(\"var1 = $var1\\nvar2 = $var2\\nvar3 = $var3\\n\");\n"
                    + "\n"
                    + "?>\n"
                    + "\n")
                .language(Language.PHP)
                .order(2);
            course1Ex2.addCodeHelper(phpHelper);

            CodeHelper pythonHelper = new CodeHelper()
                .id("help-course1-ex2-python-1")
                .content("var1, var2, var3 = raw_input().split()\n"
                    + "\n")
                .language(Language.PYTHON)
                .order(1);
            course1Ex2.addCodeHelper(pythonHelper);
            pythonHelper = new CodeHelper().id("help-course1-ex2-python-2")
                .content("print \"var1 = %s\\nvar2 = %s\\nvar3 = %s\\n\" % (var1, var2, var3)\n"
                    + "\n")
                .language(Language.PYTHON)
                .order(2);
            course1Ex2.addCodeHelper(pythonHelper);

            CodeHelper python3Helper = new CodeHelper()
                .id("help-course1-ex2-python3-1")
                .content("var1, var2, var3 = input().split()\n"
                    + "\n")
                .language(Language.PYTHON3)
                .order(1);
            course1Ex2.addCodeHelper(python3Helper);
            python3Helper = new CodeHelper().id("help-course1-ex2-python3-2")
                .content("print(\"var1 = %s\\nvar2 = %s\\nvar3 = %s\\n\" % (var1, var2, var3))\n"
                    + "\n")
                .language(Language.PYTHON3)
                .order(2);
            course1Ex2.addCodeHelper(python3Helper);

            CodeHelper rubyHelper = new CodeHelper()
                .id("help-course1-ex2-ruby-1")
                .content("var1, var2, var3 = gets.chomp.split\n"
                    + "\n")
                .language(Language.RUBY)
                .order(1);
            course1Ex2.addCodeHelper(rubyHelper);
            rubyHelper = new CodeHelper().id("help-course1-ex2-ruby-2")
                .content("puts \"var1 = %s\\nvar2 = %s\\nvar3 = %s\n"
                    + "\" % [var1, var2, var3]\n"
                    + "\n")
                .language(Language.RUBY)
                .order(2);
            course1Ex2.addCodeHelper(rubyHelper);
        }

        course1Ex2.getTestCases().stream().forEach((testCase)
            -> mongoTemplate.save(testCase));
        course1Ex2.setCoeff(BigDecimal.ONE);
        course1Ex2.getCodeHelpers().stream().forEach((helper)
            -> mongoTemplate.save(helper));

        mongoTemplate.save(course1Ex2);

        Exercice course1Ex3 = new Exercice();
        course1Ex3.setId("course1-ex3");
        course1Ex3.setTitle("Step 3");
        course1Ex3.setSubtitle("Learn to read array variable");
        course1Ex3.setText("# Step 3\n"
            + "\n"
            + "Read array variable from a line with following method:\n"
            + "\n"
            + "```{{CODE_HELPER_1}}\n"
            + "```\n"
            + "\n"
            + "Then you can browse them like this:\n"
            + "\n"
            + "```{{CODE_HELPER_2}}\n"
            + "```\n"
            + "\n"
            + "Your goal is to read all variables and print each"
            + "variable on a new line\n"
            + "\n"
            + "\n");
        course1Ex3.setType(ExerciceType.FASTEST);

        course1Ex3.addTestCase(new TestCase().id("test-course1-ex3-1")
            .name("Course1_Exercice3").hidden(false).input("this is an array")
            .output("this\n"
                + "is\n"
                + "an\n"
                + "array\n").order(1));

        course1Ex3.addTestCase(new TestCase().id("test-course1-ex3-2")
            .name("Course1_Exercice3_Private").hidden(true).input(
            "abcd efgh ijklm nopq rstu vwxyz").output("abcd\n"
                + "efgh\n"
                + "ijklm\n"
                + "nopq\n"
                + "rstu\n"
                + "vwxyz\n").order(2));

        course1Ex3:
        {
            CodeHelper bashHelper = new CodeHelper().id("help-course1-ex3-bash-1")
                .content("read arr_string\n"
                    + "array=(${arr_string/// })\n"
                    + "\n")
                .language(Language.BASH)
                .order(1);
            course1Ex3.addCodeHelper(bashHelper);
            bashHelper = new CodeHelper().id("help-course1-ex3-bash-2")
                .content("for elm in ${array[@]} ; do\n"
                    + "    echo \"Element: ${elm}\"\n"
                    + "done\n"
                    + "\n")
                .language(Language.BASH)
                .order(2);
            course1Ex3.addCodeHelper(bashHelper);

            CodeHelper cHelper = new CodeHelper().id("help-course1-ex3-c-1")
                .content("// Read the line\n"
                    + "char arr_string[2048];\n"
                    + "fgets(arr_string, 2048, stdin);\n"
                    + "\n"
                    + "// Split elements in the line into array\n"
                    + "int index = 0;\n"
                    + "char *p = strtok(arr_string, \" \");\n"
                    + "char *array[1024];\n"
                    + "while (p != NULL)\n"
                    + "{\n"
                    + "\n"
                    + "    array[index++] = p;\n"
                    + "    p = strtok(NULL, \" \");\n"
                    + "}\n"
                    + "\n")
                .language(Language.C)
                .order(1);
            course1Ex3.addCodeHelper(cHelper);
            cHelper = new CodeHelper().id("help-course1-ex3-c-2")
                .content("// With index the size of array\n"
                    + "for (int i = 0; i < index; ++i)\n"
                    + "{\n"
                    + "    printf(\"Element: %s\\n\", array[i]);\n"
                    + "}\n"
                    + "\n")
                .language(Language.C)
                .order(2);
            course1Ex3.addCodeHelper(cHelper);

            CodeHelper cppHelper = new CodeHelper().id("help-course1-ex3-cpp-1")
                .content("// Require '#include <sstream>'\n"
                    + "// Read ther line\n"
                    + "string arr_string;\n"
                    + "getline(cin, arr_string);\n"
                    + "\n"
                    + "// Split elements in the line into array\n"
                    + "string array[1024];\n"
                    + "int index = 0;\n"
                    + "stringstream ssin(arr_string);\n"
                    + "while (ssin.good() && index < 1024) {\n"
                    + "    ssin >> array[index];\n"
                    + "    ++index;\n"
                    + "}\n"
                    + "\n")
                .language(Language.CPP)
                .order(1);
            course1Ex3.addCodeHelper(cppHelper);
            cppHelper = new CodeHelper().id("help-course1-ex3-cpp-2")
                .content("// With index the size of array\n"
                    + "for(int i = 0; i < index; i++) {\n"
                    + "    cout << \"Element: \" << array[i] << endl;\n"
                    + "}\n"
                    + "\n")
                .language(Language.CPP)
                .order(2);
            course1Ex3.addCodeHelper(cppHelper);

            CodeHelper csHelper = new CodeHelper().id("help-course1-ex3-cs-1")
                .content("string arrString = Console.ReadLine();\n"
                    + "string[] array = arrString.Split(' ');\n"
                    + "\n")
                .language(Language.CS)
                .order(1);
            course1Ex3.addCodeHelper(csHelper);
            csHelper = new CodeHelper().id("help-course1-ex3-cs-2")
                .content("foreach (string elm in array)\n"
                    + "{\n"
                    + "    Console.WriteLine(\"Element: {0}\", elm);\n"
                    + "}\n"
                    + "\n")
                .language(Language.CS)
                .order(2);
            course1Ex3.addCodeHelper(csHelper);

            CodeHelper javaHelper = new CodeHelper().id("help-course1-ex3-java-1")
                .content("Scanner in = new Scanner(System.in);\n"
                    + "String[] array = in.nextLine().split(\" \");\n"
                    + "\n")
                .language(Language.JAVA)
                .order(1);
            course1Ex3.addCodeHelper(javaHelper);
            javaHelper = new CodeHelper().id("help-course1-ex3-java-2")
                .content("for (String elm: array) {\n"
                    + "    System.out.printf(\"Element: %s\\n\", elm);\n"
                    + "}\n"
                    + "\n")
                .language(Language.JAVA)
                .order(2);
            course1Ex3.addCodeHelper(javaHelper);

            CodeHelper javascriptHelper = new CodeHelper().id("help-course1-ex3-javascript-1")
                .content("var array = readline().split(' ');\n"
                    + "\n")
                .language(Language.JAVASCRIPT)
                .order(1);
            course1Ex3.addCodeHelper(javascriptHelper);
            javascriptHelper = new CodeHelper().id("help-course1-ex3-javascript-2")
                .content("array.forEach(elm => {\n"
                    + "    print(`Element: ${elm}`)\n"
                    + "});\n"
                    + "\n")
                .language(Language.JAVASCRIPT)
                .order(2);
            course1Ex3.addCodeHelper(javascriptHelper);

            CodeHelper perlHelper = new CodeHelper().id("help-course1-ex3-perl-1")
                .content("chomp(my @array = split / /, <STDIN>);\n"
                    + "\n")
                .language(Language.PERL)
                .order(1);
            course1Ex3.addCodeHelper(perlHelper);
            perlHelper = new CodeHelper().id("help-course1-ex3-perl-2")
                .content("foreach $elm (@array) {\n"
                    + "   print \"Element: $elm\\n\";\n"
                    + "}\n"
                    + "\n")
                .language(Language.PERL)
                .order(2);
            course1Ex3.addCodeHelper(perlHelper);

            CodeHelper phpHelper = new CodeHelper().id("help-course1-ex3-php-1")
                .content("<?\n"
                    + "\n"
                    + "$array = explode(\" \", stream_get_line(STDIN, 2048, \"\\n\"));\n"
                    + "\n"
                    + "?>\n"
                    + "\n")
                .language(Language.PHP)
                .order(1);
            course1Ex3.addCodeHelper(phpHelper);
            phpHelper = new CodeHelper().id("help-course1-ex3-php-2")
                .content("<?\n"
                    + "\n"
                    + "foreach ($array as $elm) {\n"
                    + "    echo(\"Element: $elm\\n\");\n"
                    + "}\n"
                    + "\n"
                    + "?>\n"
                    + "\n")
                .language(Language.PHP)
                .order(2);
            course1Ex3.addCodeHelper(phpHelper);

            CodeHelper pythonHelper = new CodeHelper().id("help-course1-ex3-python-1")
                .content("array = raw_input().split()\n"
                    + "\n")
                .language(Language.PYTHON)
                .order(1);
            course1Ex3.addCodeHelper(pythonHelper);
            pythonHelper = new CodeHelper().id("help-course1-ex3-python-2")
                .content("for elm in array:\n"
                    + "    print 'Element: %s' % elm\n"
                    + "\n")
                .language(Language.PYTHON)
                .order(2);
            course1Ex3.addCodeHelper(pythonHelper);

            CodeHelper python3Helper = new CodeHelper().id("help-course1-ex3-python3-1")
                .content("array = input().split()\n"
                    + "\n")
                .language(Language.PYTHON3)
                .order(1);
            course1Ex3.addCodeHelper(python3Helper);
            python3Helper = new CodeHelper().id("help-course1-ex3-python3-2")
                .content("for elm in array:\n"
                    + "    print('Element: %s' % elm)\n"
                    + "\n")
                .language(Language.PYTHON3)
                .order(2);
            course1Ex3.addCodeHelper(python3Helper);

            CodeHelper rubyHelper = new CodeHelper().id("help-course1-ex3-ruby-1")
                .content("array = gets.chomp.split\n"
                    + "\n")
                .language(Language.RUBY)
                .order(1);
            course1Ex3.addCodeHelper(rubyHelper);
            rubyHelper = new CodeHelper().id("help-course1-ex3-ruby-2")
                .content("array.map {|elm|puts 'Element: %s' % elm}\n"
                    + "\n")
                .language(Language.RUBY)
                .order(2);
            course1Ex3.addCodeHelper(rubyHelper);

        }

        course1Ex3.getTestCases().stream().forEach((testCase)
            -> mongoTemplate.save(testCase));
        course1Ex3.setCoeff(BigDecimal.ONE);
        course1Ex3.getCodeHelpers().stream().forEach((helper)
            -> mongoTemplate.save(helper));

        mongoTemplate.save(course1Ex3);

        Challenge course1 = new Challenge();
        course1.setId("course-1");
        course1.setDescription("Simple course to learn language");
        course1.setScope(ChallengeScope.COURSE);
        course1.setName("Course 1");
        course1.setStartDate(Instant.now().minus(1, ChronoUnit.DAYS));
        course1.setEndDate(Instant.now().plus(3650, ChronoUnit.DAYS));
        course1.addExerciceItem(course1Ex1);
        course1.addExerciceItem(course1Ex2);
        course1.addExerciceItem(course1Ex3);
        mongoTemplate.save(course1);
    }
}
