package fr.nemolovich.apps.codeplatform.api.model.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets Language
 */
public enum Language {

    BASH("bash"),
    C("c"),
    CPP("cpp"),
    CS("cs"),
    JAVA("java"),
    JAVASCRIPT("javascript"),
    MAVEN("maven"),
    PERL("perl"),
    PHP("php"),
    PYTHON("python"),
    PYTHON3("python3"),
    RUBY("ruby");

    private final String value;

    Language(String value) {
        this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static Language fromValue(String text) {
        for (Language b : Language.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        return null;
    }
}
