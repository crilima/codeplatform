package fr.nemolovich.apps.codeplatform.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.validation.annotation.Validated;

/**
 * Team
 */
@Validated
@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.SpringCodegen",
    date = "2017-10-07T19:30:45.309+02:00")
@Document(collection = "codeplatform_team")
public class Team {

    @JsonProperty("id")
    @Id
    private String id;

    @JsonProperty("name")
    @NotNull
    @Indexed
    @Size(min = 5, max = 50)
    private String name;

    @JsonProperty("creator")
    @NotNull
    @DBRef
    private User creator;

    @JsonProperty("members")
    @Valid
    @DBRef
    private Set<User> members = new HashSet<>();

    public Team id(String id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     *
     */
    @ApiModelProperty(value = "")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Team name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     *
     * @return name
     *
     */
    @ApiModelProperty(value = "")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Team creator(User creator) {
        this.creator = creator;
        return this;
    }

    /**
     * Get creator
     *
     * @return creator
     *
     */
    @ApiModelProperty(value = "")
    @Valid
    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public Team members(Set<User> members) {
        this.members = members;
        return this;
    }

    public Team addMemberItem(User membersItem) {
        if (this.members == null) {
            this.members = new HashSet<>();
        }
        this.members.add(membersItem);
        return this;
    }

    /**
     * Get members
     *
     * @return members
     *
     */
    @ApiModelProperty(value = "")
    @Valid
    public Set<User> getMembers() {
        return members;
    }

    public void setMembers(Set<User> members) {
        this.members = members;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Team team = (Team) o;
        return Objects.equals(this.name, team.name)
            && Objects.equals(this.creator, team.creator);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, creator);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Team {\n");
        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    creator: ").append(toIndentedString(creator))
            .append("\n");
        sb.append("    members: ").append(toIndentedString(members))
            .append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     * 
     * @param o {@link Object}: Object to format.
     * @return {@link String}: String representation of object.
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
