package fr.nemolovich.apps.codeplatform.api.webapi.impl;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.nemolovich.apps.codeplatform.api.model.Challenge;
import fr.nemolovich.apps.codeplatform.api.model.Exercice;
import fr.nemolovich.apps.codeplatform.api.model.Result;
import fr.nemolovich.apps.codeplatform.api.model.Team;
import fr.nemolovich.apps.codeplatform.api.model.User;
import fr.nemolovich.apps.codeplatform.api.model.enums.ChallengeScope;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.ChallengeLightViewModel;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.ResultViewModel;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.UserViewModel;
import fr.nemolovich.apps.codeplatform.api.service.ChallengeService;
import fr.nemolovich.apps.codeplatform.api.service.TeamService;
import fr.nemolovich.apps.codeplatform.api.service.UserService;
import fr.nemolovich.apps.codeplatform.api.webapi.ChallengeApi;
import io.swagger.annotations.ApiParam;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Ionut on 08/10/2017.
 */
@Controller
public class ChallengeApiController implements ChallengeApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        ChallengeApiController.class);

    private final ObjectMapper objectMapper;
    private final ChallengeService challengeService;
    private final TeamService teamService;
    private final UserService userService;

    public ChallengeApiController(ObjectMapper objectMapper,
        ChallengeService challengeService,
        UserService userService,
        TeamService teamService) {
        this.objectMapper = objectMapper;
        this.challengeService = challengeService;
        this.userService = userService;
        this.teamService = teamService;
    }

    @Override
    public ResponseEntity<Challenge> addChallenge(
        @ApiParam(value = "Challenge to be created", required = true)
        @Valid @RequestBody Challenge challenge,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("addChallenge Rest");
        if (accept != null && accept.contains("application/json")) {
            LOGGER.debug("REST request to save addChallenge : {}", challenge);
            if (challenge.getId() != null) {
                return ResponseEntity.badRequest().body(null);
            }
            Challenge result = challengeService.createChallenge(challenge);
            LOGGER.debug("addChallenge result:{}", result);
            return ResponseEntity.ok(result);
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<Challenge> deleteChallenge(
        @ApiParam(value = "ID of the challenge to delete", required = true)
        @PathVariable("challengeId") String challengeId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("deleteChallenge challengeId:{}", challengeId);
        if (accept != null && accept.contains("application/json")
            && challengeId != null) {
            challengeService.deleteChallenge(challengeId);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<Set<ChallengeLightViewModel>> getAllChallenges(
        @ApiParam(value = "Type of challenge to look for")
        @Valid @RequestParam(value = "filter", required = false) String filter,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("getAllChallenges Rest");
        if (accept != null && accept.contains("application/json")) {
            Set<Challenge> challenges;
            final User user = userService.getCurrentUser().orElse(null);
            if ("registered".equalsIgnoreCase(filter)) {
                if (user == null) {
                    challenges = new HashSet<>();
                } else {
                    challenges = challengeService.getAllChallenges()
                        .stream().filter(
                            challenge -> {
                                Set<Team> tms = challenge.getTeams()
                                .stream().filter(t -> t != null
                                    && !t.getMembers().isEmpty())
                                .collect(Collectors.toSet());
                                return !tms.isEmpty() && tms.stream()
                                .anyMatch(t -> t.getMembers().contains(user));
                            })
                        .collect(Collectors.toSet());
                }
            } else if (filter != null
                && Arrays.asList(ChallengeScope.values()).stream()
                .map(cs -> cs.name()).anyMatch(
                csName -> csName.equalsIgnoreCase(filter))) {
                challenges = challengeService.getAllChallenges().stream()
                    .filter(c -> c.getScope().toString()
                        .equalsIgnoreCase(filter))
                    .collect(Collectors.toSet());
            } else {
                challenges = challengeService.getAllChallenges();
            }
            BigDecimal score;
            ChallengeLightViewModel viewModel;
            Set<ChallengeLightViewModel> result = new HashSet<>();
            for (Challenge challenge : challenges) {
                viewModel = new ChallengeLightViewModel(challenge);
                score = BigDecimal.ZERO;
                if (!challenge.getResults().isEmpty()) {
                    for (Result r : challenge.getResults().stream()
                        .filter(r -> r.getTeam().getMembers().contains(user)
                            && !r.getSubmissions().isEmpty()
                            && r.getSubmissions().stream()
                            .filter(s -> s.getUser().equals(user))
                            .findAny().isPresent()).collect(Collectors.toSet())) {
                        for (Exercice e : challenge.getExercices()) {
                            if (r.getExercice().equals(e)) {
                                score = score.add(r.getSubmissions().stream()
                                    .map(s -> s.getScore())
                                    .sorted((a, b) -> b.compareTo(a))
                                    .findFirst().orElse(BigDecimal.ZERO)
                                    .multiply(e.getCoeff()));
                            }
                        }
                    }
                }
                viewModel.setScore(score.divide(
                    BigDecimal.valueOf(challenge.getExercices().size()), 4,
                    RoundingMode.HALF_EVEN));
                result.add(viewModel);
            }
            return ResponseEntity.ok(result);
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<ChallengeLightViewModel> getChallengeByID(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("getChallengeByID Rest");
        if (accept != null && accept.contains("application/json")
            && challengeId != null) {
            Challenge challenge = challengeService.getChallengeById(
                challengeId);
            if (challenge == null) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
            } else {
                return ResponseEntity.ok(new ChallengeLightViewModel(
                    challenge));
            }
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<ChallengeLightViewModel> registerTeam(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("registerTeam Rest");
        if (accept != null && accept.contains("application/json")
            && challengeId != null) {
            User user = this.userService.getCurrentUser().orElse(null);
            Challenge challenge = this.challengeService
                .getChallengeById(challengeId);
            if (user == null || challenge == null) {
                return new ResponseEntity<>(null, HttpStatus.NOT_MODIFIED);
            }
            Team team = this.teamService.getTeamByName(user.getLogin());
            if (team == null) {
                team = new Team().creator(user).addMemberItem(user)
                    .name(user.getLogin());
                team = this.teamService.saveTeam(team);
            } else if (team.getMembers().isEmpty()) {
                team = team.addMemberItem(user);
                team = this.teamService.saveTeam(team);
            }
            challenge = this.challengeService.addTeamInChallenge(
                challenge, team);
            return ResponseEntity.ok().body(
                new ChallengeLightViewModel(challenge));
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<ChallengeLightViewModel> leaveTeam(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "ID of the requested team", required = true)
        @PathVariable("teamId") String teamId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("leaveTeam Rest");
        if (accept != null && accept.contains("application/json")
            && challengeId != null) {
            User user = this.userService.getCurrentUser().orElse(null);
            Challenge challenge = this.challengeService
                .getChallengeById(challengeId);
            if (user == null || challenge == null) {
                return new ResponseEntity<>(null, HttpStatus.NOT_MODIFIED);
            }
            Team team = this.teamService.getTeamById(teamId);
            if (team == null) {
                return new ResponseEntity<>(null, HttpStatus.NOT_MODIFIED);
            }
            challenge = this.challengeService.deleteTeamInChallenge(challengeId,
                teamId);
            team = teamService.removeUserFromTeam(team.getId(),
                new UserViewModel(user));
            challenge = this.challengeService.addTeamInChallenge(challenge,
                team);
            return ResponseEntity.ok().body(
                new ChallengeLightViewModel(challenge));
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<Challenge> updateChallenge(
        @ApiParam(value = "Challenge to be updated", required = true)
        @Valid @RequestBody Challenge challenge,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("updateChallenge Rest");
        if (accept != null && accept.contains("application/json")) {
            if (challenge.getId() == null) {
                // if id is null create new challenge
                addChallenge(challenge, accept);
            } else {
                return ResponseEntity.ok(challengeService.updateChallenge(
                    challenge));
            }
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<Set<ResultViewModel>> getResultsByChallengeId(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "ID of the team to get results")
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("getResultsByChallengeId Rest");
        if (accept != null && accept.contains("application/json")) {
            return ResponseEntity.ok(challengeService
                .getAllResultViewModelsByChallengeId(challengeId));
        }
        return ResponseEntity.badRequest().body(null);
    }
}
