package fr.nemolovich.apps.codeplatform.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import fr.nemolovich.apps.codeplatform.api.model.Role;

/**
 * Spring Data MongoDB repository for the Role entiry management.
 */
public interface RoleRepository extends MongoRepository<Role, String> {
}
