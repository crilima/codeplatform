package fr.nemolovich.apps.codeplatform.api.webapi;

import fr.nemolovich.apps.codeplatform.api.model.Challenge;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.ChallengeLightViewModel;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.ResultViewModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.Set;
import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Challenge API
 */
@Api(value = "Challenge API", description = "The Challenge API")
public interface ChallengeApi {

    @ApiOperation(value = "Create a new challenge", response = Challenge.class,
                  tags = {"admin", "challenge",})
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Creation Succeed",
                     response = Challenge.class),
        @ApiResponse(code = 401, message = "Unauthorized",
                     response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/admin/challenge",
                    produces = {"application/json"},
                    consumes = {"application/json"},
                    method = RequestMethod.POST)
    ResponseEntity<Challenge> addChallenge(
        @ApiParam(value = "Challenge to be created", required = true)
        @Valid @RequestBody Challenge challenge,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Delete a challenge", response = Challenge.class,
                  tags = {"admin", "challenge",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Deletion Succeed",
                     response = Challenge.class),
        @ApiResponse(code = 204, message = "No Content"),
        @ApiResponse(code = 401, message = "Unauthorized",
                     response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/admin/challenge/{challengeId}",
                    produces = {"application/json"},
                    method = RequestMethod.DELETE)
    ResponseEntity<Challenge> deleteChallenge(
        @ApiParam(value = "ID of the challenge to delete", required = true)
        @PathVariable("challengeId") String challengeId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Get all challenges", response =
                                                ChallengeLightViewModel.class,
                  responseContainer = "Set", tags = {"challenge",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The existing challenges",
                     response = ChallengeLightViewModel.class,
                     responseContainer = "Set"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/challenge",
                    produces = {"application/json"},
                    method = RequestMethod.GET)
    ResponseEntity<Set<ChallengeLightViewModel>> getAllChallenges(
        @ApiParam(value = "Type of challenge to look for",
                  allowableValues = "fastest, shortest, reverse")
        @Valid @RequestParam(value = "filter", required = false) String filter,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Get a challenge by ID",
                  response = ChallengeLightViewModel.class,
                  tags = {"challenge",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The corresponding challenge",
                     response = ChallengeLightViewModel.class),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/challenge/{challengeId}",
                    produces = {"application/json"},
                    method = RequestMethod.GET)
    ResponseEntity<ChallengeLightViewModel> getChallengeByID(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Register a team to a challenge",
                  response = ChallengeLightViewModel.class,
                  tags = {"challenge",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The corresponding challenge",
                     response = ChallengeLightViewModel.class),
        @ApiResponse(code = 304, message = "Not modified",
                     response = ChallengeLightViewModel.class),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/challenge/{challengeId}/register",
                    produces = {"application/json"},
                    method = RequestMethod.GET)
    ResponseEntity<ChallengeLightViewModel> registerTeam(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Leave a team from a challenge",
                  response = ChallengeLightViewModel.class, tags = {"challenge",
                                                                    "team",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The corresponding challenge",
                     response = ChallengeLightViewModel.class),
        @ApiResponse(code = 304, message = "Not modified",
                     response = ChallengeLightViewModel.class),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/challenge/{challengeId}/leave/{teamId}",
                    produces = {"application/json"},
                    method = RequestMethod.DELETE)
    ResponseEntity<ChallengeLightViewModel> leaveTeam(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "ID of the requested team", required = true)
        @PathVariable("teamId") String teamId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Update a challenge", response = Challenge.class,
                  tags = {"admin", "challenge",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Update Succeed",
                     response = Challenge.class),
        @ApiResponse(code = 204, message = "No Content"),
        @ApiResponse(code = 401, message = "Unauthorized",
                     response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/admin/challenge",
                    produces = {"application/json"},
                    consumes = {"application/json"},
                    method = RequestMethod.PUT)
    ResponseEntity<Challenge> updateChallenge(
        @ApiParam(value = "Challenge to be updated", required = true)
        @Valid @RequestBody Challenge challenge,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Get all results for the given challenge",
                  notes = "", response = ResultViewModel.class,
                  responseContainer = "Set",
                  tags = {"challenge", "result",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The requested results",
                     response = ResultViewModel.class, responseContainer = "Set"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/challenge/{challengeId}/result",
                    produces = {"application/json"},
                    method = RequestMethod.GET)
    ResponseEntity<Set<ResultViewModel>> getResultsByChallengeId(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

}
