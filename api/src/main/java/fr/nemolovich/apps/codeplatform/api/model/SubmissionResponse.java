package fr.nemolovich.apps.codeplatform.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.Objects;
import javax.validation.Valid;
import org.springframework.validation.annotation.Validated;

/**
 * SubmissionResponse
 */
@Validated
@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.SpringCodegen",
    date = "2017-10-15T01:00:32.677+02:00")
public class SubmissionResponse {

    @JsonProperty("resultName")
    private String resultName = null;

    @JsonProperty("returnMessage")
    private String returnMessage = null;

    @JsonProperty("score")
    private BigDecimal score = null;

    @JsonProperty("stdout")
    private String stdout = null;

    @JsonProperty("stderr")
    private String stderr = null;

    public SubmissionResponse resultName(String resultName) {
        this.resultName = resultName;
        return this;
    }

    /**
     * Get resultName
     *
     * @return resultName
     *
     */
    @ApiModelProperty(value = "")
    public String getResultName() {
        return resultName;
    }

    public void setResultName(String resultName) {
        this.resultName = resultName;
    }

    public SubmissionResponse returnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
        return this;
    }

    /**
     * Get returnMessage
     *
     * @return returnMessage
     *
     */
    @ApiModelProperty(value = "")
    public String getReturnMessage() {
        return returnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

    public SubmissionResponse score(BigDecimal score) {
        this.score = score;
        return this;
    }

    /**
     * Get score
     *
     * @return score
     *
     */
    @ApiModelProperty(value = "")
    @Valid
    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    public SubmissionResponse stdout(String stdout) {
        this.stdout = stdout;
        return this;
    }

    /**
     * Get stdout
     *
     * @return stdout
     *
     */
    @ApiModelProperty(value = "")
    public String getStdout() {
        return stdout;
    }

    public void setStdout(String stdout) {
        this.stdout = stdout;
    }

    public SubmissionResponse stderr(String stderr) {
        this.stderr = stderr;
        return this;
    }

    /**
     * Get stderr
     *
     * @return stderr
     *
     */
    @ApiModelProperty(value = "")
    public String getStderr() {
        return stderr;
    }

    public void setStderr(String stderr) {
        this.stderr = stderr;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SubmissionResponse submissionResponse = (SubmissionResponse) o;
        return Objects.equals(this.resultName, submissionResponse.resultName)
            && Objects.equals(this.returnMessage,
                submissionResponse.returnMessage)
            && Objects.equals(this.score, submissionResponse.score)
            && Objects.equals(this.stdout, submissionResponse.stdout)
            && Objects.equals(this.stderr, submissionResponse.stderr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(resultName, returnMessage, score, stdout, stderr);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class SubmissionResponse {\n");

        sb.append("    resultName: ").append(toIndentedString(resultName))
            .append("\n");
        sb.append("    returnMessage: ").append(toIndentedString(returnMessage))
            .append("\n");
        sb.append("    score: ").append(toIndentedString(score)).append("\n");
        sb.append("    stdout: ").append(toIndentedString(stdout)).append("\n");
        sb.append("    stderr: ").append(toIndentedString(stderr)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     * 
     * @param o {@link Object}: Object to format.
     * @return {@link String}: String representation of object.
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
