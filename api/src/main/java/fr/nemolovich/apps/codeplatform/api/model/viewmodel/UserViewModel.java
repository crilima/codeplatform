package fr.nemolovich.apps.codeplatform.api.model.viewmodel;

import com.fasterxml.jackson.annotation.JsonProperty;

import fr.nemolovich.apps.codeplatform.api.model.Role;
import fr.nemolovich.apps.codeplatform.api.model.User;
import io.swagger.annotations.ApiModelProperty;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.springframework.validation.annotation.Validated;

/**
 * UserViewModel
 */
@Validated
@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.SpringCodegen",
    date = "2017-10-07T19:30:45.309+02:00")
public class UserViewModel {

    @JsonProperty("email")
    private String email = null;

    @JsonProperty("firstName")
    private String firstName = null;

    @JsonProperty("lastName")
    private String lastName = null;

    @JsonProperty("login")
    @NotNull
    private String login = null;

    @JsonProperty("roles")
    private Set<String> roles = null;

    @JsonProperty("imageUrl")
    private String imageUrl = null;

    public UserViewModel() {
        this.roles = new HashSet<>();
    }

    public UserViewModel(User user) {
        this(user.getLogin(), user.getFirstName(),
            user.getLastName(), user.getEmail(), user.getImageUrl(),
            user.getRoles().stream().map(
                Role::getName).collect(Collectors.toSet()));
    }


    public UserViewModel(String login, String firstName,
        String lastName, String email, String imageUrl, Set<String> roles) {
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.imageUrl = imageUrl;
        this.roles = roles;
    }


    /**
     * Set email
     *
     * @param email The email to set
     * @return current UserViewModel
     *
     */
    public UserViewModel email(String email) {
        this.email = email;
        return this;
    }

    @ApiModelProperty(value = "")
    @Size(min = 5, max = 100)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Set firstName
     *
     * @param firstName The firstName to set
     * @return current UserViewModel
     *
     */
    public UserViewModel firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    @ApiModelProperty(value = "")
    @Size(min = 0, max = 50)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Set lastName
     *
     * @param lastName The lastName to set
     * @return current UserViewModel
     *
     */
    public UserViewModel lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    @ApiModelProperty(value = "")
    @Size(min = 0, max = 50)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public UserViewModel login(String login) {
        this.login = login;
        return this;
    }

    /**
     * Get login
     *
     * @return login
     *
     */
    @ApiModelProperty(required = true, value = "")
    @NotNull
    @Pattern(regexp = "^[_'.@A-Za-z0-9-]*$")
    @Size(min = 1, max = 50)
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @ApiModelProperty(value = "")
    public Set<String> getRoles() {
        return Collections.unmodifiableSet(roles);
    }

    public void setRoles(Set<String> roles) {
        this.roles = new HashSet<>(roles);
    }

    /**
     * Get imageUrl
     *
     * @return imageUrl
     *
     */
    @ApiModelProperty(value = "")
    @Size(min = 0, max = 256)
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserViewModel userViewModel = (UserViewModel) o;
        return Objects.equals(this.email, userViewModel.email)
            && Objects.equals(this.firstName, userViewModel.firstName)
            && Objects.equals(this.lastName, userViewModel.lastName)
            && Objects.equals(this.login, userViewModel.login)
            && Objects.equals(this.roles, userViewModel.roles);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, firstName, lastName, login, roles);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class UserViewModel {\n");
        sb.append("    email: ").append(toIndentedString(email)).append("\n");
        sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
        sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
        sb.append("    login: ").append(toIndentedString(login)).append("\n");
        sb.append("    roles: ").append(toIndentedString(roles)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     * 
     * @param o {@link Object}: Object to format.
     * @return {@link String}: String representation of object.
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
