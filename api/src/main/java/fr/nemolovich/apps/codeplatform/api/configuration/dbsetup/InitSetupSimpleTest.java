package fr.nemolovich.apps.codeplatform.api.configuration.dbsetup;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;

import fr.nemolovich.apps.codeplatform.api.model.CodeStarter;
import fr.nemolovich.apps.codeplatform.api.model.Exercice;
import fr.nemolovich.apps.codeplatform.api.model.TestCase;
import fr.nemolovich.apps.codeplatform.api.model.enums.ExerciceType;
import fr.nemolovich.apps.codeplatform.api.model.enums.Language;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 *
 * @author bgohier
 */
@ChangeLog(order = "002")
//@Profile("dev")
public class InitSetupSimpleTest {

    @ChangeSet(order = "01", author = "initiator", id = "01-addSimpleTest")
    public void addExercices(MongoTemplate mongoTemplate) {
        simpleTest:
        {
            CodeStarter bashStarter = new CodeStarter().id("simple-bash-starter")
                .language(Language.BASH)
                .content("# n: the number of integers in list\n"
                    + "read n\n"
                    + "read arr_string\n"
                    + "array=(${arr_string/// })\n"
                    + "\n"
                    + "for (( i=0; i<n; i++ )); do\n"
                    + "    # t: an integer\n"
                    + "    t=${array[$i]}\n"
                    + "done\n"
                    + "\n"
                    + "echo \"sum\"\n"
                    + "\n");
            CodeStarter cStarter = new CodeStarter().id("simple-c-starter")
                .language(Language.C)
                .content("#include <stdlib.h>\n"
                    + "#include <stdio.h>\n"
                    + "#include <string.h>\n"
                    + "\n"
                    + "int main()\n"
                    + "{\n"
                    + "    int n; // the number of integers in list\n"
                    + "    scanf(\"%d\", &n);\n"
                    + "    for (int i = 0; i < n; i++) {\n"
                    + "        int t; // an integer\n"
                    + "        scanf(\"%d\", &t);\n"
                    + "    }\n"
                    + "\n"
                    + "    printf(\"sum\\n\");\n"
                    + "\n"
                    + "    return 0;\n"
                    + "}\n"
                    + "\n");
            CodeStarter cppStarter = new CodeStarter().id("simple-cpp-starter")
                .language(Language.CPP)
                .content("#include <iostream>\n"
                    + "#include <string>\n"
                    + "#include <vector>\n"
                    + "#include <algorithm>\n"
                    + "\n"
                    + "using namespace std;\n"
                    + "\n"
                    + "int main()\n"
                    + "{\n"
                    + "    int n; // the number of integers in list\n"
                    + "    cin >> n; cin.ignore();\n"
                    + "    for (int i = 0; i < n; i++) {\n"
                    + "        int t; // an integer\n"
                    + "        cin >> t; cin.ignore();\n"
                    + "    }\n"
                    + "\n"
                    + "    cout << \"sum\" << endl;\n"
                    + "}\n"
                    + "\n");
            CodeStarter csStarter = new CodeStarter().id("simple-cs-starter")
                .language(Language.CS)
                .content("using System;\n"
                    + "using System.Linq;\n"
                    + "using System.IO;\n"
                    + "using System.Text;\n"
                    + "using System.Collections;\n"
                    + "using System.Collections.Generic;\n"
                    + "\n"
                    + "class main\n"
                    + "{\n"
                    + "    static void Main(string[] args)\n"
                    + "    {\n"
                    + "        int n = int.Parse(Console.ReadLine()); // the number of integers in list\n"
                    + "        foreach (string i in Console.ReadLine().Split(' ')) {\n"
                    + "            int t = int.Parse(i); // an integer\n"
                    + "        }\n"
                    + "        Console.WriteLine(\"sum\");\n"
                    + "    }\n"
                    + "}\n"
                    + "\n");
            CodeStarter javaStarter = new CodeStarter().id("simple-java-starter")
                .language(Language.JAVA)
                .content("import java.util.*;\n"
                    + "import java.io.*;\n"
                    + "import java.math.*;\n"
                    + "\n"
                    + "class Main {\n"
                    + "    public static void main(String args[]) {\n"
                    + "        Scanner in = new Scanner(System.in);\n"
                    + "        int n = in.nextInt(); // the number of integers in list\n"
                    + "        for (int i = 0; i < n; i++) {\n"
                    + "            int t = in.nextInt(); // an integer\n"
                    + "        }\n"
                    + "\n"
                    + "        System.out.println(\"sum\");\n"
                    + "    }\n"
                    + "}\n"
                    + "\n");
            CodeStarter jsStarter = new CodeStarter().id("simple-javascript-starter")
                .language(Language.JAVASCRIPT)
                .content("var n = parseInt(readline()); // the number of integers in list\n"
                    + "var inputs = readline().split(' ');\n"
                    + "for (var i = 0; i < n; i++) {\n"
                    + "    var t = parseInt(inputs[i]); // an integer\n"
                    + "}\n"
                    + "\n"
                    + "print('sum');\n"
                    + "\n");
            CodeStarter perlStarter = new CodeStarter().id("simple-perl-starter")
                .language(Language.PERL)
                .content("chomp(my $n = <STDIN>); # the number of integers in list\n"
                    + "\n"
                    + "foreach $x (split(/ /, <STDIN>)) {\n"
                    + "    # $t: an integer\n"
                    + "    my $t = $x;\n"
                    + "}\n"
                    + "\n"
                    + "print \"sum\\n\";\n"
                    + "\n");
            CodeStarter phpStarter = new CodeStarter().id("simple-php-starter")
                .language(Language.PHP)
                .content("<?php\n"
                    + "\n"
                    + "fscanf(STDIN, \"%d\", $n); // the number of integers in list\n"
                    + "\n"
                    + "$inputs = fgets(STDIN);\n"
                    + "$inputs = explode(\" \", $inputs);\n"
                    + "for ($i = 0; $i < $n; $i++)\n"
                    + "{\n"
                    + "    $t = intval($inputs[$i]); // an integer\n"
                    + "}\n"
                    + "\n"
                    + "echo(\"sum\");\n"
                    + "?>\n"
                    + "\n");
            CodeStarter pythonStarter = new CodeStarter().id("simple-python-starter")
                .language(Language.PYTHON)
                .content("import sys\n"
                    + "import math\n"
                    + "\n"
                    + "n = input()  # the number of integers in list\n"
                    + "for i in raw_input().split():\n"
                    + "    # t: an integer\n"
                    + "    t = int(i)\n"
                    + "\n"
                    + "print \"sum\"\n"
                    + "\n");
            CodeStarter python3Starter = new CodeStarter().id("simple-python3-starter")
                .language(Language.PYTHON3)
                .content("import sys\n"
                    + "import math\n"
                    + "\n"
                    + "n = int(input())  # the number of integers in list\n"
                    + "for i in input().split():\n"
                    + "    # t: an integer\n"
                    + "    t = int(i)\n"
                    + "\n"
                    + "print(\"sum\")\n"
                    + "\n");
            CodeStarter rubyStarter = new CodeStarter().id("simple-ruby-starter")
                .language(Language.RUBY)
                .content("n = gets.to_i  # the number of integers in list\n"
                    + "\n"
                    + "gets.chomp.split.each{|i|\n"
                    + "    # t: an integer\n"
                    + "    t = i.to_i\n"
                    + "}\n"
                    + "\n"
                    + "puts \"sum\"\n"
                    + "\n");

            Exercice simpleTest = new Exercice();
            simpleTest.setId("simple-test");
            simpleTest.setTitle("Simple Exemple");
            simpleTest.setSubtitle("Learn to make a sum!");
            simpleTest.setText("# Simple Test\n"
                + "\n"
                + "## Learn to make a sum!\n"
                + "\n"
                + "Your goal is to sum all inputs numbers. You will get a list of numbers separated\n"
                + "by space (). You have to sum each number and display it.\n"
                + "\n"
                + "### Inputs\n"
                + "`N`: The size of the number list (0 < `N` < 1000000)\n"
                + "`L`: A string containing the list of `N` numbers separated by spaces\n"
                + "\n"
                + "### Outputs\n"
                + "`S`: The sum of the `N` integers in the list\n"
                + "\n"
                + "### Example\n"
                + "\n"
                + "| Inputs                           | Outputs                         |\n"
                + "|:---------------------------------|:--------------------------------|\n"
                + "| ```sh\\n5\\n1 2 3 4 5\\n``` | ```sh\\n2\\n``` |\n"
                + "\n");
            simpleTest.setType(ExerciceType.FASTEST);

            StringBuilder sb;
            StringBuilder sb2;
            for (int i = 1; i <= 2; i++) {
                sb = new StringBuilder();
                sb2 = new StringBuilder();
                String name = "t" + i;
                try (InputStream is = InitSetupSimpleTest.class.getResourceAsStream(
                    "/in/simple-test/" + name + ".in"); BufferedInputStream bis
                    = new BufferedInputStream(is)) {
                    byte[] buffer = new byte[2048];
                    int len;
                    while ((len = bis.read(buffer)) != -1) {
                        sb.append(new String(buffer, 0, len));
                    }
                } catch (IOException ioe) {
                    Logger.getLogger(InitSetupSimpleTest.class.getName()).log(
                        Level.SEVERE, "Cannot read input file", ioe);
                }
                try (InputStream is = InitSetupSimpleTest.class.getResourceAsStream(
                    "/out/simple-test/" + name + ".out"); BufferedInputStream bis
                    = new BufferedInputStream(is)) {
                    byte[] buffer = new byte[2048];
                    int len;
                    while ((len = bis.read(buffer)) != -1) {
                        sb2.append(new String(buffer, 0, len));
                    }
                } catch (IOException ioe) {
                    Logger.getLogger(InitSetupSimpleTest.class.getName()).log(
                        Level.SEVERE, "Cannot read output file", ioe);
                }
                simpleTest.addTestCase(new TestCase().id("test-simple-" + i)
                    .name(name).hidden(false).input(sb.toString())
                    .output(sb2.toString()).order(i));
            }
            sb = new StringBuilder();
            sb2 = new StringBuilder();
            try (InputStream is = InitSetupSimpleTest.class.getResourceAsStream(
                "/in/simple-test/t3.in"); BufferedInputStream bis
                = new BufferedInputStream(is)) {
                byte[] buffer = new byte[2048];
                int len;
                while ((len = bis.read(buffer)) != -1) {
                    sb.append(new String(buffer, 0, len));
                }
            } catch (IOException ioe) {
                Logger.getLogger(InitSetupSimpleTest.class.getName()).log(
                    Level.SEVERE, "Cannot read input file", ioe);
            }
            try (InputStream is = InitSetupSimpleTest.class.getResourceAsStream(
                "/out/simple-test/t3.out"); BufferedInputStream bis
                = new BufferedInputStream(is)) {
                byte[] buffer = new byte[2048];
                int len;
                while ((len = bis.read(buffer)) != -1) {
                    sb2.append(new String(buffer, 0, len));
                }
            } catch (IOException ioe) {
                Logger.getLogger(InitSetupSimpleTest.class.getName()).log(
                    Level.SEVERE, "Cannot read output file", ioe);
            }
            simpleTest.addTestCase(new TestCase().id("test-simple-3")
                .name("t3").hidden(true).input(sb.toString())
                .output(sb2.toString()).order(3));

            sb = new StringBuilder();
            sb2 = new StringBuilder();
            try (InputStream is = InitSetupSimpleTest.class.getResourceAsStream(
                "/in/simple-test/Many Values.in"); BufferedInputStream bis
                = new BufferedInputStream(is)) {
                byte[] buffer = new byte[2048];
                int len;
                while ((len = bis.read(buffer)) != -1) {
                    sb.append(new String(buffer, 0, len));
                }
            } catch (IOException ioe) {
                Logger.getLogger(InitSetupSimpleTest.class.getName()).log(
                    Level.SEVERE, "Cannot read input file", ioe);
            }
            try (InputStream is = InitSetupSimpleTest.class.getResourceAsStream(
                "/out/simple-test/Many Values.out"); BufferedInputStream bis
                = new BufferedInputStream(is)) {
                byte[] buffer = new byte[2048];
                int len;
                while ((len = bis.read(buffer)) != -1) {
                    sb2.append(new String(buffer, 0, len));
                }
            } catch (IOException ioe) {
                Logger.getLogger(InitSetupSimpleTest.class.getName()).log(
                    Level.SEVERE, "Cannot read output file", ioe);
            }
            simpleTest.addTestCase(new TestCase().id("test-simple-4")
                .name("Many Values").hidden(false).input(sb.toString())
                .output(sb2.toString()).order(4));

            sb = new StringBuilder();
            sb2 = new StringBuilder();
            try (InputStream is = InitSetupSimpleTest.class.getResourceAsStream(
                "/in/simple-test/More_Values.in"); BufferedInputStream bis
                = new BufferedInputStream(is)) {
                byte[] buffer = new byte[2048];
                int len;
                while ((len = bis.read(buffer)) != -1) {
                    sb.append(new String(buffer, 0, len));
                }
            } catch (IOException ioe) {
                Logger.getLogger(InitSetupSimpleTest.class.getName()).log(
                    Level.SEVERE, "Cannot read input file", ioe);
            }
            try (InputStream is = InitSetupSimpleTest.class.getResourceAsStream(
                "/out/simple-test/More_Values.out"); BufferedInputStream bis
                = new BufferedInputStream(is)) {
                byte[] buffer = new byte[2048];
                int len;
                while ((len = bis.read(buffer)) != -1) {
                    sb2.append(new String(buffer, 0, len));
                }
            } catch (IOException ioe) {
                Logger.getLogger(InitSetupSimpleTest.class.getName()).log(
                    Level.SEVERE, "Cannot read output file", ioe);
            }
            simpleTest.addTestCase(new TestCase().id("test-simple-5")
                .name("More_Values").hidden(true).input(sb.toString())
                .output(sb2.toString()).order(5));
            simpleTest.getTestCases().stream().forEach((testCase)
                -> mongoTemplate.save(testCase));
            simpleTest.setCoeff(BigDecimal.valueOf(0.1));
            simpleTest.addCodeStarter(bashStarter);
            simpleTest.addCodeStarter(cStarter);
            simpleTest.addCodeStarter(cppStarter);
            simpleTest.addCodeStarter(csStarter);
            simpleTest.addCodeStarter(jsStarter);
            simpleTest.addCodeStarter(javaStarter);
            simpleTest.addCodeStarter(perlStarter);
            simpleTest.addCodeStarter(phpStarter);
            simpleTest.addCodeStarter(pythonStarter);
            simpleTest.addCodeStarter(python3Starter);
            simpleTest.addCodeStarter(rubyStarter);
            simpleTest.getCodeStarters().stream().forEach((starter)
                -> mongoTemplate.save(starter));

            mongoTemplate.save(simpleTest);
        }
    }
}
