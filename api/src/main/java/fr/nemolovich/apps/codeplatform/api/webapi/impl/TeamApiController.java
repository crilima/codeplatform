package fr.nemolovich.apps.codeplatform.api.webapi.impl;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.nemolovich.apps.codeplatform.api.model.Challenge;
import fr.nemolovich.apps.codeplatform.api.model.Team;
import fr.nemolovich.apps.codeplatform.api.model.User;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.ResultViewModel;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.TeamViewModel;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.UserViewModel;
import fr.nemolovich.apps.codeplatform.api.security.SecurityUtils;
import fr.nemolovich.apps.codeplatform.api.service.ChallengeService;
import fr.nemolovich.apps.codeplatform.api.service.TeamService;
import fr.nemolovich.apps.codeplatform.api.service.UserService;
import fr.nemolovich.apps.codeplatform.api.webapi.TeamApi;
import io.swagger.annotations.ApiParam;

import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Ionut on 08/10/2017.
 */
@Controller
public class TeamApiController implements TeamApi {

    private final ObjectMapper objectMapper;

    private final UserService userService;
    private final ChallengeService challengeService;
    private final TeamService teamService;

    private static final Logger LOGGER = LoggerFactory.getLogger(
        TeamApiController.class);

    public TeamApiController(ObjectMapper objectMapper, UserService userService,
        ChallengeService challengeService, TeamService teamService) {
        this.objectMapper = objectMapper;
        this.userService = userService;
        this.challengeService = challengeService;
        this.teamService = teamService;
    }

    @Override
    public ResponseEntity<TeamViewModel> deleteTeam(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "ID of the team to delete", required = true)
        @PathVariable("teamId") String teamId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        if (accept != null && accept.contains("application/json")) {
            Challenge challenge = challengeService.deleteTeamInChallenge(
                challengeId, teamId);
            if (challenge != null) {
                return ResponseEntity.ok().body(null);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
            }
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<Set<ResultViewModel>> getResultsByTeamId(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "ID of the requested team", required = true)
        @PathVariable("teamId") String teamId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        if (accept != null && accept.contains("application/json")) {
            Set<ResultViewModel> resultViewModels = challengeService
                .getAllResultViewModelsByChallengeIdAndTeamId(challengeId,
                    teamId);
            if (resultViewModels.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return ResponseEntity.ok(resultViewModels);
            }
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<TeamViewModel> getTeamByChallengeId(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "ID of the requested team", required = true)
        @PathVariable("teamId") String teamId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        if (accept != null && accept.contains("application/json")) {
            Team team = challengeService.getTeamInChallengeById(challengeId,
                teamId);
            if (team != null) {
                return ResponseEntity.ok().body(new TeamViewModel(team));
            }
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<TeamViewModel> getTeamByJoiningChallengeId(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "ID of the requested team", required = true)
        @PathVariable("teamId") String teamId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        if (accept != null && accept.contains("application/json")) {
            Challenge challenge = challengeService.joinTeamInChallenge(
                challengeId, teamId, SecurityUtils.getCurrentUserLogin());
            if (challenge != null) {
                return ResponseEntity.ok().body(new TeamViewModel(
                    teamService.getTeamById(teamId)));
            }
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<Set<TeamViewModel>> getTeamsByChallengeId(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @RequestParam(value = "filter", required = false) String filter,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        if (accept != null && accept.contains("application/json")) {
            Set<TeamViewModel> teamViewModels = challengeService
                .getTeamsRegisteredInChallenge(challengeId);
            if (filter != null && !filter.isEmpty()) {
                teamViewModels = teamViewModels.stream()
                    .filter(teamViewModel -> teamService
                        .isMemberOfTeamViewModel(teamViewModel,
                            SecurityUtils.getCurrentUserLogin()))
                    .collect(Collectors.toSet());
            }
            return ResponseEntity.ok(teamViewModels);
        }

        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<TeamViewModel> updateTeam(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "the team to update", required = true)
        @Valid @RequestBody TeamViewModel team,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        if (accept != null && accept.contains("application/json")) {
            Challenge challenge = challengeService.getChallengeById(
                challengeId);
            Team teamToVerify = challengeService.getTeamInChallenge(challenge,
                team.getId());
            if (teamToVerify != null
                && !(!teamToVerify.getName().equals(team.getName())
                && challengeService.hasTeamNameAlreadyInChallenge(challenge,
                    team.getName()))) {
                Team updated = teamService.updateTeamByViewModel(team);
                return ResponseEntity.ok(new TeamViewModel(updated));
            }
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<TeamViewModel> updateMyTeam(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "the team to update", required = true)
        @Valid @RequestBody TeamViewModel team,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        if (accept != null && accept.contains("application/json")) {
            Challenge challenge = challengeService.getChallengeById(
                challengeId);

            Team teamToVerify = challengeService.getTeamInChallenge(challenge,
                team.getId());
            User user = userService.getCurrentUser().orElse(null);
            if (teamToVerify != null && teamToVerify.getCreator().equals(user)
                && !(!teamToVerify.getName().equals(team.getName())
                && challengeService.hasTeamNameAlreadyInChallenge(challenge,
                    team.getName()))) {
                Team updated = teamService.updateTeamByViewModel(team);
                return ResponseEntity.ok(new TeamViewModel(updated));
            }
            return ResponseEntity.ok(new TeamViewModel(teamToVerify));
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<TeamViewModel> addTeam(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "the team to add", required = true)
        @Valid @RequestBody TeamViewModel teamViewModel,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        if (accept != null && accept.contains("application/json")) {
            Team team = teamService.addTeamByTeamViewModel(teamViewModel);
            Challenge challenge = challengeService.getChallengeById(
                challengeId);
            if (team != null && challenge != null) {
                challenge = challengeService.addTeamInChallenge(challenge,
                    team);
                if (challenge != null) {
                    return ResponseEntity.ok(new TeamViewModel(
                        challenge.getTeams().stream().filter(t
                            -> t.getName().equalsIgnoreCase(
                                teamViewModel.getName())).findFirst().get()
                    ));
                }
            }
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<TeamViewModel> removeUserFromTeam(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "ID of the team from which delete", required = true)
        @PathVariable("teamId") String teamId,
        @ApiParam(value = "ID of the user to remove", required = true)
        @PathVariable("userId") String userId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        if (accept != null && accept.contains("application/json")) {

            User user = userService.getUserByLogin(userId);
            if (user == null) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return ResponseEntity.ok(new TeamViewModel(
                teamService.removeUserFromTeam(teamId,
                    new UserViewModel(user))));
        }
        return ResponseEntity.badRequest().body(null);
    }
}
