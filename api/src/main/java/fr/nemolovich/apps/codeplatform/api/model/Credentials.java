package fr.nemolovich.apps.codeplatform.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 * Credentials
 */
@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.SpringCodegen",
    date = "2017-09-27T13:36:14.592+02:00")
public class Credentials {

    @JsonProperty("username")
    private String username = null;

    @JsonProperty("password")
    private String password = null;

    public Credentials username(String username) {
        this.username = username;
        return this;
    }

    /**
     * Get username
     *
     * @return username
     *
     */
    @ApiModelProperty(required = true, value = "")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Credentials password(String password) {
        this.password = password;
        return this;
    }

    /**
     * Get password
     *
     * @return password
     *
     */
    @ApiModelProperty(required = true, value = "")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Credentials credentials = (Credentials) o;
        return Objects.equals(this.username, credentials.username)
            && Objects.equals(this.password, credentials.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Credentials {\n");

        sb.append("    username: ").append(toIndentedString(username))
            .append("\n");
        sb.append("    password: ").append(toIndentedString(password))
            .append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     * 
     * @param o {@link Object}: Object to format.
     * @return {@link String}: String representation of object.
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
