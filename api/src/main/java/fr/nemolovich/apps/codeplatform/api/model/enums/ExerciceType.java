package fr.nemolovich.apps.codeplatform.api.model.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets ExerciceType
 */
public enum ExerciceType {

    FASTEST("fastest"),
    SHORTEST("shortest"),
    REVERSE("reverse");

    private final String value;

    ExerciceType(String value) {
        this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static ExerciceType fromValue(String text) {
        for (ExerciceType b : ExerciceType.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        return null;
    }
}
