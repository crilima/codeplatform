package fr.nemolovich.apps.codeplatform.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import fr.nemolovich.apps.codeplatform.api.model.User;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data MongoDB repository for the User entity.
 */
@Repository
public interface UserRepository extends MongoRepository<User, String> {

    List<User> findAllByActivatedIsFalseAndCreatedDateBefore(Instant dateTime);

    List<User> findAll();

    Optional<User> findOneByEmail(String email);

    Optional<User> findOneByLogin(String login);
}
