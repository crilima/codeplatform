package fr.nemolovich.apps.codeplatform.api.service;

import java.math.BigDecimal;
import java.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import fr.nemolovich.apps.codeplatform.api.model.CodeFile;
import fr.nemolovich.apps.codeplatform.api.model.Result;
import fr.nemolovich.apps.codeplatform.api.model.Submission;
import fr.nemolovich.apps.codeplatform.api.model.User;
import fr.nemolovich.apps.codeplatform.api.model.enums.Language;
import fr.nemolovich.apps.codeplatform.api.repository.CodeFileRepository;
import fr.nemolovich.apps.codeplatform.api.repository.SubmissionRepository;

/**
 * Service class for managing users.
 */
@Service
public class SubmissionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        SubmissionService.class);

    private final SubmissionRepository submissionRepository;
    private final CodeFileRepository codeFileRepository;

    public SubmissionService(SubmissionRepository submissionRepository,
        CodeFileRepository codeFieRepository) {
        this.submissionRepository = submissionRepository;
        this.codeFileRepository = codeFieRepository;
    }

    public Submission saveSubmission(Submission submission) {
        return this.submissionRepository.save(submission);
    }

    public Submission addCodeFile(Submission submission, CodeFile codeFile) {
        codeFile = this.codeFileRepository.save(codeFile);
        return submission.addFileItem(codeFile);
    }

    public Submission removeCodeFile(Submission submission, CodeFile codeFile) {
        this.codeFileRepository.delete(codeFile);
        return submission.removeFileItem(codeFile);
    }

    public Submission getSubmissionByResultForUser(Result result,
        Language language, User user) {
        Submission submission = result.getSubmissions()
            .stream().filter(s
                -> s.getUser().equals(user) && s.getLanguage().equals(language))
            .findFirst().orElse(null);
        if (submission == null) {
            submission = new Submission().language(language).user(user)
                .score(BigDecimal.ONE.negate()).date(Instant.now());
            submission = this.saveSubmission(submission);
        }
        return submission;
    }

}
