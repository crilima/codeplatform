package fr.nemolovich.apps.codeplatform.api.model.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets SubmissionType
 */
public enum SubmissionType {

    ZIP("zip"),
    FILES("files");

    private String value;

    SubmissionType(String value) {
        this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static SubmissionType fromValue(String text) {
        for (SubmissionType b : SubmissionType.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        return null;
    }
}
