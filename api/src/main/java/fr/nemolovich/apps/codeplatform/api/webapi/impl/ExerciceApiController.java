package fr.nemolovich.apps.codeplatform.api.webapi.impl;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.nemolovich.apps.codeplatform.api.model.Challenge;
import fr.nemolovich.apps.codeplatform.api.model.Exercice;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.ExerciceViewModel;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.ResultViewModel;
import fr.nemolovich.apps.codeplatform.api.service.ChallengeService;
import fr.nemolovich.apps.codeplatform.api.service.ExerciceService;
import fr.nemolovich.apps.codeplatform.api.service.ResultService;
import fr.nemolovich.apps.codeplatform.api.webapi.ExerciceApi;
import fr.nemolovich.apps.codeplatform.manager.docker.Manager;
import io.swagger.annotations.ApiParam;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Ionut on 08/10/2017.
 */
@Controller
public class ExerciceApiController implements ExerciceApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        ExerciceApiController.class);

    private final ObjectMapper objectMapper;
    private final ExerciceService exerciceService;
    private final ChallengeService challengeService;
    private final ResultService resultService;

    public ExerciceApiController(ObjectMapper objectMapper,
        ExerciceService exerciceService, ChallengeService challengeService,
        ResultService resultService) {
        this.objectMapper = objectMapper;
        this.exerciceService = exerciceService;
        this.challengeService = challengeService;
        this.resultService = resultService;
    }

    @Override
    public ResponseEntity<Exercice> addExercice(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "The exercice to create", required = true)
        @Valid @RequestBody Exercice exercice,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("addExercice Rest");
        if (accept != null && accept.contains("application/json")) {
            LOGGER.debug("REST request to save addExercice : {}", exercice);
            Exercice createdExercice;
            if (exercice.getId() != null) {
                LOGGER.info("Exercice id is given, we will use this one and add it to challenge");
                createdExercice = exerciceService.getExerciceById(
                    exercice.getId());
            } else {
                LOGGER.info("Exercice will be created");
                createdExercice = exerciceService.createExercice(exercice);
            }
            Challenge challenge = challengeService.addExerciceInChallenge(
                challengeService.getChallengeById(challengeId),
                createdExercice);
            LOGGER.debug("Added Exercic to Challenge:[{}]", challenge);
            if (challenge != null) {
                return ResponseEntity.ok(createdExercice);
            }
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<Exercice> deleteExercice(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "ID of the exercice to delete", required = true)
        @PathVariable("exerciceId") String exerciceId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("deleteChallenge challengeId:{}", challengeId);
        if (accept != null && accept.contains("application/json")
            && challengeId != null) {
            LOGGER.debug("REST request to delete exercice : {} from challeng : {}",
                exerciceId, challengeId);
            // We just delete the exercice from challenge, don't delete it for real
            Challenge challenge = challengeService.deleteExercicesInChallenge(
                challengeId, exerciceId);
            if (challenge != null) {
                return ResponseEntity.ok(null);
            }
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<Set<ExerciceViewModel>> getAllExercicesByChallengeId(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {

        if (accept != null && accept.contains("application/json")) {
            Challenge challenge = challengeService.getChallengeById(
                challengeId);
            if (challenge == null) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
            } else {
                return ResponseEntity.ok(challenge.getExercices().stream()
                    .map(ex -> new ExerciceViewModel(ex))
                    .collect(Collectors.toSet()));
            }
        }
        return ResponseEntity.badRequest()
            .body(null);
    }

    @Override
    public ResponseEntity<Set<ResultViewModel>> getResultsByExerciceId(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "ID of the requested exercice", required = true)
        @PathVariable("exerciceId") String exerciceId,
        @ApiParam(value = "ID of the team to get results")
        @Valid @RequestParam(value = "team", required = false) String team,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("getResultsByExerciceId exerciceId:{}", exerciceId);
        if (accept != null && accept.contains("application/json")) {
            // Verify if the exercice is inside a challenge
            Exercice exercice = challengeService.getExerciceInChallengeById(
                challengeId, exerciceId);
            if (exercice != null) {
                Set<ResultViewModel> resultViewModels = resultService
                    .getResultViewModelsByExerciceId(exerciceId);
                if (team != null && !team.isEmpty()) {
                    resultViewModels = resultViewModels.stream().filter(
                        resultViewModel -> resultViewModel.getTeam()
                        .getName().equalsIgnoreCase(team))
                        .collect(Collectors.toSet());
                }
                // return Result of this specific Exercice
                return ResponseEntity.ok(resultViewModels);
            }
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<Exercice> updateExercice(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "the exercice to update", required = true)
        @Valid
        @RequestBody Exercice exercice,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        LOGGER.debug("update exercice exerciceId:{}", exercice);
        if (accept != null && accept.contains("application/json")) {
            // If creation
            if (exercice.getId() == null) {
                return addExercice(challengeId, exercice, accept);
            } else {
                Exercice exerciceTest = challengeService
                    .getExerciceInChallengeById(challengeId, exercice.getId());
                Challenge challenge = challengeService.getChallengeById(
                    challengeId);
                // Exercice is an exercice of the challenge
                // And we will not update with the same title
                if (exerciceTest != null && !challengeService
                    .hasExerciceTitleAlreadyInChallenge(challenge,
                        exercice.getTitle())) {
                    Exercice updatedExercice = exerciceService.updateExercice(
                        exercice);
                    return ResponseEntity.ok(updatedExercice);
                }
            }
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<Exercice> getExerciceById(
        @ApiParam(value = "ID of the requested challenge", required = true)
        @PathVariable("challengeId") String challengeId,
        @ApiParam(value = "ID of the requested exercice", required = true)
        @PathVariable("exerciceId") String exerciceId,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        if (accept != null && accept.contains("application/json")) {
            // Verify if the exercice is inside a challenge
            Exercice exercice = challengeService.getExerciceInChallengeById(
                challengeId, exerciceId);
            if (exercice != null) {
                return ResponseEntity.ok(Exercice.publicExercice(exercice));
            }
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.badRequest().body(null);
    }

    @Override
    public ResponseEntity<Set> listLanguages(
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception {
        if (accept != null && accept.contains("application/json")) {
            return ResponseEntity.ok(new HashSet(Manager.availableLanguages()));
        }
        return ResponseEntity.badRequest().body(null);
    }
}
