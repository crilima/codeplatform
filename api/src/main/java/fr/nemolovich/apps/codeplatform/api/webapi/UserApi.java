package fr.nemolovich.apps.codeplatform.api.webapi;


import fr.nemolovich.apps.codeplatform.api.model.Role;
import fr.nemolovich.apps.codeplatform.api.model.User;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.UserRegisterModel;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.UserViewModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import java.util.Set;
import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * User API
 */
@Api(value = "User API", description = "The User API")
public interface UserApi {

    @ApiOperation(value = "Create a new user", notes = "",
        response = User.class, tags = {"admin", "user",})
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Creation Succeed",
            response = UserViewModel.class),
        @ApiResponse(code = 401, message = "Unauthorized",
            response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/admin/users",
        produces = {"application/json"},
        consumes = {"application/json"},
        method = RequestMethod.POST)
    ResponseEntity<UserViewModel> addUser(
        @ApiParam(value = "The user to create", required = true)
        @Valid @RequestBody User user,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Delete an user", notes = "",
        response = UserViewModel.class, tags = {"admin", "user",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Deletion Succeed",
            response = UserViewModel.class),
        @ApiResponse(code = 204, message = "No Content"),
        @ApiResponse(code = 401, message = "Unauthorized",
            response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/admin/users/{login}",
        produces = {"application/json"},
        method = RequestMethod.DELETE)
    ResponseEntity<Object> deleteUser(
        @ApiParam(value = "login", required = true)
        @PathVariable("login") String login,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Return current user", notes = "",
        response = UserViewModel.class, tags = {"account",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK",
            response = UserViewModel.class),
        @ApiResponse(code = 401, message = "Unauthorized"),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/account",
        produces = {"application/json"},
        method = RequestMethod.GET)
    ResponseEntity<UserViewModel> getAccount(
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Get all users", notes = "",
        response = UserViewModel.class, responseContainer = "Set",
        tags = {"user",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The existing users",
            response = UserViewModel.class, responseContainer = "Set"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/users",
        produces = {"application/json"},
        method = RequestMethod.GET)
    ResponseEntity<Set<UserViewModel>> getAllUsers(
        @ApiParam(value = "Page number of the requested page")
        @Valid @RequestParam(value = "page", required = false) Integer page,
        @ApiParam(value = "Size of a page")
        @Valid @RequestParam(value = "size", required = false) Integer size,
        @ApiParam(value = "Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.")
        @Valid @RequestParam(value = "sort", required = false) List<String> sort,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Get current user roles", notes = "",
        response = String.class, responseContainer = "Set", tags = {"account",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK", response = String.class,
            responseContainer = "Set"),
        @ApiResponse(code = 401, message = "Unauthorized"),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/account/roles",
        produces = {"application/json"},
        method = RequestMethod.GET)
    ResponseEntity<Set<Role>> getRoles(
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Get an user by login", notes = "",
        response = UserViewModel.class, tags = {"user",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The corresponding user",
            response = UserViewModel.class),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/users/{login}",
        produces = {"application/json"},
        method = RequestMethod.GET)
    ResponseEntity<UserViewModel> getUserByID(
        @ApiParam(value = "login", required = true)
        @PathVariable("login") String login,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Create a new user", notes = "",
        response = UserViewModel.class, tags = {"account",})
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Creation Succeed",
            response = UserViewModel.class),
        @ApiResponse(code = 401, message = "Unauthorized",
            response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/register",
        produces = {"application/json", "text/plain"},
        consumes = {"application/json"},
        method = RequestMethod.POST)
    ResponseEntity<UserViewModel> registerAccount(
        @ApiParam(value = "The user request to register", required = true)
        @Valid @RequestBody UserRegisterModel userRegisterModel,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Change current password", notes = "",
        response = UserViewModel.class, tags = {"account",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Update Succeed",
            response = UserViewModel.class),
        @ApiResponse(code = 401, message = "Unauthorized",
            response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/account/password",
        produces = {"application/json"},
        consumes = {"application/json"},
        method = RequestMethod.PUT)
    ResponseEntity<UserViewModel> changePassword(
        @ApiParam(value = "The user to upate", required = true)
        @Valid @RequestBody UserRegisterModel userRegisterModel,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Update current user", notes = "",
        response = UserViewModel.class, tags = {"account",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Update Succeed",
            response = UserViewModel.class),
        @ApiResponse(code = 401, message = "Unauthorized",
            response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/account",
        produces = {"application/json"},
        consumes = {"application/json"},
        method = RequestMethod.PUT)
    ResponseEntity<UserViewModel> updateAccount(
        @ApiParam(value = "UserViewModel", required = true)
        @Valid @RequestBody UserViewModel userViewModel,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "Update an user", notes = "",
        response = UserViewModel.class, tags = {"admin", "user",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Update Succeed",
            response = UserViewModel.class),
        @ApiResponse(code = 204, message = "No Content"),
        @ApiResponse(code = 401, message = "Unauthorized",
            response = Object.class),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/admin/users",
        produces = {"application/json"},
        consumes = {"application/json"},
        method = RequestMethod.PUT)
    ResponseEntity<User> updateUser(
        @ApiParam(value = "UserViewModel", required = true)
        @Valid @RequestBody User user,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

}
