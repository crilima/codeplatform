package fr.nemolovich.apps.codeplatform.api.webapi.exception;

@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.SpringCodegen",
    date = "2017-10-07T19:30:45.309+02:00")
public class ApiException extends Exception {

    private final int code;

    public ApiException(int code, String msg) {
        super(msg);
        this.code = code;
    }
}
