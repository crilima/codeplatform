package fr.nemolovich.apps.codeplatform.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import fr.nemolovich.apps.codeplatform.api.model.Exercice;

/**
 * Spring Data MongoDB repository for the Exercice entity.
 */
@Repository
public interface ExerciceRepository extends MongoRepository<Exercice, String> {

}
