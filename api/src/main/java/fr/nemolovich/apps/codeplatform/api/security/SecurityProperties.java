package fr.nemolovich.apps.codeplatform.api.security;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Security.
 * <p>
 * Properties are configured in the application.yml file.
 */
@ConfigurationProperties(prefix = "security", ignoreUnknownFields = false)
public class SecurityProperties {

    private final SecurityProperties.Authentication authentication
        = new SecurityProperties.Authentication();

    public SecurityProperties.Authentication getAuthentication() {
        return this.authentication;
    }

    public static class Authentication {

        private final SecurityProperties.Authentication.Oauth oauth
            = new SecurityProperties.Authentication.Oauth();
        private final SecurityProperties.Authentication.Jwt jwt
            = new SecurityProperties.Authentication.Jwt();

        public Authentication() {
        }

        public SecurityProperties.Authentication.Oauth getOauth() {
            return this.oauth;
        }

        public SecurityProperties.Authentication.Jwt getJwt() {
            return this.jwt;
        }

        public static class Jwt {

            private String secret;
            private long tokenValidityInSeconds = 1800L;

            public Jwt() {
            }

            public String getSecret() {
                return this.secret;
            }

            public void setSecret(String secret) {
                this.secret = secret;
            }

            public long getTokenValidityInSeconds() {
                return this.tokenValidityInSeconds;
            }

            public void setTokenValidityInSeconds(long tokenValidityInSeconds) {
                this.tokenValidityInSeconds = tokenValidityInSeconds;
            }

        }

        public static class Oauth {

            private String clientId;
            private String clientSecret;
            private int tokenValidityInSeconds = 1800;

            public Oauth() {
            }

            public String getClientId() {
                return this.clientId;
            }

            public void setClientId(String clientId) {
                this.clientId = clientId;
            }

            public String getClientSecret() {
                return this.clientSecret;
            }

            public void setClientSecret(String clientSecret) {
                this.clientSecret = clientSecret;
            }

            public int getTokenValidityInSeconds() {
                return this.tokenValidityInSeconds;
            }

            public void setTokenValidityInSeconds(int tokenValidityInSeconds) {
                this.tokenValidityInSeconds = tokenValidityInSeconds;
            }
        }
    }

}
