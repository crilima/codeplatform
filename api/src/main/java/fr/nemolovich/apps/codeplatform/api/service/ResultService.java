package fr.nemolovich.apps.codeplatform.api.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import fr.nemolovich.apps.codeplatform.api.model.Exercice;
import fr.nemolovich.apps.codeplatform.api.model.Result;
import fr.nemolovich.apps.codeplatform.api.model.Submission;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.ResultViewModel;
import fr.nemolovich.apps.codeplatform.api.repository.ResultRepository;

/**
 * Service class for managing users.
 */
@Service
public class ResultService {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        ResultService.class);

    private final ChallengeService challengeService;
    private final ExerciceService exerciceService;
    private final TeamService teamService;
    private final ResultRepository resultRepository;
    private final SubmissionService submissionService;

    public ResultService(UserService userRepository,
        ExerciceService exerciceService, ResultRepository resultRepository,
        ChallengeService challengeService, TeamService teamService,
        SubmissionService submissionService) {
        this.exerciceService = exerciceService;
        this.resultRepository = resultRepository;
        this.challengeService = challengeService;
        this.teamService = teamService;
        this.submissionService = submissionService;
    }

    public Set<Result> getResultsByExerciceId(String exerciceId) {
        LOGGER.debug("GetResults By ExerciceId: {}", exerciceId);
        Exercice exercice = exerciceService.getExerciceById(exerciceId);
        if (exercice != null) {
            return resultRepository.findByExercice(exercice);
        }
        return null;
    }

    public Set<Result> getResultsByChallengeIdAdnExerciceId(String challengeId,
        String exerciceId) {
        LOGGER.debug("GetResults By ChallengeId: {} and ExerciceId: {}",
            challengeId, exerciceId);
        Set<Result> results = challengeService.getAllResultsByChallengeId(
            challengeId);
        Exercice exercice = exerciceService.getExerciceById(exerciceId);
        if (exercice != null) {
            return results.stream().filter((r) -> r.getExercice()
                .equals(exercice)).collect(Collectors.toSet());
        }
        return null;
    }

    public Result getResultForTeamInChallenge(String challengeId,
        String exerciceId, String teamId) {
        LOGGER.debug(
            "GetResult By ChallengeId: {} and ExerciceId: {} and TeamId: {}",
            challengeId, exerciceId, teamId);
        Set<Result> results = challengeService
            .getAllResultsByChallengeIdAndTeamId(challengeId, teamId);
        Exercice exercice = exerciceService.getExerciceById(exerciceId);
        Result result = null;
        if (exercice != null) {
            result = results.stream().filter((r) -> r.getExercice()
                .equals(exercice)).findFirst().orElse(null);
            if (result == null) {
                result = new Result().exercice(exercice).team(teamService
                    .getTeamById(teamId)).submissions(new ArrayList<>());
                result = resultRepository.save(result);
            }
        }
        return result;
    }

    public Set<ResultViewModel> getResultViewModelsByExerciceId(
        String exerciceId) {
        LOGGER.debug("GetResults By ExerciceId: {}", exerciceId);
        Set<Result> results = getResultsByExerciceId(exerciceId);
        if (results != null) {
            return results.stream().map(ResultViewModel::new).collect(
                Collectors.toSet());
        }
        return new HashSet<>();
    }

    public Result addSubmission(Result result, Submission submission) {
        if (result != null && submission != null) {
            return resultRepository.save(result.addSubmissionItem(submission));
        } else {
            LOGGER.error("result or submission is null, verify data");
            return null;
        }
    }

}
