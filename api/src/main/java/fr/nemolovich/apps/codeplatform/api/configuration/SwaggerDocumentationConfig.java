package fr.nemolovich.apps.codeplatform.api.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.SpringCodegen",
    date = "2017-10-07T19:30:45.309+02:00")
@Configuration
public class SwaggerDocumentationConfig {

    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("CodePlatform Swagger")
            .description("CodePlatform REST API  ")
            .license("GNU General Public License v3.0")
            .licenseUrl("https://www.gnu.org/licenses/gpl-3.0")
            .termsOfServiceUrl("")
            .version("0.1.1")
            .contact(new Contact("", "", "brian.gohier@hotmail.fr;dan.geffroy@gmail.com;adrien.wattez@gmail.com;ionut.iortoman@gmail.com"))
            .build();
    }

    @Bean
    public Docket customImplementation() {
        return new Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.basePackage("fr.nemolovich.apps.codeplatform.api.webapi"))
            .build()
            .directModelSubstitute(org.threeten.bp.LocalDate.class,
                java.sql.Date.class)
            .directModelSubstitute(org.threeten.bp.OffsetDateTime.class,
                java.util.Date.class)
            .apiInfo(apiInfo());
    }

}
