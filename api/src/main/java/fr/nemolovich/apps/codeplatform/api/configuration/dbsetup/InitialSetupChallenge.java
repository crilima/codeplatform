package fr.nemolovich.apps.codeplatform.api.configuration.dbsetup;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;

import fr.nemolovich.apps.codeplatform.api.model.Challenge;
import fr.nemolovich.apps.codeplatform.api.model.Exercice;
import fr.nemolovich.apps.codeplatform.api.model.Result;
import fr.nemolovich.apps.codeplatform.api.model.Team;
import fr.nemolovich.apps.codeplatform.api.model.enums.ChallengeScope;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;

/**
 * Created by awattez on 10/10/2017.
 */
/**
 * Creates the initial database setup for user
 */
@ChangeLog(order = "005")
@Profile(value = "dev")
public class InitialSetupChallenge {

    @ChangeSet(order = "01", author = "initiator", id = "01-addChallenges")
    public void addChallenges(MongoTemplate mongoTemplate) {

        Exercice exercice1 = mongoTemplate.findOne(new BasicQuery(
            "{ title : 'First simple test' }"), Exercice.class);
        Exercice exercice2 = mongoTemplate.findOne(new BasicQuery(
            "{ title : 'Title exercice2' }"), Exercice.class);
        Exercice exercice3 = mongoTemplate.findOne(new BasicQuery(
            "{ title : 'Title exercice3' }"), Exercice.class);

        Team team0 = mongoTemplate.findOne(new BasicQuery(
            "{ id : 'team-0' }"), Team.class);
        Team team1 = mongoTemplate.findOne(new BasicQuery(
            "{ id : 'team-1' }"), Team.class);
        Team team2 = mongoTemplate.findOne(new BasicQuery(
            "{ id : 'team-2' }"), Team.class);
        Team team3 = mongoTemplate.findOne(new BasicQuery(
            "{ id : 'team-3' }"), Team.class);

        Result result0 = mongoTemplate.findOne(new BasicQuery(
            "{ id : 'result-000' }"), Result.class);
        Result result1 = mongoTemplate.findOne(new BasicQuery(
            "{ id : 'result-001' }"), Result.class);
        Result result2 = mongoTemplate.findOne(new BasicQuery(
            "{ id : 'result-002' }"), Result.class);
        Result result3 = mongoTemplate.findOne(new BasicQuery(
            "{ id : 'result-003' }"), Result.class);
        Result result4 = mongoTemplate.findOne(new BasicQuery(
            "{ id : 'result-004' }"), Result.class);
        Result result5 = mongoTemplate.findOne(new BasicQuery(
            "{ id : 'result-005' }"), Result.class);
        Result result6 = mongoTemplate.findOne(new BasicQuery(
            "{ id : 'result-006' }"), Result.class);

        Challenge challenge1 = new Challenge();
        challenge1.setId("challenge-0");
        challenge1.setDescription("Challenge Description");
        challenge1.setScope(ChallengeScope.CHALLENGE);
        challenge1.setName("challenge1 name");
        // StartDate Now and end in 1 day
        challenge1.setEndDate(Instant.now().plus(1, ChronoUnit.DAYS));
        challenge1.setStartDate(Instant.now());
        challenge1.addExerciceItem(exercice1).addExerciceItem(exercice3);
        challenge1.addTeamItem(team0).addTeamItem(team2);
        // team0 - results
        challenge1.addResultItem(result0).addResultItem(result1);
        // team2 - results
        challenge1.addResultItem(result3).addResultItem(result5);
        mongoTemplate.save(challenge1);

        Challenge challenge2 = new Challenge();
        challenge2.setId("challenge-1");
        challenge2.setDescription("Challenge Description");
        challenge2.setScope(ChallengeScope.COURSE);
        challenge2.setName("challenge2 name");
        // StartDate Now and end in 7 day
        challenge2.setEndDate(Instant.now().plus(7, ChronoUnit.DAYS));
        challenge2.setStartDate(Instant.now());
        challenge2.addExerciceItem(exercice2);
        // team1 - results
        challenge2.addResultItem(result2);
        // team2 - results
        challenge2.addResultItem(result4);
        // team3 - results
        challenge2.addResultItem(result6);
        challenge2.addTeamItem(team1).addTeamItem(team2).addTeamItem(team3);
        mongoTemplate.save(challenge2);
    }

}
