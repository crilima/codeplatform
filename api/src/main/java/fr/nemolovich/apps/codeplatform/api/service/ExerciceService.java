package fr.nemolovich.apps.codeplatform.api.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import fr.nemolovich.apps.codeplatform.api.model.Exercice;
import fr.nemolovich.apps.codeplatform.api.model.Result;
import fr.nemolovich.apps.codeplatform.api.repository.ExerciceRepository;
import fr.nemolovich.apps.codeplatform.api.repository.ResultRepository;

import java.util.List;
import java.util.Set;

/**
 * Service class for managing users.
 */
@Service
public class ExerciceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(
        ExerciceService.class);

    private final ResultRepository resultRepository;
    private final ExerciceRepository exerciceRepository;

    public ExerciceService(UserService userRepository,
                           ExerciceRepository exerciceRepository,
                           ResultRepository resultRepository) {
        this.exerciceRepository = exerciceRepository;
        this.resultRepository = resultRepository;
    }

    public Exercice getExerciceById(String id) {
        return exerciceRepository.findOne(id);
    }

    public Exercice createExercice(Exercice exercice) {
        LOGGER.debug("Create Exercice:[{}]", exercice.toString());
        Exercice exerciceSaved = exerciceRepository.save(exercice);
        LOGGER.debug("Created exercice: {}", exerciceSaved);
        return exerciceSaved;
    }

    public Exercice updateExercice(Exercice exercice) {
        LOGGER.debug("Update Exercice:[{}]", exercice.toString());
        Exercice exerciceUpdated = exerciceRepository.save(exercice);
        LOGGER.debug("Updated exercice: {}", exerciceUpdated);
        return exerciceUpdated;
    }

    public List<Exercice> getAllExercices() {
        return exerciceRepository.findAll();
    }

    public void deleteExercice(String id) {
        Exercice exercice = exerciceRepository.findOne(id);
        exerciceRepository.delete(exercice);
        LOGGER.debug("Deleted exercice: {}", exercice);
    }

    public Set<Result> getResultsByExerciceId(String exerciceId) {
        LOGGER.debug("GetResults By ExerciceId: {}", exerciceId);
        Exercice exercice = exerciceRepository.findOne(exerciceId);
        Set<Result> results = resultRepository.findByExercice(exercice);
        return results;
    }


}
