package fr.nemolovich.apps.codeplatform.api.webapi;


import fr.nemolovich.apps.codeplatform.api.model.viewmodel.LoginViewModel;
import fr.nemolovich.apps.codeplatform.api.model.viewmodel.UserViewModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.SpringCodegen",
    date = "2017-10-01T18:58:34.924+02:00")
@Api(value = "api", description = "The JWT API")
public interface JWTAccessApi {

    @ApiOperation(value = "Activate user account", notes = "",
        response = UserViewModel.class, tags = {"user-access-activation",
            "admin"})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK",
            response = UserViewModel.class),
        @ApiResponse(code = 401, message = "Unauthorized"),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/admin/activate",
        produces = {"application/json"},
        method = RequestMethod.GET)
    ResponseEntity<UserViewModel> activateAccount(
        @NotNull @ApiParam(value = "login", required = true)
        @Valid @RequestParam(value = "login", required = true) String login,
        @RequestHeader(value = "Accept", required = false) String accept)
        throws Exception;

    @ApiOperation(value = "authenticate", notes = "",
        response = UserViewModel.class, tags = {"user-access-activation",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK",
            response = UserViewModel.class),
        @ApiResponse(code = 401, message = "Unauthorized"),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/authenticate",
        produces = {"application/json"},
        consumes = {"application/json"},
        method = RequestMethod.POST)
    ResponseEntity<UserViewModel> authorize(
        @ApiParam(value = "loginVM", required = true)
        @Valid @RequestBody LoginViewModel loginVM,
        @RequestHeader(value = "Accept", required = false) String accept,
        HttpServletResponse response) throws Exception;

    @ApiOperation(value = "isAuthenticated", notes = "",
        response = UserViewModel.class, tags = {"user-access-activation",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK",
            response = UserViewModel.class),
        @ApiResponse(code = 401, message = "Unauthorized"),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found")})
    @RequestMapping(value = "/api/authenticate",
        produces = {"application/json"},
        method = RequestMethod.GET)
    ResponseEntity<UserViewModel> isAuthenticated(
        @RequestHeader(value = "Accept", required = false) String accept,
        HttpServletRequest httpServletRequest) throws Exception;
}
