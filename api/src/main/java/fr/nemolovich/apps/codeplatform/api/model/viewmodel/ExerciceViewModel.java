package fr.nemolovich.apps.codeplatform.api.model.viewmodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.nemolovich.apps.codeplatform.api.model.Exercice;
import fr.nemolovich.apps.codeplatform.api.model.enums.ExerciceType;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.data.annotation.Id;
import org.springframework.validation.annotation.Validated;

/**
 * Exercice
 */
@Validated
@javax.annotation.Generated(
    value = "bgohier",
    date = "2017-10-27T12:326:00.000+02:00")
public class ExerciceViewModel {

    @JsonProperty("id")
    @Id
    private final String id;

    @JsonProperty("title")
    @Size(min = 5, max = 50)
    @NotNull
    private final String title;

    @JsonProperty("subtitle")
    @Size(min = 0, max = 100)
    private final String subtitle;

    @JsonProperty("text")
    private final String text;

    @JsonProperty("type")
    private final ExerciceType type;

    @JsonProperty("coeff")
    private BigDecimal coeff = null;

    public ExerciceViewModel(Exercice exercice) {
        this.id = exercice.getId();
        this.title = exercice.getTitle();
        this.subtitle = exercice.getSubtitle();
        this.text = exercice.getText();
        this.type = exercice.getType();
        this.coeff = exercice.getCoeff();
    }

    /**
     * Get id
     *
     * @return id
     *
     */
    @ApiModelProperty(value = "")
    public String getId() {
        return id;
    }

    /**
     * Get title
     *
     * @return title
     *
     */
    @ApiModelProperty(value = "")
    public String getTitle() {
        return title;
    }

    /**
     *
     * Get type
     *
     * @return type
     */
    @ApiModelProperty(value = "")
    public ExerciceType getType() {
        return type;
    }

    /**
     * Get subtitle
     *
     * @return subtitle
     *
     */
    @ApiModelProperty(value = "")
    public String getSubtitle() {
        return subtitle;
    }

    /**
     * Get text
     *
     * @return text
     *
     */
    @ApiModelProperty(value = "")
    public String getText() {
        return text;
    }

    public ExerciceViewModel coeff(BigDecimal coeff) {
        this.coeff = coeff;
        return this;
    }

    /**
     * Get coeff
     *
     * @return coeff
     *
     */
    @ApiModelProperty(value = "")
    public BigDecimal getCoeff() {
        return coeff;
    }

    public void setCoeff(BigDecimal coeff) {
        this.coeff = coeff;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ExerciceViewModel exercice = (ExerciceViewModel) o;
        return Objects.equals(this.id, exercice.id)
            && Objects.equals(this.title, exercice.title)
            && Objects.equals(this.type, exercice.type)
            && Objects.equals(this.subtitle, exercice.subtitle)
            && Objects.equals(this.text, exercice.text)
            && Objects.equals(this.coeff, exercice.coeff);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, type, subtitle, text, coeff);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Exercice {\n");
        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    title: ").append(toIndentedString(title)).append("\n");
        sb.append("    type: ").append(toIndentedString(type)).append("\n");
        sb.append("    subtitle: ").append(toIndentedString(subtitle))
            .append("\n");
        sb.append("    text: ").append(toIndentedString(text)).append("\n");
        sb.append("    coeff: ").append(toIndentedString(coeff)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     * 
     * @param o {@link Object}: Object to format.
     * @return {@link String}: String representation of object.
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
