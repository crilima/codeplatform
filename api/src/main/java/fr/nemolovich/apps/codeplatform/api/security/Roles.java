package fr.nemolovich.apps.codeplatform.api.security;

/**
 * Created by awattez on 02/10/2017.
 */
public enum Roles {

    ADMIN("ROLE_ADMIN"),
    USER("ROLE_USER"),
    ANONYMOUS("ROLE_ANONYMOUS"),
    RECRUITER("ROLE_RECRUITER");

    private final String value;

    private Roles(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
