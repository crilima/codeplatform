package fr.nemolovich.apps.codeplatform.api;

import java.net.InetAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;

import fr.nemolovich.apps.codeplatform.api.security.SecurityProperties;
import fr.nemolovich.apps.codeplatform.manager.docker.Manager;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableConfigurationProperties({SecurityProperties.class})
@ComponentScan(basePackages = { "fr.nemolovich.apps.codeplatform.api", "fr.nemolovich.apps.codeplatform.api.webapi" })
public class Swagger2SpringBoot implements CommandLineRunner {
    private static final Logger LOGGER = LoggerFactory.getLogger(Swagger2SpringBoot.class);


    @Override
    public void run(String... arg0) throws Exception {
        if (arg0.length > 0 && arg0[0].equals("exitcode")) {
            throw new ExitException();
        }
    }

    public static void main(String[] args) throws Exception {
        SpringApplication app = new SpringApplication(Swagger2SpringBoot.class);
        Environment env = app.run(args).getEnvironment();
        LOGGER.info("\nApplication '{}' is running! Access URLs:\n" +
                        "Local: \t\thttps://localhost:{}{}\n" +
                        "External: \thttps://{}:{}{}\n",
                env.getProperty("spring.application.name"),
                env.getProperty("server.port"),
                env.getProperty("server.contextPath"),
                InetAddress.getLocalHost().getHostAddress(),
                env.getProperty("server.port"),
                env.getProperty("server.contextPath"));
        Manager.init();
    }

    class ExitException extends RuntimeException implements ExitCodeGenerator {
        private static final long serialVersionUID = 1L;

        @Override
        public int getExitCode() {
            return 10;
        }

    }
}
