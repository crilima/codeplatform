#!/bin/bash

PARAM="$1"
DEPLOY=false
BUILD=false
ENV="prod"

BASE_HREF="${BASE_HREF:-/}"

if [ "${PARAM}" = "--deploy" ] ; then
    DEPLOY=true
elif [ "${PARAM}" = "--build" ] ; then
    BUILD=true
    if [ ! -z "$2" ] ; then
        ENV="$2"
    fi
elif [ "${PARAM}" = "--debug" ] ; then
    /bin/bash
    exit 0
else
    echo "ERROR: incorrect parameters '$*'"
    exit 1
fi

if [ ! -z "${API_URL}" ] ; then
    sed -i ':a;N;$!ba;s!\(provide: BASE_PATH,\n\s\+useValue: '"'"'\)https://.\+'"'"'!\1'"${API_URL}'"'!' ${APP_PATH}/src/app/app.module.https.ts
    sed -i ':a;N;$!ba;s!\(provide: APP_BASE_HREF,\n\s\+useValue: '"'"'\)/titan/'"'"'!\1'"${BASE_HREF}'"'!' ${APP_PATH}/src/app/app.module.https.ts
    sed -i ':a;N;$!ba;s!\(provide: BASE_PATH,\n\s\+useValue: '"'"'\)http://.\+'"'"'!\1'"${API_URL}'"'!' ${APP_PATH}/src/app/app.module.ts
    sed -i ':a;N;$!ba;s!\(provide: APP_BASE_HREF,\n\s\+useValue: '"'"'\)/titan/'"'"'!\1'"${BASE_HREF}'"'!' ${APP_PATH}/src/app/app.module.ts
fi
fi

if [ -z "$(ls ${CERT_PATH} 2>/dev/null)" ] ;then
    cp -rp ${CERT_PATH_BACK}/* ${CERT_PATH}/
fi

npm list -g @angular/cli 1>/dev/null 2>&1
exists=$?
if [ ${exists} -ne 0 ] ; then
    mkdir -p /usr/local/lib/node_modules/@angular/cli/node_modules/node-sass/.node-gyp/8.9.0
    timeout 60 npm install -g @angular/cli
    if [ $? -eq 124 ] ; then
        mkdir -p /usr/local/lib/node_modules/@angular/cli/node_modules/node-sass/.node-gyp/8.9.0
        timeout 60 npm install -g @angular/cli
    fi
fi
if ${DEPLOY} ; then
    ng serve --base-href "${BASE_HREF}" --deploy-url "${BASE_HREF}" --disable-host-check true \
      --ssl 1 --ssl-key "${CERT_PATH}/key.pem" --ssl-cert "${CERT_PATH}/cert.pem"
elif ${BUILD} ; then
    DIST_PATH=${DIST_PATH:-/tmp/null}
    rm -rf ${DIST_PATH}/*
    ng build --env=${ENV} --base-href ${BASE_HREF}
    chmod -R 777 ${APP_PATH}/dist/
    cp -rp ${APP_PATH}/dist/* ${DIST_PATH}/
fi

exit 0
