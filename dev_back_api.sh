#!/bin/bash

function max()
{
    echo "$1
$2" | sort -r -n | head -n 1
}

function min()
{
    echo "$1
$2" | sort -n | head -n 1
}

function detect_changes()
{
    detect-changes -M $1 -m $2 -e "*/target/*" -e "*/LICENSE" \
      -e "*/README.md" -e "*/nb*.xml" -e "*/docker_images/*" \
      ${MANAGER_PATH} ${API_PATH} 2>/dev/null
}

function check_changes()
{
    local need_compile=false
    local need_compile_manager=false
    local timestamp=$(date +"%s")

    while true ; do
        ${need_compile} && local max_a=1 || local max_a=$(max 5 ${MTIME})
        ${need_compile} && local min_a=0 || local min_a=$(min 1 ${MTIME})
        local changes="$(detect_changes ${max_a} ${min_a})"
        local now=$(date +"%s")
        [ ! -z "${changes}" ] && changed=true || changed=false
        local manager_changed=$(echo "${changes}" | grep -Pq "^${MANAGER_PATH}.*$" && echo true || echo false)
        [ $[now-timestamp] -ge ${MTIME} ] && timeOK=true || timeOK=false
        if ${changed} ; then
            if ${need_compile} ; then
                echo "New changes:
${changes}"
                ${manager_changed} && need_compile_manager=true
                timestamp=$(date +"%s")
                echo "Compile in ${MTIME} seconds..."
                sleep 1
            else
                echo "Changes detected:
${changes}"
                need_compile=true
                ${manager_changed} && need_compile_manager=true
                echo "Compile in ${MTIME} seconds..."
                timestamp=$(date +"%s")
                sleep 1
            fi
        else
            if ${need_compile} && ${timeOK} ; then
                if ${need_compile_manager} ; then
                    compile_manager
                    need_compile_manager=false
                fi
                compile_api
                stop_server
                start_server
                need_compile=false
            else
                sleep 0.5
            fi
        fi
    done


    exit 0

}

function start_server() {
    echo "Starting server..."
    java ${DEBUG_OPTS} -Dmanager.config.path="${CONFIGS_PATH}/manager/manager.properties" \
      -Dlanguages.config.path="${CONFIGS_PATH}/manager/languages.yml" \
      -Dspring.config.location="file:${CONFIGS_PATH}/api/application.yaml" \
      -jar "${JAR_FILE}" &
    SERVER_PID="$!"
    SERVER_RUNNING=true
    echo "Server started"
}

function server_running()
{
    echo $(ps -eaf | grep -v grep | grep java | grep -qe " ${SERVER_PID} " ; echo $?)
}

function stop_server() {
    echo "Stopping server..."
    ${SERVER_RUNNING} && kill -15 ${SERVER_PID}
    tries=0
    MAX_TRIES=15
    while [ $(server_running) -eq 0 -a ${tries} -lt ${MAX_TRIES} ] ; do
        echo "Wait for stop ($[tries+1])..."
        tries=$[tries+1]
        sleep 1
    done
    if [ $(server_running) -eq 0 ] ; then
        echo "Force to stop..."
        kill -9 ${SERVER_PID}
    fi
    echo "Server stopped"
}

function compile_manager()
{
    echo "Compile manager..."
    cd ${MANAGER_PATH} && mvn -B clean install -DskipTests
    echo "Manager compiled"
}

function compile_api()
{
    echo "Compile API..."
    cd ${API_PATH} && mvn -B clean package -DskipTests
    echo "API compiled"
}

if [ ! -r ${MANAGER_PATH}/target ] ; then
    compile_manager
fi

FLAG=/root/running
if [ ! -f ${FLAG} ] ; then
    # Install manager in container local maven repository
    cd ${MANAGER_PATH} && mvn -B dependency:resolve && mvn -B install -DskipTests
    cd ${API_PATH} && mvn -B dependency:resolve
    touch ${FLAG}
fi

if [ ! -r ${API_PATH}/target ] ; then
    compile_api
fi

if [ -f ${CONFIGS_PATH}/manager/manager.properties ] ; then
    cat ${CONFIGS_PATH}/manager/manager.properties | while read -r line ; do
        export ${line}
    done
fi

SERVER_RUNNING=false
MTIME=${MTIME:-15}
[ "$1" = "--debug" ] && DEBUG=true || DEBUG=false
PARAM2="$2"
DEBUG_PORT=${PARAM2:-7777}

cd ${API_PATH}

JAR_FILE="$(ls target/shaded-*.jar 2>/dev/null)"

${DEBUG} && DEBUG_OPTS="-agentlib:jdwp=transport=dt_socket,server=y,address=${DEBUG_PORT},suspend=n" || DEBUG_OPTS=""

if [ -z "${JAR_FILE}" ] ; then
    echo "ERROR: Cannot find jar file"
    exit 1
else
    start_server
fi

echo "API container is running."
check_changes &

pid="$!"
trap "echo 'Stopping API container'; kill -SIGTERM ${pid} 2>/dev/null; kill -SIGTERM ${SERVER_PID} 2>/dev/null" SIGINT SIGTERM

while kill -0 ${pid} > /dev/null 2>&1; do
    wait
done

stop_server 2>/dev/null

exit 0
