#!/bin/bash

BASE_HREF="${BASE_HREF:-/}"

if [ ! -r ${APP_PATH}/node_modules ] ; then
    npm install --no-audit
fi

FLAG=${APP_PATH}/DEV_RUNNING

touch ${FLAG}

wait_cmd()
{
    while [ -f ${FLAG} ] ; do sleep 1 ; done
    echo "Flag has been removed. Stopping node container..."
    kill -15 ${SERVER_PID} 2>/dev/null
    exit 0
}

ng serve --base-href "${BASE_HREF}" --deploy-url "${BASE_HREF}" \
  --disable-host-check true --ssl true --ssl-key "${CERT_PATH}/server-key.pem" \
  --ssl-cert "${CERT_PATH}/server-cert.pem" &
SERVER_PID="$!"
echo "Node container is running."
wait_cmd &

pid="$!"
trap "echo 'Stopping node container'; kill -SIGTERM ${pid}; kill -SIGTERM ${SERVER_PID}" SIGINT SIGTERM

while kill -0 ${pid} > /dev/null 2>&1; do
    wait
done

rm -f ${FLAG}
kill -15 ${SERVER_PID} 2>/dev/null
exit 0

