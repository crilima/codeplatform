import { BrowserModule }                    from '@angular/platform-browser';
import { RouterModule, Routes }             from '@angular/router';
import { NgModule }                         from '@angular/core';
import { AceEditorModule }                  from 'ng2-ace-editor';
import { AppComponent }                     from './app.component';
import { BrowserAnimationsModule }          from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule, MatButtonModule, MatSelectModule, MatDialogModule,
  MatIconModule, MatSidenavModule, MatListModule, MatMenuModule, MatChipsModule,
  MatTabsModule, MatInputModule, MatProgressSpinnerModule, MatExpansionModule,
  MatGridListModule, MatTableModule, MatSortModule, MatPaginatorModule,
  MatStepperModule}                         from '@angular/material';
import { DialogsService }                   from './dialogs/dialogs.service';
import { ConfirmDialog }                    from './dialogs/confirm-dialog.component';
import { ShowDialog }                       from './dialogs/show-dialog.component';
import { MarkdownModule }                   from 'angular2-markdown';
import { ClipboardModule }                  from 'ngx-clipboard';

import { AccountComponent }                 from './account/account-edit.component';
import { CourseEditorComponent }            from './course-editor/course-editor.component';
import { EditorComponent }                  from './editor/editor.component';
import { HomeComponent }                    from './home/home.component';
import { ChallengeListComponent }           from './challenge-list/challenge-list.component';
import { ExerciseListComponent }            from './exercise-list/exercise-list.component';
import { TeamListComponent }                from './team/team-list.component';
import { TeamEditComponent }                from './team/team-edit.component';
import { ScoreboardComponent }              from './scoreboard/scoreboard.component';
import { TokenService }                     from './token.service';
import { FileUploadModule }                 from 'ng2-file-upload';
import { SplitPaneModule }                  from 'ng2-split-pane/lib/ng2-split-pane';
import { ParticlesModule }                  from 'angular-particle';

import {LoginModal}                         from './login/login-modal.component'
import { BASE_PATH, COLLECTION_FORMATS }    from './swagger-generated-API/variables';
import { APP_BASE_HREF }                    from '@angular/common';

/*swagger stuff*/ 
import { CodeblockRendererService }         from './codeblock/codeblock-renderer.service';
import { ApiModule }                        from './swagger-generated-API/api.module';
import { HttpModule }                       from '@angular/http';
import { HttpClientModule }                 from '@angular/common/http';

const appRoutes: Routes = [
  { path: 'scoreboard', component: ScoreboardComponent },
  { path: 'scoreboard/:challengeId', component: ScoreboardComponent },
  { path: 'challenges', component: ChallengeListComponent },
  { path: 'courses', component: ChallengeListComponent },
  { path: 'courses/:challengeId/editor', component: CourseEditorComponent },
  { path: 'challenges/:id', component: ExerciseListComponent },
  { path: 'challenges/:challengeId/:exerciceId/editor', component: EditorComponent },
  { path: 'teams', component: TeamListComponent },
  { path: 'teams/:challengeId', component: TeamListComponent },
  { path: 'teams/:challengeId/edit/:teamId', component: TeamEditComponent },
  { path: 'account', component: AccountComponent },
  { path: '', component: HomeComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    ConfirmDialog,
    ShowDialog,
    AccountComponent,
    CourseEditorComponent,
    EditorComponent,
    HomeComponent,
    ChallengeListComponent,
    ExerciseListComponent,
    TeamListComponent,
    TeamEditComponent,
    ScoreboardComponent,
    LoginModal
  ],
  exports: [ConfirmDialog, ShowDialog],
  entryComponents: [ConfirmDialog, LoginModal, ShowDialog],
  imports: [
    RouterModule.forRoot(
     appRoutes,
     { enableTracing: false }
     ),
    BrowserModule,
    AceEditorModule,
    ClipboardModule,
    MatCardModule,
    MatButtonModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatIconModule,
    MatSidenavModule,
    MarkdownModule,
    MatListModule,
    MatMenuModule,
    FileUploadModule,
    SplitPaneModule,
    ParticlesModule,
    MatChipsModule,
    MatTabsModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatGridListModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatStepperModule,
    ApiModule,
    HttpModule,
    HttpClientModule
  ],
  providers: [
    DialogsService,
    TokenService,
    CodeblockRendererService,
    AppComponent,
    {
      provide: BASE_PATH,
      useValue: 'http://codeplatform:8081/YellowBuilders/CodePlatform/0.1.1'
    },
    {
      provide: APP_BASE_HREF,
      useValue: '/titan/'
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
