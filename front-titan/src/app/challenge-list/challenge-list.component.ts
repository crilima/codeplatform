import { Component }                from '@angular/core';
import { ChallengeService }         from '../swagger-generated-API/api/challenge.service';
import { ChallengeLightViewModel }  from '../swagger-generated-API/model/challengeLightViewModel';
import { ChallengeScope }           from '../swagger-generated-API/model/challengeScope';
import { Router, NavigationEnd }    from '@angular/router';

@Component({
  templateUrl: './challenge-list.component.html',
  styleUrls: ['./challenge-list.component.css']
})
export class ChallengeListComponent {

  constructor(private challengeService: ChallengeService,
              private router: Router) {
    this.getChallenges();
  }
  private myChallenges: ChallengeLightViewModel[];
  private challenges: ChallengeLightViewModel[];
  private errorMessage: string;
  private kindName: string;
  private scope = ChallengeScope;

  isRegistered(challengeID) {
    return this.myChallenges.some(c => c.id === challengeID);
  }

  getExercicesRoute(challengeID) {
    return this.kindName === '' + ChallengeScope.Challenge 
      ? ['/challenges', challengeID]
      : ['/courses', challengeID, 'editor']
  }

  getChallenges() {
    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        let match = (e.url+'').match(new RegExp('/challenges$'));
        let isChallenge: boolean = match && match.length > 0;
        match = (e.url+'').match(new RegExp('/courses$'));
        let isCourse: boolean = match && match.length > 0;
        let filter = isChallenge ? '' + ChallengeScope.Challenge
            : isCourse ? '' + ChallengeScope.Course
            : null;
        this.kindName = isChallenge ? '' + ChallengeScope.Challenge
            : isCourse ? '' + ChallengeScope.Course
            : 'item';
        this.challengeService.getAllChallenges(filter).subscribe(
          challenges => {
            this.challengeService.getAllChallenges("registered").subscribe(
              myChallenges => {
                this.myChallenges = myChallenges;
                this.challenges = challenges.sort((a,b) =>
                  b.startDate['epochSecond'] - a.startDate['epochSecond']);
              });
          });
      }
    });
  }
}
