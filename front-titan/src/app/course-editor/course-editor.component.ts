import { Component, OnInit ,ViewChild }       from '@angular/core';
import { AbstractControl, FormBuilder,
         FormGroup, Validators }              from '@angular/forms';
import { DialogsService}                      from '../dialogs/dialogs.service';
import { MarkdownService }                    from 'angular2-markdown';
import { Exercice }                           from '../swagger-generated-API/model/exercice';
import { ExerciceViewModel }                  from '../swagger-generated-API/model/exerciceViewModel';
import { ChallengeLightViewModel }            from '../swagger-generated-API/model/challengeLightViewModel';
import { TeamViewModel }                      from '../swagger-generated-API/model/teamViewModel';
import { UserViewModel }                      from '../swagger-generated-API/model/userViewModel';
import { TestCase }                           from '../swagger-generated-API/model/testCase';
import { ResultViewModel }                    from '../swagger-generated-API/model/resultViewModel';
import { SubmissionViewModel }                from '../swagger-generated-API/model/submissionViewModel';
import { ChallengeService }                   from '../swagger-generated-API/api/challenge.service';
import { ExerciceService }                    from '../swagger-generated-API/api/exercice.service';
import { SubmissionRequest }                  from '../swagger-generated-API/model/submissionRequest';
import { SubmissionResponse }                 from '../swagger-generated-API/model/submissionResponse';
import { SubmissionType }                     from '../swagger-generated-API/model/submissionType';
import { SubmissionService }                  from '../swagger-generated-API/api/submission.service';
import { UserAccessActivationService }        from '../swagger-generated-API/api/userAccessActivation.service';
import { TokenService }                       from '../token.service';
import { DefaultStarters }                    from '../editor/default-starters';
import { CodeblockRendererService }           from '../codeblock/codeblock-renderer.service';
import { AppComponent }                       from '../app.component';

import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Location, CommonModule }           from '@angular/common';
import { FileUploader }                     from 'ng2-file-upload';
import 'rxjs/add/operator/switchMap';
import '../editor/brace-imports';

const URL = '/api/';

@Component({
  templateUrl: './course-editor.component.html',
  styleUrls: ['./course-editor.component.scss']
})
export class CourseEditorComponent implements OnInit {

  get formArray(): AbstractControl | null {
    return this.formExercice.get('formArray');
  }

  private currentUser: UserViewModel = null;
  private bestScore: { [id: string]: number } = {};
  private savedCode:  { [id: string]: { [id: string]: string; } } = {};
  private codeStarters: { [id: string]: { [id: string]: string; } };
  private text: string;
  private resizeEvent: number;
  private options: any = {
    // maxLines: 1000,
    printMargin: true,
    enableBasicAutocompletion: true,
    enableSnippets: true,
    enableLiveAutocompletion: true
  };
  private selectedTheme: string;
  private themes;
  private allLangs;
  private langs;
  private selectedLang: string;
  private selectedEditorLang: string;
  private errorMessage: string = null;
  private currentExercice: Exercice;
  private currentIndex: number = 0;
  private exerciceInstructions: { [id: string]: string } = {};
  private exercices: ExerciceViewModel[];
  private testCases: { [id: string]: TestCase } = {};
  private bestCodes: { [id: string]: { [id: string]: string } } = {};
  private team: TeamViewModel;
  private results: { [id: string]: SubmissionResponse[] } = {};
  public uploader: FileUploader = new FileUploader({url: URL});
  public hasBaseDropZoneOver: boolean = false;
  private processing: boolean = true;
  private currentChallenge: ChallengeLightViewModel;
  private currentChallengeId: string;
  private formExercice: FormGroup;
  private exerciceResults: { [id: string]: boolean } = {};
  private exerciceResolved: { [id: string]: string } = {};
  private initIndex: boolean = true;

  constructor (
    private dialogsService: DialogsService,
    private challengeService: ChallengeService,
    private exerciceService: ExerciceService,
    private route: ActivatedRoute,
    private location: Location,
    private submissionService: SubmissionService,
    private userAccessActivationService: UserAccessActivationService,
    private tokenService: TokenService,
    private markdownService: MarkdownService,
    private codeblockRendererService: CodeblockRendererService,
    private appComponent: AppComponent,
    private formBuilder: FormBuilder,
    private router: Router) {
      this.appComponent.retrieveLangs((langs) => {
        this.langs = langs;
        this.themes = this.appComponent.getThemes();
        this.selectedLang = this.appComponent.selectedLang;
        this.selectedTheme = this.appComponent.selectedTheme;
        this.selectedEditorLang = this.appComponent.getSelectedLang(this.selectedLang).realName;
        this.appComponent.onThemeChange((theme) => { 
          this.selectedTheme = theme;
          this.onThemeChange(false);
        });
        this.appComponent.onLanguageChange((language) => {
          this.selectedLang = language;
          this.appComponent.getSelectedLang(this.selectedLang).realName;
          this.clearCode(false);
        });
      });
  }
  @ViewChild('editor') editor;
  @ViewChild('stepper') stepper;
  @ViewChild('markdown') markdown;
  ngOnInit() {
    this.codeStarters = {};
    this.text = "";
    this.route.params.subscribe(params => {
      this.userAccessActivationService.isAuthenticated().subscribe(
        data => {
          this.currentUser = data
          if (!this.currentUser) {
            this.errorMessage = "You must login to access to this page";
            this.processing = false;
          } else {
            this.currentChallengeId = params['challengeId'];
            this.challengeService.getTeamsByChallengeId(this.currentChallengeId, "true")
              .subscribe(teams => {
              if (teams.length < 1) {
                this.errorMessage = "You must regitered to access to this course";
                this.processing = false;
              } else {
                this.team = teams[0];
                this.challengeService.getChallengeByID(this.currentChallengeId)
                  .subscribe(challenge => {
                  if (challenge) {
                    this.initChallenge(challenge);
                  } else {
                    this.errorMessage = 'Cannot find challenge';
                  }
                });
              }
            });
          }
        },
        error => {
          if(error.status === 401) {
            this.errorMessage = "You must login to access to editor";
          }
          this.processing = false;
        });
    });
    this.markdownService.renderer.code = (code: string, language: string) => {
      let lang = this.getSelectedLang(this.selectedLang);
      return this.codeblockRendererService.getConsole([{code:code, className:''}], lang.realName, this.selectedTheme, true);
    }
  }
  ngAfterViewChecked() {
  // ngDoCheck() {
    if (!this.initIndex) {
      this.processing = true;
    }
    if (this.stepper && !this.initIndex) {
      if (this.currentIndex !== this.stepper.selectedIndex) {
        this.updateStepper();
      }
      this.initIndex = true;
      this.processing = false;
    }
    if (this.editor && this.editor._editor) {
      this.editor._editor.$blockScrolling = Infinity;
    }
  }
  initChallenge(challenge: ChallengeLightViewModel) {
    this.currentChallenge = challenge;
    this.challengeService.getAllExercicesByChallengeId(this.currentChallengeId)
      .subscribe(exercices => {
      this.exercices = Object.assign([], exercices.sort((a,b) => a.id.localeCompare(b.id)));
      this.exercices.forEach(exercice => {
        this.codeStarters[exercice.id] = {};
        this.savedCode[exercice.id] = {};
        for (let cs in DefaultStarters) {
          this.codeStarters[exercice.id][cs] = DefaultStarters[cs];
        };
      });
      const reverseExercices: ExerciceViewModel[] = Object.assign([],
          exercices.sort((a,b) => b.id.localeCompare(a.id)));
      const e: ExerciceViewModel = reverseExercices[0];
      const others: ExerciceViewModel[] = reverseExercices.slice(1);
      this.retrieveExercice(e.id, others);
      
      const forms: FormGroup[] = [];
      this.exercices.forEach(exercice => {
        forms.push(this.formBuilder.group({
          ['exerciceCtrl-' + exercice.id]: ['', this.validateExercice],
        }));
      });
      this.formExercice = this.formBuilder.group({
        formArray: this.formBuilder.array(forms),
      });
    });
  }
  fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
  }
  getSelectedLang(key: string) {
    return this.appComponent.getSelectedLang(key);
  }
  onThemeChange(parentChange: boolean = true) {
    this.exerciceInstructions[this.currentExercice.id] = this.getExerciceInstructions(this.currentExercice) + " \n";
    Object.keys(this.exerciceInstructions).forEach(k => {
      this.exerciceInstructions[k] = this.exerciceInstructions[k] + " \n";
    });
    setTimeout(() => {
      this.exerciceInstructions[this.currentExercice.id] = this.getExerciceInstructions(this.currentExercice);
    }, 1000);
    if (parentChange) {
      this.appComponent.changeTheme(this.selectedTheme);
    }
  }
  changeIndex() {
  }
  getAceGutter(code: string): string {
    return this.codeblockRendererService.getAceGutter(code);
  }
  getAceLines(code: string): string[] {
    return this.codeblockRendererService.getAceLines(code);
  }
  retrieveExercice(exerciceID: string, othersExercices: ExerciceViewModel[]) {
    this.challengeService.getExerciceByChallengeId(this.currentChallengeId,
      exerciceID).subscribe(exercice => {
      if (!(exercice.id in this.exerciceInstructions)) {
        this.exerciceInstructions[exercice.id] = this.getExerciceInstructions(exercice);
      }
      this.exercices[othersExercices.length] = exercice;
      let action: (results: ResultViewModel) => void = (results) => {
        let e: ExerciceViewModel = othersExercices[0];
        let others: ExerciceViewModel[] = othersExercices.length > 1
          ? othersExercices.slice(1) : [];
        this.retrieveExercice(e.id, others);
      };
      this.testCases[exercice.id] = exercice.testCases.sort((a, b) => 
        a.order === b.order ? a.name.localeCompare(b.name) : a.order - b.order)[0];
      for (let cs of exercice.codeStarters) {
        this.codeStarters[exercice.id][cs.language] = cs.content;
      }
      if (othersExercices.length == 0) {
        this.currentExercice = exercice;
        action = (results) => {
          this.chooseCode();
          this.goToUnresolved();
          this.processing = false;
        };
      }
      this.getResults(exercice, action);
    });
  }
  goToUnresolved() {
    let tries: number = 0;
    let index: number = 0;
    while (tries < this.exercices.length - 1
           && index < this.exercices.length - 1
           && this.exerciceResults[this.exercices[index].id]) {
      index++;
      tries++;
    }
    this.currentIndex = index;
    this.currentExercice = this.exercices[this.currentIndex];
    this.initIndex = false;
  }
  getIndex(exerciceID: string) {
    return this.exercices.map(e => e.id).indexOf(exerciceID);
  }
  getKeys(array: { [id: string]: any }, sort: boolean = true) {
    let keys: string[] = Object.keys(array);
    if (sort) {
      keys = keys.sort((a, b) => a.localeCompare(b));
    }
    return keys;
  }
  getResults(exercice, callback = null) {
    this.exerciceService.getResultsByExerciceId(this.currentChallengeId,
        exercice.id, this.team.name).subscribe(results => {
      this.exerciceResults[exercice.id] = false;
      this.exerciceResolved[exercice.id] = 'false';
      this.bestScore[exercice.id] = 0;
      this.bestCodes[exercice.id] = {};
      if (results && results.length > 0) {
        let result: ResultViewModel = results[0];
        let submissions: SubmissionViewModel[] = result.submissions;
        if (submissions && submissions.length > 0) {
          this.bestScore[exercice.id] = submissions.sort((a,b) =>
            b.score === a.score
              ? b.date['epochSecond'] - a.date['epochSecond']
              : b.score - a.score)[0].score;
          let resolved: boolean = this.bestScore[exercice.id] === 1;
          this.exerciceResults[exercice.id] = resolved;
          this.exerciceResolved[exercice.id] = resolved ? 'true' : 'false';
          for (let lang of this.langs) {
            let code = submissions.filter(s => "" + s.language == lang.language
              && s.user.login === this.currentUser.login).sort((a,b) =>
              b.score === a.score
                ? b.date['epochSecond'] - a.date['epochSecond']
                : b.score - a.score);
            if (code && code[0] && code[0]!.files[0]!.content) {
              this.bestCodes[exercice.id][lang.language] = atob(code[0].files[0].content);
            }
          }
        }
      }
      if (callback) {
        callback(results);
      }
    });
  }
  register() {
    this.processing = true;
    this.dialogsService
      .confirm('Confirm action', 'Do you want to register to this course?')
      .subscribe(res => {
        if (res) {
          this.challengeService.registerTeam(this.currentChallengeId).subscribe(
            challenge => {
            if (challenge) {
              this.challengeService.getTeamsByChallengeId(this.currentChallengeId, "true")
                  .subscribe(teams => {
                this.team = teams[0];
                this.errorMessage = null;
                this.initChallenge(challenge);
              });
            } else {
              this.errorMessage = 'Cannot find challenge';
            }
          });
        } else {
          this.processing = false;
        }
    });
  }
  setCurrentIndex(index: number) {
    this.currentExercice = this.exercices[this.currentIndex];
    this.loadStarter();
    this.exerciceInstructions[this.currentExercice.id] = this.getExerciceInstructions(this.currentExercice) + " \n";
    Object.keys(this.exerciceInstructions).forEach(k => {
      this.exerciceInstructions[k] = this.exerciceInstructions[k] + " \n";
    });
    setTimeout(() => {
      this.exerciceInstructions[this.currentExercice.id] = this.getExerciceInstructions(this.currentExercice);
    }, 1000);
    this.getResults(this.currentExercice, (results) => {
      this.chooseCode();
      let lang = this.getSelectedLang(this.selectedLang);
      if (this.savedCode[this.currentExercice.id][lang.language]) {
        this.text = this.savedCode[this.currentExercice.id][lang.language];
      }
    });
  }
  selectionChange(event) {
    this.currentIndex = event.selectedIndex;
    this.setCurrentIndex(this.currentIndex);
  }
  goBackStep() {
    this.currentIndex = Math.max(this.currentIndex - 1, 0);
    this.updateStepper();
  }
  goForwardStep() {
    this.currentIndex = Math.min(this.currentIndex + 1, this.exercices.length - 1);
    this.updateStepper();
  }
  updateStepper() {
    this.stepper.selectedIndex = this.currentIndex;
    this.stepper._steps.forEach(s => s.interacted = true);
    // this.stepper._steps.forEach((e, i) => {
      // this.stepper._emitStepperSelectionEvent(i);
    // });
    // this.stepper._emitStepperSelectionEvent(this.currentIndex);
    this.stepper._stateChanged();
  }
  validateExercice(control: AbstractControl) {
    if (control.value !== 'true') {
      return { exerciceResolved: false };
    }
    return null;
  }
  isEditable(exerciceID: string) {
    let keys: string[] = this.getKeys(this.exerciceResults);
    let index: number = keys.indexOf(exerciceID);
    return index < 1 || this.exerciceResults[keys[index-1]];
  }
  courseDone() {
    this.getKeys(this.exerciceResults).every(k => this.exerciceResults[k]);
  }
  getResult(exerciceID: string) {
    return this.results && this.results[exerciceID]
      && this.results[exerciceID].length > 0 
        ? this.results[exerciceID].find(r => r.resultName === this.testCases[exerciceID].name)
        : null;
  }
  onChange(code) {
    this.editor._editor.$blockScrolling = Infinity;
    let lang = this.getSelectedLang(this.selectedLang);
    this.savedCode[this.currentExercice.id][lang.language] = code;
  }
  resizeEditor(n: number = 15) {
    clearTimeout(this.resizeEvent);
    this.resizeEvent = setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
      if (n > 0) {
        this.resizeEditor(n - 1);
      }
    }, 100);
  }
  resetCode() {
    this.dialogsService
     .confirm('Confirm action', 'Do you want to reset your code?', false)
     .subscribe(res => {
      if (res) {
        this.loadStarter();
      }
    });
  }
  loadStarter() {
    let lang = this.getSelectedLang(this.selectedLang);
    if (!(typeof this.codeStarters[this.currentExercice.id][lang.language]==='undefined'
        || this.codeStarters[this.currentExercice.id][lang.language]==="")) {
      this.text = this.codeStarters[this.currentExercice.id][lang.language];
    } else {
      this.text = "";
    }
  }
  clearCode(parentChange: boolean = true) {
    let lang = this.getSelectedLang(this.selectedLang);
    this.exerciceInstructions[this.currentExercice.id] = this.getExerciceInstructions(this.currentExercice);
    this.selectedEditorLang = lang.realName;
    if (parentChange) {
      this.appComponent.changeLanguage(this.selectedLang);
    }
    this.chooseCode();
  }
  chooseCode() {
    let lang = this.getSelectedLang(this.selectedLang);
    if (!(typeof this.bestCodes[this.currentExercice.id][lang.language]==='undefined'
        || this.bestCodes[this.currentExercice.id][lang.language]==="")) {
      this.text = this.bestCodes[this.currentExercice.id][lang.language];
    } else if (!(typeof this.codeStarters[this.currentExercice.id][lang.language]==='undefined'
               || this.codeStarters[this.currentExercice.id][lang.language]==="")) {
      this.text = this.codeStarters[this.currentExercice.id][lang.language];
    } else if (!(typeof this.savedCode[this.currentExercice.id][lang.language]==='undefined'
               || this.savedCode[this.currentExercice.id][lang.language]==="")) {
      this.text = this.savedCode[this.currentExercice.id][lang.language];
    } else {
      this.text = "";
    }
  }
  getBestScore() {
    return this.currentExercice.id in this.getKeys(this.bestScore) != null
      ? (this.bestScore[this.currentExercice.id] * 100).toFixed(2) + '%'
      :'N/A';
  }
  getExerciceInstructions(exercice: Exercice) {
    let inst = "";
    if (/\{\{CODE_HELPER_\d+\}\}/.test(exercice.text)) {
      let lang = this.getSelectedLang(this.selectedLang);
      inst = exercice.text.replace(/(\{\{CODE_HELPER_\d+\}\})/g, lang.language + "\n$1");
      let languageHelpers = exercice.codeHelpers
        .filter(h => h.language + '' === lang.language)
        .sort((a,b) => a.order - b.order);
      for (let i=0; i<languageHelpers.length; i++) {
        inst = inst.replace('{{CODE_HELPER_' + (i+1) + '}}', languageHelpers[i].content);
      }
    } else {
      inst = exercice.text;
    }
    inst = inst.replace(/</g, "&lt;").replace(/>/g, "&gt;") + "\n ";
    inst = inst.replace(/(\{\{CODE_HELPER_\d+\}\})/g, "<<NOT AVAILABLE FOR THIS LANGUAGE>>");
    // console.log(inst);
    return inst;
  }
  submitCode():void{
    this.processing = true;
    let lang = this.getSelectedLang(this.selectedLang);
    let files = new Map<string, Blob>();
    let blob = new Blob([this.text], {type: 'text/plain'});
    files.set(lang.mainScript, blob);
    let request : SubmissionRequest = {
      "challenge": this.currentChallengeId,
      "exercice" : this.currentExercice.id,
      "team"     : this.team.id,
      "type"     : SubmissionType.Files,
      "language" : lang.language
    };
    this.submissionService.submitCode(JSON.stringify(request), files)
      .subscribe(res => {
      if (res === undefined || res === null) {
        this.userAccessActivationService.isAuthenticated().subscribe(
          data => {
            this.currentUser = data;
            if (!this.currentUser) {
              this.errorMessage = "You must login to access to this page";
            }
            this.dialogsService
                 .show('Submission error', 'The submission failed')
                 .subscribe(x => { this.processing = false; });
          },
          error => {
            if(error.status === 401){
              this.errorMessage = "You must login to access to editor";
            }
            this.dialogsService
                .show('Submission error', 'The submission failed')
                .subscribe(x => { this.processing = false; });
          });
      } else {
        let score: number = res.score ? res.score : 0;
        let wasResolved: boolean = this.exerciceResults[this.currentExercice.id];
        let bestScore: number = this.bestScore[this.currentExercice.id];
        bestScore = !bestScore || score > bestScore ? score : bestScore;
        this.bestScore[this.currentExercice.id] = bestScore;
        let resolved: boolean = bestScore === 1;
        this.exerciceResults[this.currentExercice.id] = resolved;
        this.exerciceResolved[this.currentExercice.id] = resolved ? 'true' : 'false';
        let index = this.getIndex(this.currentExercice.id);
        let action: () => void = !wasResolved && resolved && index < this.exercices.length - 1
          ? () => { this.goForwardStep(); }
          : !wasResolved && resolved
          ? () => {
            this.dialogsService
              .confirm('Congratulations!', 'You passed this course! Do you want to browse for another course or stay on it?', false, 'Browse courses', 'Stay here')
              .subscribe(res => {
              if (res) {
                this.router.navigate(['courses']);
              }
            });
          }
          : () => {};
        this.processing = false;
        this.dialogsService
             .show('Submission score', 'Your score is: <b>' + (score * 100).toFixed(2) + ' %</b>')
             .subscribe(x => { action() });
      }
    });
  }
  testCode():void {
    this.processing = true;
    let lang = this.getSelectedLang(this.selectedLang);
    let files = new Map<string, Blob>();
    let blob = new Blob([this.text], {type: 'text/plain'});
    files.set(lang.mainScript, blob);
    let request : SubmissionRequest = {
      "challenge": this.currentChallengeId,
      "exercice" : this.currentExercice.id,
      "team"     : this.team.id,
      "type"     : SubmissionType.Files,
      "language" : lang.language
    };
    this.results[this.currentExercice.id] = [];
    this.submissionService.testCode(JSON.stringify(request), files)
      .subscribe(res => {
      if (!res) {
        this.userAccessActivationService.isAuthenticated().subscribe(
          data => {
            this.currentUser = data;
            if (!this.currentUser) {
              this.errorMessage = "You must login to access to this page";
            }
            this.dialogsService
                .show('Submission error', 'The submission failed')
                .subscribe(x => { this.processing = false; });
          },
          error => {
            if(error.status === 401){
              this.errorMessage = "You must login to access to editor";
            }
            this.dialogsService
                .show('Submission error', 'The submission failed')
                .subscribe(x => { this.processing = false; });
          });
      } else {
        this.results[this.currentExercice.id] = res;
        this.processing = false;
      }
    });
  }
}