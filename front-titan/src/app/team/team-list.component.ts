import { Component }                        from '@angular/core';
import { ChallengeService }                 from '../swagger-generated-API/api/challenge.service';
import { TeamService }                      from '../swagger-generated-API/api/team.service';
import { UserAccessActivationService }      from '../swagger-generated-API/api/userAccessActivation.service';
import { ChallengeLightViewModel }          from '../swagger-generated-API/model/challengeLightViewModel';
import { ChallengeScope }                   from '../swagger-generated-API/model/challengeScope';
import { TeamViewModel }                    from '../swagger-generated-API/model/teamViewModel';
import { UserViewModel }                    from '../swagger-generated-API/model/userViewModel';
import { ActivatedRoute, ParamMap }         from '@angular/router';
import { DialogsService }                   from '../dialogs/dialogs.service';

@Component({
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.scss']
})
export class TeamListComponent {
  private challengeID: string;
  private challengeList: ChallengeLightViewModel[];
  private allChallenges: ChallengeLightViewModel[];
  private teams: TeamViewModel[];
  private errorMessage: string;
  private scopes: string[] = Object.keys(ChallengeScope).filter(a=>a.match(/^[A-Z]/));
  private scope: string = ''+ChallengeScope[ChallengeScope.Challenge];
  private currentUser: UserViewModel;
  private ownedTeams: TeamViewModel[];

  constructor(private challengeService: ChallengeService,
              private teamService: TeamService,
              private userAccessActivationService: UserAccessActivationService,
              private route: ActivatedRoute,
              private dialogsService: DialogsService) {

    this.route.paramMap.switchMap((params: ParamMap) => {
      if (params.get('challengeId')) {
        this.challengeID = params.get('challengeId');
      }
      this.getChallenges();
      return params.get('challengeId') || [];
    }).subscribe(challengeID => {
    });
  }

  isOwner(teamID) {
    return this.currentUser && this.ownedTeams && this.ownedTeams.some(t => t.id === teamID);
  }

  isMember(teamID) {
    return this.currentUser && this.teams && this.teams.some(
        t => t.id === teamID && t.members.some(m => m.login === this.currentUser.login));
  }

  sortedMembers(team) {
    return team.members.sort((a,b) => a.login === team.creator.login ? -1 
            : b.login === team.creator.login ? 1 : a.login.localeCompare(b.login));
  }

  leaveTeam(teamID) {
    this.dialogsService
      .confirm('Confirm action', 'Do you want to leave this team?', false)
      .subscribe(res => {
        if (res) {
          this.challengeService.leaveTeam(this.challengeID, teamID).subscribe(
            challenge => {
              this.getChallenges();
          });
        }
    });
    return false;
  }

  changeScope() {
    this.challengeList = this.allChallenges.filter(c => (''+c.scope).toLowerCase() === this.scope.toLowerCase());
  }

  getChallenges() {
    if (this.challengeID) {
      this.teamService.getTeamsByChallengeId(this.challengeID).subscribe(
        teams => {
          this.teams = teams.filter(t => t.members.length > 0)
            .sort((a,b) => a.name.localeCompare(b.name));
          this.userAccessActivationService.isAuthenticated().subscribe(
            user => {
              this.currentUser = user;
              this.ownedTeams = this.teams.filter(t =>
                t.creator.login === this.currentUser.login);
          });
      });
    } else {
      this.challengeService.getAllChallenges().subscribe(
        challenges => {
          this.allChallenges = challenges;
          this.challengeList = this.allChallenges.filter(c => (''+c.scope).toLowerCase() === this.scope.toLowerCase());
      });
    }
  }
}
