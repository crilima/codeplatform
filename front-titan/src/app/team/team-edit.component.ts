import { Component }                    from '@angular/core';
import { TeamService }                  from '../swagger-generated-API/api/team.service';
import { UserAccessActivationService }  from '../swagger-generated-API/api/userAccessActivation.service';
import { UserService }                  from '../swagger-generated-API/api/user.service';
import { TeamViewModel }                from '../swagger-generated-API/model/teamViewModel';
import { UserViewModel }                from '../swagger-generated-API/model/userViewModel';
import { ActivatedRoute, ParamMap }     from '@angular/router';

@Component({
  templateUrl: './team-edit.component.html',
  styleUrls: ['./team-edit.component.css']
})
export class TeamEditComponent {
  private currentTeam: TeamViewModel;
  private allTeams: TeamViewModel[];
  private currentUser: UserViewModel;
  private users: UserViewModel[];
  private errorMessage: string;
  private infoMessage: string;
  private challengeID: string;
  private teamName: string;
  private user1: UserViewModel;
  private user2: UserViewModel;
  private user3: UserViewModel;
  private members: UserViewModel[];
  private processing: boolean = true;

  constructor(private teamService: TeamService,
              private userService: UserService,
              private userAccessActivationService: UserAccessActivationService,
              private route: ActivatedRoute) {

    this.route.params.subscribe(params => {
      this.userAccessActivationService.isAuthenticated().subscribe(
        user => {
        this.currentUser = user;
        if (this.currentUser) {
          this.challengeID = params['challengeId'];
          let teamID = params['teamId'];
          this.userService.getAllUsers().subscribe(users => {
            let allUsers = users.filter(u => u.login !== this.currentUser.login);
            if (this.challengeID && teamID) {
                this.teamService.getTeamsByChallengeId(this.challengeID)
                  .subscribe(teams => {
                    // Filter users that are not already registered in a team for this challenge
                    this.users = allUsers.filter(u => 
                      !teams.map(t => t.members).reduce((a, o) => a.concat(o))
                        .some(m => m.login === u.login));
                    let myTeam = teams.find(t => t.id === teamID);
                    let members: UserViewModel[] = myTeam ? myTeam.members
                      .filter(u => u.login !== this.currentUser.login) : [];
                    this.user1 = members[0];
                    this.user2 = members[1];
                    this.user3 = members[2];
                    this.members = [];
                    if (this.user1) {
                      this.members.push(this.user1);
                      if (this.user2) {
                        this.members.push(this.user2);
                        if (this.user3) {
                          this.members.push(this.user3);
                        }
                      }
                    }
                    this.users = this.users.concat(this.members);
                    this.allTeams = teams;
                    this.teamName = myTeam.name;
                    this.currentTeam = myTeam;
                    this.processing = false;
                });
            } else {
              this.errorMessage = 'Cannot find team';
              this.processing = false;
            }
          });
        } else {
          this.errorMessage = 'Cannot find user';
          this.processing = false;
        }
      });
    });
  }

  getUsers(user: UserViewModel) {
    return this.users.filter(u => user && user.login === u.login 
      || !this.members.some(m => u.login === m.login));
  }

  saveTeam() {
    this.processing = true;
    this.currentTeam.name = this.teamName;
    this.currentTeam.members = this.members;
    if (!this.members.some(m => m.login === this.currentUser.login)) {
      this.members.push(this.currentUser);
    }
    this.infoMessage = "";
    this.teamService.updateMyTeam(this.challengeID, this.currentTeam)
      .subscribe(team => {
        if (this.currentTeam.name === team.name
          && this.currentTeam.members.length === team.members.length
          && this.currentTeam.members.map(m => m.login)
              .every(l => team.members.map(m => m.login).indexOf(l) !== -1)) {
          this.currentTeam = team;
          this.infoMessage = "Successfully saved";
        } else {
          if (this.allTeams.some(t => t.name === this.currentTeam.name)) {
            this.infoMessage = '<b class="error-msg">Save error: Team name already exists for this challenge</b>';
          } else {
            this.infoMessage = '<b class="error-msg">Save error</b>';
          }
        }
        this.processing = false;
    },
    error => {
      this.infoMessage = '<b class="error-msg">Save error</b>';
      this.processing = false;
    });
  }

  updateUsers() {
    this.members = [];
    if (this.user1) {
      this.members.push(this.user1);
    }
    if (this.user2) {
      this.members.push(this.user2);
    }
    if (this.user3) {
      this.members.push(this.user3);
    }
  }

}
