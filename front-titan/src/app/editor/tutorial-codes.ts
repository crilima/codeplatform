
let tutorialCodes = {
    "code1" : {},
    "code2" : {},
    "code3" : {}
  };

tutorialCodes.code1["bash"] = "read a\nread b\n\nprintf \"%.0f\" $(bc &lt;&lt;&lt; \"scale=2;$a / $b\")\n\n";
tutorialCodes.code2["bash"] = "read a\nread b\n\nprintf \"%d\" $(bc &lt;&lt;&lt; \"scale=2;$a / $b\")\n\n";
tutorialCodes.code3["bash"] = "read a\nread b\n\nif [ $b -eq 0 ] ; then\n\techo ZERODIV\nelse\n\tprintf \"%d\" $(bc &lt;&lt;&lt; \"scale=2;$a / $b\")\nfi\n\n";

tutorialCodes.code1["c"] = "#include <stdlib.h>\n#include <stdio.h>\n#include <string.h>\n\nint main()\n{\n\t\n\tint a, b;\n\tscanf(\"%d\", &a);\n\tscanf(\"%d\", &b);\n\t\n\tprintf(\"%0.f\\n\", a/(float)b);\n\n\treturn 0;\n}\n\n";
tutorialCodes.code2["c"] = "#include <stdlib.h>\n#include <stdio.h>\n#include <string.h>\n\nint main()\n{\n\t\n\tint a, b;\n\tscanf(\"%d\", &a);\n\tscanf(\"%d\", &b);\n\t\n\tprintf(\"%d\\n\", a/b);\n\n\treturn 0;\n}\n\n";
tutorialCodes.code3["c"] = "#include <stdlib.h>\n#include <stdio.h>\n#include <string.h>\n\nint main()\n{\n\t\n\tint a, b;\n\tscanf(\"%d\", &a);\n\tscanf(\"%d\", &b);\n\t\n\tif (b==0)\n\t{\n\t\tprintf(\"ZERODIV\\n\");\n\t}\n\telse\n\t{\n\t\tprintf(\"%d\\n\", a/b);\n\t}\n\n\treturn 0;\n}\n\n";

tutorialCodes.code1["cpp"] = "#include <iostream>\n#include <string>\n#include <vector>\n#include <algorithm>\n\nusing namespace std;\n\nint main()\n{\n\tint a, b;\n\tcin >> a; cin.ignore();\n\tcin >> b; cin.ignore();\n\t\n\tprintf(\"%.0f\\n\", (a/(float)b));\n\t\n}\n\n";
tutorialCodes.code2["cpp"] = "#include <iostream>\n#include <string>\n#include <vector>\n#include <algorithm>\n\nusing namespace std;\n\nint main()\n{\n\tint a, b;\n\tcin >> a; cin.ignore();\n\tcin >> b; cin.ignore();\n\t\n\tprintf(\"%d\\n\", a/b);\n\t\n}\n\n";
tutorialCodes.code3["cpp"] = "#include <iostream>\n#include <string>\n#include <vector>\n#include <algorithm>\n\nusing namespace std;\n\nint main()\n{\n\tint a, b;\n\tcin >> a; cin.ignore();\n\tcin >> b; cin.ignore();\n\t\n\tif (b == 0)\n\t{\n\t\tcout << \"ZERODIV\" << endl;\n\t}\n\telse\n\t{\n\t\tprintf(\"%d\\n\", a/b);\n\t}\n\t\n}\n\n";

tutorialCodes.code1["cs"] = "using System;\n\nclass main\n{\n\tstatic void Main(string[] args)\n\t{\n\t\tint a = int.Parse(Console.ReadLine());\n\t\tint b = int.Parse(Console.ReadLine());\n\t\t\n\t\tConsole.WriteLine(a/(float)b);\n\t}\n}\n\n";
tutorialCodes.code2["cs"] = "using System;\n\nclass main\n{\n\tstatic void Main(string[] args)\n\t{\n\t\tint a = int.Parse(Console.ReadLine());\n\t\tint b = int.Parse(Console.ReadLine());\n\t\t\n\t\tConsole.WriteLine(a/b);\n\t}\n}\n\n";
tutorialCodes.code3["cs"] = "using System;\n\nclass main\n{\n\tstatic void Main(string[] args)\n\t{\n\t\tint a = int.Parse(Console.ReadLine());\n\t\tint b = int.Parse(Console.ReadLine());\n\t\t\n\t\tif (b == 0) {\n\t\t\tConsole.WriteLine(\"ZERODIV\");\n\t\t} else {\n\t\t\tConsole.WriteLine(a/b);\n\t\t}\n\t}\n}\n\n";

tutorialCodes.code1["java"] = "import java.util.*;\nimport java.io.*;\nimport java.math.*;\n\nclass Main {\n\n\tpublic static void main(String args[]) {\n\t\tScanner in = new Scanner(System.in);\n\t\tint a = in.nextInt();\n\t\tint b = in.nextInt();\n\n\t\tSystem.out.printf(\"%.0f\\n\", a/(float)b);\n\t}\n}\n\n";
tutorialCodes.code2["java"] = "import java.util.*;\nimport java.io.*;\nimport java.math.*;\n\nclass Main {\n\n\tpublic static void main(String args[]) {\n\t\tScanner in = new Scanner(System.in);\n\t\tint a = in.nextInt();\n\t\tint b = in.nextInt();\n\n\t\tSystem.out.printf(\"%d\\n\", a/b);\n\t}\n}\n\n";
tutorialCodes.code3["java"] = "import java.util.*;\nimport java.io.*;\nimport java.math.*;\n\nclass Main {\n\n\tpublic static void main(String args[]) {\n\t\tScanner in = new Scanner(System.in);\n\t\tint a = in.nextInt();\n\t\tint b = in.nextInt();\n\t\t\n\t\tif (b == 0) {\n\t\t\tSystem.out.println(\"ZERODIV\");\n\t\t} else {\n\t\t\tSystem.out.printf(\"%d\\n\", a/b);\n\t\t}\n\t}\n}\n\n";

tutorialCodes.code1["javascript"] = "var a = parseInt(readline());\nvar b = parseInt(readline());\n\nprint(a/b);\n\n";
tutorialCodes.code2["javascript"] = "var a = parseInt(readline());\nvar b = parseInt(readline());\n\nprint(parseInt(a/b));\n\n";
tutorialCodes.code3["javascript"] = "var a = parseInt(readline());\nvar b = parseInt(readline());\n\n\nif (b === 0) {\n\tprint('ZERODIV');\n} else {\n\tprint(parseInt(a/b));\n}\n\n";

tutorialCodes.code1["perl"] = "chomp(my $a = &lt;STDIN&gt;);\nchomp(my $b = &lt;STDIN&gt;);\n\nprint $a/$b;\n\n";
tutorialCodes.code2["perl"] = "chomp(my $a = &lt;STDIN&gt;);\nchomp(my $b = &lt;STDIN&gt;);\n\nprint int($a/$b);\n\n";
tutorialCodes.code3["perl"] = "chomp(my $a = &lt;STDIN&gt;);\nchomp(my $b = &lt;STDIN&gt;);\n\nif ($b == 0) {\n\tprint 'ZERODIV';\n} else {\n\tprint int($a/$b);\n}\n\n";

tutorialCodes.code1["php"] = "<?php\n\nfscanf(STDIN, \"%d\", $a);\nfscanf(STDIN, \"%d\", $b);\n\necho($a/$b);\n\n?>\n\n";
tutorialCodes.code2["php"] = "<?php\n\nfscanf(STDIN, \"%d\", $a);\nfscanf(STDIN, \"%d\", $b);\n\necho(floor($a/$b));\n\n?>\n\n";
tutorialCodes.code3["php"] = "<?php\n\nfscanf(STDIN, \"%d\", $a);\nfscanf(STDIN, \"%d\", $b);\n\n\nif ($b == 0) {\n\techo('ZERODIV');\n} else {\n\techo((floor($a/$b)));\n}\n\n?>\n\n";

tutorialCodes.code1["python"] = "import sys\nimport math\n\na = input()\nb = input()\n\nprint int(round(a/float(b)))\n\n";
tutorialCodes.code2["python"] = "import sys\nimport math\n\na = input()\nb = input()\n\nprint int(math.floor(a/float(b)))\n\n";
tutorialCodes.code3["python"] = "import sys\nimport math\n\na = input()\nb = input()\n\nif b == 0:\n\tprint 'ZERODIV'\nelse:\n\tprint int(math.floor(a/float(b)))\n\n";

tutorialCodes.code1["python3"] = "import sys\nimport math\n\na = int(input())\nb = int(input())\n\nprint(round(a/b))\n\n";
tutorialCodes.code2["python3"] = "import sys\nimport math\n\na = int(input())\nb = int(input())\n\nprint(math.floor(a/b))\n\n";
tutorialCodes.code3["python3"] = "import sys\nimport math\n\na = int(input())\nb = int(input())\n\nif b == 0:\n\tprint('ZERODIV')\nelse:\n\tprint(math.floor(a/b))\n\n";

tutorialCodes.code1["ruby"] = "a = gets.to_i\nb = gets.to_i\n\nputs '%.0f' % [a/b.to_f]\n\n";
tutorialCodes.code2["ruby"] = "a = gets.to_i\nb = gets.to_i\n\nputs '%d' % [a/b.to_f]\n\n";
tutorialCodes.code3["ruby"] = "a = gets.to_i\nb = gets.to_i\n\nif b == 0\n\tputs 'ZERODIV'\nelse\n\tputs '%d' % [a/b.to_f]\nend\n\n";

export var TutorialCodes = tutorialCodes;

