/**
 * CodePlatform Swagger
 * CodePlatform REST API
 *
 * OpenAPI spec version: 0.1.1
 * Contact: brian.gohier@hotmail.fr;dan.geffroy@gmail.com;adrien.wattez@gmail.com;ionut.iortoman@gmail.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

/* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams }               from '@angular/common/http';

import { Observable }                                        from 'rxjs/Observable';
import '../rxjs-operators';

import { Challenge }                                         from '../model/challenge';
import { ChallengeLightViewModel }                           from '../model/challengeLightViewModel';
import { Exercice }                                          from '../model/exercice';
import { ResultViewModel }                                   from '../model/resultViewModel';
import { Team }                                              from '../model/team';
import { TeamViewModel }                                     from '../model/teamViewModel';
import { UserViewModel }                                     from '../model/userViewModel';

import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';
import { CustomQueryEncoderHelper }                          from '../encoder';
import {TokenService}                                        from '../../token.service';


@Injectable()
export class ChallengeService {

    protected basePath = 'http://localhost:8081/YellowBuilders/CodePlatform/0.1.1';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient,
                @Optional()@Inject(BASE_PATH) basePath: string,
                @Optional() configuration: Configuration,
                private tokenService : TokenService) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (let consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    public isJsonMime(mime: string): boolean {
        const jsonMime: RegExp = new RegExp('^(application\/json|[^;/ \t]+\/[^;/ \t]+[+]json)[ \t]*(;.*)?$', 'i');
        return mime != null && (jsonMime.test(mime) || mime.toLowerCase() === 'application/json-patch+json');
    }


    /**
     * Create a new challenge
     *
     * @param challenge Challenge to be created
     */
    public addChallenge(challenge: Challenge): Observable<ChallengeLightViewModel> {
        if (challenge === null || challenge === undefined) {
            throw new Error('Required parameter challenge was null or undefined when calling addChallenge.');
        }

        return this.httpClient.post<any>(`${this.basePath}/api/admin/challenge`, challenge, {
            headers: this.tokenService.generateHeaders(),
            withCredentials: this.configuration.withCredentials,
        });
    }

    /**
     * Create a new exercice for the challenge
     *
     * @param challengeId ID of the requested challenge
     * @param exercice The exercice to create
     */
    public addExercice(challengeId: string, exercice: Exercice): Observable<Exercice> {
        if (challengeId === null || challengeId === undefined) {
            throw new Error('Required parameter challengeId was null or undefined when calling addExercice.');
        }
        if (exercice === null || exercice === undefined) {
            throw new Error('Required parameter exercice was null or undefined when calling addExercice.');
        }

        return this.httpClient.post<any>(`${this.basePath}/api/admin/challenge/${encodeURIComponent(String(challengeId))}/exercice`, exercice, {
            headers: this.tokenService.generateHeaders(),
            withCredentials: this.configuration.withCredentials,
        });
    }

    /**
     * Create a new team for the challenge
     *
     * @param challengeId ID of the requested challenge
     * @param team the team to add
     */
    public addTeam(challengeId: string, team: Team): Observable<TeamViewModel> {
        if (challengeId === null || challengeId === undefined) {
            throw new Error('Required parameter challengeId was null or undefined when calling addTeam.');
        }
        if (team === null || team === undefined) {
            throw new Error('Required parameter team was null or undefined when calling addTeam.');
        }

        return this.httpClient.post<any>(`${this.basePath}/api/admin/challenges/${encodeURIComponent(String(challengeId))}/team`, team, {
            headers: this.tokenService.generateHeaders(),
            withCredentials: this.configuration.withCredentials,
        });
    }

    /**
     * Delete a challenge
     *
     * @param challengeId ID of the challenge to delete
     */
    public deleteChallenge(challengeId: string): Observable<ChallengeLightViewModel> {
        if (challengeId === null || challengeId === undefined) {
            throw new Error('Required parameter challengeId was null or undefined when calling deleteChallenge.');
        }

        return this.httpClient.delete<any>(`${this.basePath}/api/admin/challenge/${encodeURIComponent(String(challengeId))}`, {
            headers: this.tokenService.generateHeaders(),
            withCredentials: this.configuration.withCredentials,
        });
    }

    /**
     * delete an exercice for the challenge
     *
     * @param challengeId ID of the requested challenge
     * @param exerciceId ID of the exercice to delete
     */
    public deleteExercice(challengeId: string, exerciceId: string): Observable<Exercice> {
        if (challengeId === null || challengeId === undefined) {
            throw new Error('Required parameter challengeId was null or undefined when calling deleteExercice.');
        }
        if (exerciceId === null || exerciceId === undefined) {
            throw new Error('Required parameter exerciceId was null or undefined when calling deleteExercice.');
        }

        return this.httpClient.delete<any>(`${this.basePath}/api/admin/challenge/${encodeURIComponent(String(challengeId))}/exercice/${encodeURIComponent(String(exerciceId))}`, {
            headers: this.tokenService.generateHeaders(),
            withCredentials: this.configuration.withCredentials,
        });
    }

    /**
     * Delete a team from the challenge
     *
     * @param challengeId ID of the requested challenge
     * @param teamId ID of the team to delete
     */
    public deleteTeam(challengeId: string, teamId: string): Observable<TeamViewModel> {
        if (challengeId === null || challengeId === undefined) {
            throw new Error('Required parameter challengeId was null or undefined when calling deleteTeam.');
        }
        if (teamId === null || teamId === undefined) {
            throw new Error('Required parameter teamId was null or undefined when calling deleteTeam.');
        }

        return this.httpClient.delete<any>(`${this.basePath}/api/admin/challenges/${encodeURIComponent(String(challengeId))}/team/${encodeURIComponent(String(teamId))}`, {
            headers: this.tokenService.generateHeaders(),
            withCredentials: this.configuration.withCredentials,
        });
    }

    /**
     * Get all challenges
     *
     * @param filter Type of challenge to look for
     */
    public getAllChallenges(filter?: string): Observable<Array<ChallengeLightViewModel>> {

        let queryParameters = new HttpParams();
        if (filter !== undefined) {
            queryParameters = queryParameters.set('filter', <any>filter);
        }

        return this.httpClient.get<any>(`${this.basePath}/api/challenge`, {
            params: queryParameters,
            headers: this.tokenService.generateHeaders(),
            withCredentials: this.configuration.withCredentials,
        });
    }

    /**
     * Get all the exercices for the given challenge
     *
     * @param challengeId ID of the requested challenge
     */
    public getAllExercicesByChallengeId(challengeId: string): Observable<Array<Exercice>> {
        if (challengeId === null || challengeId === undefined) {
            throw new Error('Required parameter challengeId was null or undefined when calling getAllExercicesByChallengeId.');
        }

        return this.httpClient.get<any>(`${this.basePath}/api/challenge/${encodeURIComponent(String(challengeId))}/exercice`, {
            headers: this.tokenService.generateHeaders(),
            withCredentials: this.configuration.withCredentials,
        });
    }

    /**
     * Register a team to a challenge
     *
     * @param challengeId ID of the requested challenge
     */
    public registerTeam(challengeId: string): Observable<ChallengeLightViewModel> {
        if (challengeId === null || challengeId === undefined) {
            throw new Error('Required parameter challengeId was null or undefined when calling registerTeam.');
        }

        return this.httpClient.get<any>(`${this.basePath}/api/challenge/${encodeURIComponent(String(challengeId))}/register`, {
            headers: this.tokenService.generateHeaders(),
            withCredentials: this.configuration.withCredentials,
        });
    }

    /**
     * Leave a team from a challenge
     *
     * @param challengeId ID of the requested challenge
     * @param teamId ID of the requested team
     */
    public leaveTeam(challengeId: string, teamId: string): Observable<ChallengeLightViewModel> {
        if (challengeId === null || challengeId === undefined) {
            throw new Error('Required parameter challengeId was null or undefined when calling leaveTeam.');
        }
        if (teamId === null || teamId === undefined) {
            throw new Error('Required parameter teamId was null or undefined when calling leaveTeam.');
        }

        return this.httpClient.delete<any>(`${this.basePath}/api/challenge/${encodeURIComponent(String(challengeId))}/leave/${encodeURIComponent(String(teamId))}`, {
            headers: this.tokenService.generateHeaders(),
            withCredentials: this.configuration.withCredentials,
        });
    }

    /**
     * Get a challenge by ID
     *
     * @param challengeId ID of the requested challenge
     */
    public getChallengeByID(challengeId: string): Observable<ChallengeLightViewModel> {
        if (challengeId === null || challengeId === undefined) {
            throw new Error('Required parameter challengeId was null or undefined when calling getChallengeByID.');
        }

        return this.httpClient.get<any>(`${this.basePath}/api/challenge/${encodeURIComponent(String(challengeId))}`, {
            headers: this.tokenService.generateHeaders(),
            withCredentials: this.configuration.withCredentials,
        });
    }

    /**
     * Get an exercice for a challenge by ID
     *
     * @param challengeId ID of the requested challenge
     * @param exerciceId ID of the requested exercice
     */
    public getExerciceByChallengeId(challengeId: string, exerciceId: string): Observable<Exercice> {
        if (challengeId === null || challengeId === undefined) {
            throw new Error('Required parameter challengeId was null or undefined when calling getExerciceByChallengeId.');
        }
        if (exerciceId === null || exerciceId === undefined) {
            throw new Error('Required parameter exerciceId was null or undefined when calling getExerciceByChallengeId.');
        }

        return this.httpClient.get<any>(`${this.basePath}/api/challenge/${encodeURIComponent(String(challengeId))}/exercice/${encodeURIComponent(String(exerciceId))}`, {
            headers: this.tokenService.generateHeaders(),
            withCredentials: this.configuration.withCredentials,
        });
    }

    /**
     * Get all results for the given challenge
     *
     * @param challengeId ID of the requested challenge
     * @param team ID of the team to get results
     */
    public getResultsByChallengeId(challengeId: string, team?: string): Observable<Array<ResultViewModel>> {
        if (challengeId === null || challengeId === undefined) {
            throw new Error('Required parameter challengeId was null or undefined when calling getResultsByChallengeId.');
        }

        let queryParameters = new HttpParams();
        if (team !== undefined) {
            queryParameters = queryParameters.set('team', <any>team);
        }

        return this.httpClient.get<any>(`${this.basePath}/api/challenge/${encodeURIComponent(String(challengeId))}/result`, {
            params: queryParameters,
            headers: this.tokenService.generateHeaders(),
            withCredentials: this.configuration.withCredentials,
        });
    }

    /**
     * Explore the results for the given exercice
     *
     * @param challengeId ID of the requested challenge
     * @param exerciceId ID of the requested exercice
     * @param team ID of the team to get results
     */
    public getResultsByExerciceId(challengeId: string, exerciceId: string, team?: string): Observable<Array<ResultViewModel>> {
        if (challengeId === null || challengeId === undefined) {
            throw new Error('Required parameter challengeId was null or undefined when calling getResultsByExerciceId.');
        }
        if (exerciceId === null || exerciceId === undefined) {
            throw new Error('Required parameter exerciceId was null or undefined when calling getResultsByExerciceId.');
        }

        let queryParameters = new HttpParams();
        if (team !== undefined) {
            queryParameters = queryParameters.set('team', <any>team);
        }

        return this.httpClient.get<any>(`${this.basePath}/api/challenge/${encodeURIComponent(String(challengeId))}/exercice/${encodeURIComponent(String(exerciceId))}/result`, {
            params: queryParameters,
            headers: this.tokenService.generateHeaders(),
            withCredentials: this.configuration.withCredentials,
        });
    }

    /**
     * Explore the results for the given team
     *
     * @param challengeId ID of the requested challenge
     * @param teamId ID of the requested team
     */
    public getResultsByTeamId(challengeId: string, teamId: string): Observable<Array<ResultViewModel>> {
        if (challengeId === null || challengeId === undefined) {
            throw new Error('Required parameter challengeId was null or undefined when calling getResultsByTeamId.');
        }
        if (teamId === null || teamId === undefined) {
            throw new Error('Required parameter teamId was null or undefined when calling getResultsByTeamId.');
        }

        return this.httpClient.get<any>(`${this.basePath}/api/challenge/${encodeURIComponent(String(challengeId))}/team/${encodeURIComponent(String(teamId))}/result`, {
            headers: this.tokenService.generateHeaders(),
            withCredentials: this.configuration.withCredentials,
        });
    }

    /**
     * A team join the challenge
     * 
     * @param challengeId ID of the requested challenge
     * @param teamId ID of the requested team
     */
    public getTeamByJoiningChallengeId(challengeId: string, teamId: string): Observable<TeamViewModel> {
        if (challengeId === null || challengeId === undefined) {
            throw new Error('Required parameter challengeId was null or undefined when calling getTeamByJoiningChallengeId.');
        }
        if (teamId === null || teamId === undefined) {
            throw new Error('Required parameter teamId was null or undefined when calling getTeamByJoiningChallengeId.');
        }

        return this.httpClient.get<any>(`${this.basePath}/api/challenge/${encodeURIComponent(String(challengeId))}/team/${encodeURIComponent(String(teamId))}/join`, {
            headers: this.tokenService.generateHeaders(),
            withCredentials: this.configuration.withCredentials,
        });
    }

    /**
     * Get teams registred to a challenge
     *
     * @param challengeId ID of the requested challenge
     * @param teamId ID of the requested team
     */
    public getTeamByChallengeId(challengeId: string, teamId: string): Observable<TeamViewModel> {
        if (challengeId === null || challengeId === undefined) {
            throw new Error('Required parameter challengeId was null or undefined when calling getTeamByChallengeId.');
        }
        if (teamId === null || teamId === undefined) {
            throw new Error('Required parameter teamId was null or undefined when calling getTeamByChallengeId.');
        }

        return this.httpClient.get<any>(`${this.basePath}/api/challenge/${encodeURIComponent(String(challengeId))}/team/${encodeURIComponent(String(teamId))}`, {
            headers: this.tokenService.generateHeaders(),
            withCredentials: this.configuration.withCredentials,
        });
    }

    /**
     * Get teams registred to a challenge
     *
     * @param challengeId ID of the requested challenge
     */
    public getTeamsByChallengeId(challengeId: string, filter?: string): Observable<Array<TeamViewModel>> {
        if (challengeId === null || challengeId === undefined) {
            throw new Error('Required parameter challengeId was null or undefined when calling getTeamsByChallengeId.');
        }

         let queryParameters = new HttpParams();
         if (filter !== undefined) {
             queryParameters = queryParameters.set('filter', <any>filter);
         }

        return this.httpClient.get<any>(`${this.basePath}/api/challenge/${encodeURIComponent(String(challengeId))}/team`, {
            params: queryParameters,
            headers: this.tokenService.generateHeaders(),
            withCredentials: this.configuration.withCredentials,
        });
    }

    /**
     * Delete a team from the challenge
     * 
     * @param challengeId ID of the requested challenge
     * @param teamId ID of the team from which delete
     * @param userId ID of the user to remove
     */
    public removeUserFromTeam(challengeId: string, teamId: string, userId: string): Observable<TeamViewModel> {
        if (challengeId === null || challengeId === undefined) {
            throw new Error('Required parameter challengeId was null or undefined when calling removeUserFromTeam.');
        }
        if (teamId === null || teamId === undefined) {
            throw new Error('Required parameter teamId was null or undefined when calling removeUserFromTeam.');
        }
        if (userId === null || userId === undefined) {
            throw new Error('Required parameter userId was null or undefined when calling removeUserFromTeam.');
        }

        return this.httpClient.delete<any>(`${this.basePath}/api/admin/challenges/${encodeURIComponent(String(challengeId))}/team/${encodeURIComponent(String(teamId))}/member/${encodeURIComponent(String(userId))}`, {
            headers: this.tokenService.generateHeaders(),
            withCredentials: this.configuration.withCredentials,
        });
    }

    /**
     * Update a challenge
     *
     * @param challenge Challenge to be updated
     */
    public updateChallenge(challenge: Challenge): Observable<ChallengeLightViewModel> {
        if (challenge === null || challenge === undefined) {
            throw new Error('Required parameter challenge was null or undefined when calling updateChallenge.');
        }

        return this.httpClient.put<any>(`${this.basePath}/api/admin/challenge`, challenge, {
            headers: this.tokenService.generateHeaders(),
            withCredentials: this.configuration.withCredentials,
        });
    }

    /**
     * Update an exercice for the challenge
     *
     * @param challengeId ID of the requested challenge
     * @param exercice the exercice to update
     */
    public updateExercice(challengeId: number, exercice: Exercice): Observable<Exercice> {
        if (challengeId === null || challengeId === undefined) {
            throw new Error('Required parameter challengeId was null or undefined when calling updateExercice.');
        }
        if (exercice === null || exercice === undefined) {
            throw new Error('Required parameter exercice was null or undefined when calling updateExercice.');
        }

        return this.httpClient.put<any>(`${this.basePath}/api/admin/challenge/${encodeURIComponent(String(challengeId))}/exercice`, exercice, {
            headers: this.tokenService.generateHeaders(),
            withCredentials: this.configuration.withCredentials,
        });
    }

    /**
     * Update a team for the challenge
     *
     * @param challengeId ID of the requested challenge
     * @param team the team to add
     */
    public updateTeam(challengeId: string, team: TeamViewModel): Observable<TeamViewModel> {
        if (challengeId === null || challengeId === undefined) {
            throw new Error('Required parameter challengeId was null or undefined when calling updateTeam.');
        }
        if (team === null || team === undefined) {
            throw new Error('Required parameter team was null or undefined when calling updateTeam.');
        }

        return this.httpClient.put<any>(`${this.basePath}/api/admin/challenges/${encodeURIComponent(String(challengeId))}/team`, team, {
            headers: this.tokenService.generateHeaders(),
            withCredentials: this.configuration.withCredentials,
        });
    }

}
