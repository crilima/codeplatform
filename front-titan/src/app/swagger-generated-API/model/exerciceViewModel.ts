/**
 * CodePlatform Swagger
 * CodePlatform REST API 
 *
 * OpenAPI spec version: 0.1.1
 * Contact: brian.gohier@hotmail.fr;dan.geffroy@gmail.com;adrien.wattez@gmail.com;ionut.iortoman@gmail.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import { ExerciceType } from './exerciceType';

export interface ExerciceViewModel {
    id?: string;

    type?: ExerciceType;

    title?: string;

    subtitle?: string;

    text?: string;

    coeff?: number;

}
