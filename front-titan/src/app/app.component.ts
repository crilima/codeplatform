import { Component, ViewChild }                     from '@angular/core';
import { Router, NavigationEnd }                    from '@angular/router';
import { MatDialog, MatCardModule, MatMenuTrigger } from '@angular/material';
import { LoginModal }                               from './login/login-modal.component';
import { UserViewModel }                            from './swagger-generated-API/model/userViewModel'
import { UserAccessActivationService }              from './swagger-generated-API/api/userAccessActivation.service'
import { ExerciceService }                          from './swagger-generated-API/api/exercice.service'
import { TokenService }                             from './token.service'

const MENU_EXPANDED_STORAGE_NAME = 'menu-expanded';
const USER_LANGUAGE_STORAGE_NAME = 'user-language';
const USER_THEME_STORAGE_NAME = 'user-theme';
export const EMOJI_NAMES = [
  'wink',
  'smile',
  'smile-smiling-eyes',
  'grinning-face',
  'smile-open-mouth',
  'smile-smiling-eyes-open-mouth',
  'loudly-crying',
  'tears-of-joy',
  'crying',
  'persevering',
  'disapointed',
  'unamused',
  'sleeping',
  'sunglases',
  'medical-mask',
  'savouring',
  'smile-closed-eyes',
  'confounded',
  'wink-stuck-out-tongue',
  'kiss-smiling-eyes',
  'kiss',
  'kiss-closed-eyes',
  'blowing-kiss',
  'heart-eyes',
];

const DEFAULT_PARTICLES_PARAM: object = {
  "particles": {
    "number": {
      "value": 450,
      "density": {
        "enable": true,
        "value_area": 1200
      }
    },
    "color": {
      "value": "#BBBBFF"
    },
    "shape": {
      "type": ["circle"],
      "stroke": {
        "width": 0.5,
        "color": "#BB772230"
      },
    },
    "opacity": {
      "value": 1,
      "random": true,
      "anim": {
        "enable": true,
        "speed": 0.1,
        "opacity_min": 0.5,
        "sync": false
      }
    },
    "size": {
      "value": 3,
      "random": true,
      "anim": {
        "enable": true,
        "speed": 1,
        "size_min": 0.2,
        "sync": false
      }
    },
    "line_linked": {
      "enable": false,
    },
    "move": {
      "enable": true,
      "speed": 0.25,
      "direction": "none",
      "random": true,
      "straight": false,
      "out_mode": "out",
      "bounce": false,
      "attract": {
        "enable": true,
        "rotateX": 10000,
        "rotateY": 20000
      }
    }
  },
  "interactivity": {
    "detect_on": "canvas",
    "events": {
      "onhover": {
        "enable": false,
      },
      "onclick": {
        "enable": false,
      },
      "resize": true
    },
  },
  "retina_detect": true
};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  private themes = [
    {realName:"twilight", name:"Twilight"},
    {realName:"github", name:"Github"},
    {realName:"eclipse", name:"Eclipse"},
    {realName:"cobalt", name:"Cobalt"},
    {realName:"monokai", name:"Monokai"},
  ];
  private allLangs = [
    {language:"bash", realName:"sh", name:"Bash", mainScript:"main.sh"},
    {language:"c", realName:"c_cpp", name:"C", mainScript:"main.c"},
    {language:"cpp", realName:"c_cpp", name:"C++", mainScript:"main.cpp"},
    {language:"cs", realName:"csharp", name:"C#", mainScript:"Main.cs"},
    {language:"go", realName:"golang", name:"GO", mainScript:"main.go"},
    {language:"java", realName:"java", name:"Java", mainScript:"Main.java"},
    {language:"javascript", realName:"javascript", name:"JavaScript", mainScript:"main.js"},
    {language:"perl", realName:"perl", name:"Perl", mainScript:"main.pl"},
    {language:"php", realName:"php", name:"PHP", mainScript:"main.php"},
    {language:"python", realName:"python", name:"Python 2", mainScript:"main.py"},
    {language:"python3", realName:"python", name:"Python 3", mainScript:"main.py"},
    {language:"ruby", realName:"ruby", name:"Ruby", mainScript:"main.rb"}
  ];
  private langs = [];

  private currentUser: UserViewModel;
  private isEditor: boolean = false;
  private redirectURL: string;
  private themeChangeHandler: ((theme: string) => void) = (theme) => {};
  private languageChangeHandler: ((language: string) => void) = (language) => {};
  private expanded: boolean = !localStorage.getItem(MENU_EXPANDED_STORAGE_NAME) || localStorage.getItem(MENU_EXPANDED_STORAGE_NAME) === 'true';
  public selectedLang: string = localStorage.getItem(USER_LANGUAGE_STORAGE_NAME) ? localStorage.getItem(USER_LANGUAGE_STORAGE_NAME) : "python3";
  public selectedTheme: string = localStorage.getItem(USER_THEME_STORAGE_NAME) ? localStorage.getItem(USER_THEME_STORAGE_NAME) : "cobalt";
  private selectedEditorLang: string;

  private particlesEnabed = false;
  private particlesStyle: object = {
          'position': 'absolute',
          'overflow': 'hidden',
          'width': '100%',
          'height': '100%',
          'display': 'block',
          'top': 0,
          'left': 0,
          'right': 0,
          'bottom': 0,
          'background': 'transparent',
        };
  private particlesParams: object = JSON.parse(JSON.stringify(DEFAULT_PARTICLES_PARAM));
  private particlesParamsBlackout: object = [JSON.parse(JSON.stringify(DEFAULT_PARTICLES_PARAM))].map(param => {
    param['particles']['number']['value'] = 1;
    param['particles']['number']['density']['value_area'] = 2000;
    param['particles']['color']['value'] = "#FFEA00";
    param['particles']['shape'] = {
      "type": "image",
      "image": {
        "src": "assets/images/blackout.png",
        "width": 100,
        "height": 100
      }
    };
    param['particles']['opacity'] = {
      "value": 1,
      "random": false,
      "anim": {
        "enable": false,
      }
    };
    param['particles']['size']['value'] = 100;
    param['particles']['size']['anim']['speed'] = 8;
    param['particles']['size']['anim']['size_min'] = 40;
    param['particles']['move']['speed'] = 1;
    param['particles']['move']['random'] = false;
    param['particles']['move']['attract']['rotateX'] = 100000,
    param['particles']['move']['attract']['rotateY'] = 200000;
    return param;
  })[0];
  private particlesParamsShips: object = [JSON.parse(JSON.stringify(DEFAULT_PARTICLES_PARAM))].map(param => {
    param['particles']['number']['value'] = 5;
    param['particles']['color']['value'] = "#FFEA00";
    param['particles']['shape'] = {
      "type": "image",
      "image": {
        "src": "assets/images/spaceship-cap.png",
        "width": 100,
        "height": 100
      }
    };
    param['particles']['opacity'] = {
      "value": 1,
      "random": false,
      "anim": {
        "enable": false,
      }
    };
    param['particles']['size']['value'] = 60;
    param['particles']['size']['anim']['speed'] = 5;
    param['particles']['size']['anim']['size_min'] = 5;
    param['particles']['move']['speed'] = 20;
    param['particles']['move']['direction'] = "right";
    param['particles']['move']['random'] = false;
    param['particles']['move']['attract']['rotateX'] = 100000,
    param['particles']['move']['attract']['rotateY'] = 200000;
    return param;
  })[0];
  private particlesWidth: number = 100;
  private particlesHeight: number = 100;

  @ViewChild(MatMenuTrigger) menuTrigger: MatMenuTrigger;
  constructor(private dialog: MatDialog,
              private userAccessActivationService: UserAccessActivationService,
              private tokenService : TokenService,
              private exerciceService : ExerciceService,
              private router: Router) {
    this.retrieveLangs(()=>{});
    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        let match = (e.url+'').match(new RegExp('/editor$'));
        this.isEditor = match && match.length > 0;
        this.userAccessActivationService.isAuthenticated()
          .subscribe(data => this.currentUser = data);
        this.redirectURL = localStorage.getItem('redirect-url');
        if (this.redirectURL) {
          localStorage.removeItem('redirect-url');
          this.router.navigate([this.redirectURL]);
        }
      }
    });
  }

  private onMenuItemClick() {
    this.menuTrigger.closeMenu();
  }

  public retrieveLangs(fun: ((langs) => void)) {
    this.exerciceService.listLanguages().subscribe(
      res => {
        this.langs = this.allLangs.filter((l) => res.includes(l.language));
        this.selectedEditorLang = this.getSelectedLang(this.selectedLang).realName;
        fun(this.langs);
      });
  }

  getSelectedLang(key: string) {
      return this.langs.find(e => e.language === key);
  }

  getLanguageName(language: string) {
    return this.langs.find(t => t.language === language).name;
  }

  getLanguageMainScript(language: string) {
    return this.langs.find(t => t.language === language).mainScript;
  }

  getLanguageRealName(language: string) {
    return this.langs.find(t => t.language === language).realName;
  }

  changeLanguage(language: string) {
    if (!this.langs.find(t => t.name === language)) {
      this.selectedLang = language;
      this.storeLanguage();
    }
  }

  private storeLanguage() {
      let lang = this.getSelectedLang(this.selectedLang);
      this.selectedEditorLang = lang.realName;
      localStorage.setItem(USER_LANGUAGE_STORAGE_NAME, lang.language);
  }

  onLanguageChange(fun: (language: string) => void) {
    this.languageChangeHandler = fun;
  }

  private onLanguageChangeEvent() {
    this.storeLanguage();
    this.menuTrigger.closeMenu();
    this.languageChangeHandler(this.selectedLang);
  }

  getThemes() {
    return this.themes;
  }

  getThemeName(theme: string) {
    return this.themes.find(t => t.name === theme).name;
  }

  changeTheme(theme: string) {
    if (!this.themes.find(t => t.name === theme)) {
      this.selectedTheme = theme;
      this.storeTheme();
    }
  }

  onThemeChange(fun: (theme: string) => void) {
    this.themeChangeHandler = fun;
  }

  private onThemeChangeEvent() {
    this.storeTheme();
    this.menuTrigger.closeMenu();
    this.themeChangeHandler(this.selectedTheme);
  }

  private storeTheme() {
    localStorage.setItem(USER_THEME_STORAGE_NAME, this.selectedTheme);
  }

  resizeMenu() {
    this.expanded = !this.expanded;
    localStorage.setItem(MENU_EXPANDED_STORAGE_NAME, '' + this.expanded);
  }

  navigate(paths: string[]): void {
    this.router.navigate(paths);
  }

  openLoginModal(): void {
    let dialogRef = this.dialog.open(LoginModal, {
      data: { currentUser: this.currentUser }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.currentUser = result;
        localStorage.setItem('redirect-url',this.router.url);
        this.router.navigate(['/']);
      }
    });
  }

  setUser(user: UserViewModel) {
    this.currentUser = user;
  }

  logout() : void {
    this.tokenService.removeToken();
    this.currentUser = null;
    this.router.navigate(['/']);
  }

  getCurrentUser() : UserViewModel {
    return this.currentUser;
  }

  onComponentChange(value){
     console.log(value);
   }
}
