import { Observable } from 'rxjs/Rx';
import { ConfirmDialog } from './confirm-dialog.component';
import { ShowDialog } from './show-dialog.component';
import { MatDialogRef, MatDialog, MatDialogConfig } from '@angular/material';
import { Injectable } from '@angular/core';

@Injectable()
export class DialogsService {

    constructor(private dialog: MatDialog) { }

    public confirm(title: string,
                   message: string,
                   selectFirst: boolean = true,
                   firstButton: string = 'YES',
                   secondButton: string = 'NO'): Observable<boolean> {

        let dialogRef: MatDialogRef<ConfirmDialog>;

        dialogRef = this.dialog.open(ConfirmDialog);
        dialogRef.componentInstance.title = title;
        dialogRef.componentInstance.message = message;
        dialogRef.componentInstance.firstButton = firstButton;
        dialogRef.componentInstance.secondButton = secondButton;
        dialogRef.componentInstance.selectFirst = selectFirst;

        return dialogRef.afterClosed();
    }

    public show(title: string,
                message: string): Observable<boolean> {

        let dialogRef: MatDialogRef<ShowDialog>;

        dialogRef = this.dialog.open(ShowDialog);
        dialogRef.componentInstance.title = title;
        dialogRef.componentInstance.message = message;

        return dialogRef.afterClosed();
    }
}
