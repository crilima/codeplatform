import { MatDialogRef } from '@angular/material';
import { Component } from '@angular/core';

@Component({
    selector: 'confirm-dialog',
    template: `
        <h3>{{ title }}</h3>
        <p [innerHTML]="message"></p>
        <button type="button" mat-button class="mat-button {{selectFirst?'mat-raised-button':''}}"
            (click)="dialogRef.close(true)">{{firstButton}}</button>
        <button type="button" mat-button class="mat-button {{selectFirst?'':'mat-raised-button'}}"
            (click)="dialogRef.close(false)">{{secondButton}}</button>
    `,
    styleUrls:["./dialog.css"]
})
export class ConfirmDialog {

    public title: string;
    public message: string;
    public firstButton: string = 'YES';
    public secondButton: string = 'NO';
    public selectFirst: boolean = true;

    constructor(public dialogRef: MatDialogRef<ConfirmDialog>) {

    }
}
