import { Injectable }    from '@angular/core';
import { HttpHeaders }   from '@angular/common/http';
import { UserViewModel } from './swagger-generated-API/model/userViewModel';

@Injectable()
export class TokenService {

  constructor() {
  }

  setToken(login: string, token : string) : void{
      localStorage.setItem('user-token', JSON.stringify({ login: login, token: token }));
  }
  getToken(): string {
    var user = JSON.parse(localStorage.getItem('user-token'));
    var token = user && user.token;
    return token ? token : "";
  }
  removeToken() {
    localStorage.removeItem('user-token');
  }
  generateHeaders(): HttpHeaders {
    return new HttpHeaders({
      'Authorization': this.getToken()
    });
  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

}
