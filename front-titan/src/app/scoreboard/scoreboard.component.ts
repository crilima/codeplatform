import { Component, ViewChild, ElementRef } from '@angular/core';
import { DataSource }                       from '@angular/cdk/collections';
import { MatSort, MatPaginator, PageEvent } from '@angular/material';
import { BehaviorSubject }                  from 'rxjs/BehaviorSubject';
import { Observable }                       from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { ResultService }                    from '../swagger-generated-API/api/result.service';
import { ResultViewModel }                  from '../swagger-generated-API/model/resultViewModel';
import { ChallengeService }                 from '../swagger-generated-API/api/challenge.service';
import { ChallengeLightViewModel }          from '../swagger-generated-API/model/challengeLightViewModel';
import { ChallengeScope }                   from '../swagger-generated-API/model/challengeScope';
import { ExerciceViewModel }                from '../swagger-generated-API/model/exerciceViewModel';
import { TeamViewModel }                    from '../swagger-generated-API/model/teamViewModel';
import { ActivatedRoute, ParamMap }         from '@angular/router';

function getDateFromEpoch(date) {
  return +date['epochSecond'] * 1000 + +date['nano'] / 1000000;
}

@Component({
  templateUrl: './scoreboard.component.html',
  styleUrls: ['./scoreboard.component.css']
})
export class ScoreboardComponent {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('filter') filter: ElementRef;

  private displayedAllColumns = ['challenge', 'exercice', 'team', 'score'];
  private displayedColumnsForChallenge = ['exercice', 'team', 'score'];
  private displayedColumnsForExercice = ['team', 'score'];
  private displayedColumnsForTeam = ['exercice', 'score'];
  private displayedColumns = this.displayedAllColumns;
  private database: ResultDatabase;
  private allData: ResultDatabase;
  private dataByTeam: ResultDatabase;
  private dataSource: ResultDataSource | null;
  private dataSourceAll: ResultDataSource | null;
  private dataSourceByExercice: ResultDataSource | null;
  private dataCache:
    { exercices: DataCacheElement[],
      teams: DataCacheElement[],
      mixed: DataCacheElement[]
    } = {
    'exercices': new Array<DataCacheElement>(),
    'teams': new Array<DataCacheElement>(),
    'mixed': new Array<DataCacheElement>()
  };
  private results: Data[];
  private currentChallenge: ChallengeLightViewModel;
  private challengeID: string;
  private currentExerciceID: string;
  private currentTeamID: string;
  private pageSize = 10;
  private pageSizeOptions = [10, 25, 50, 100];
  private pageEvent: PageEvent;
  // private currentExerciceID: string = 'exercice-1';
  // private currentTeamID: string = 'team-0';
  private exercicesList: ExerciceViewModel[];
  private teamsList: TeamViewModel[];
  private challengeList: ChallengeLightViewModel[];
  private processing: boolean = true;

  constructor(private resultService:ResultService,
              private challengeService: ChallengeService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.paramMap.switchMap((params: ParamMap) => {
      if (params.get('challengeId')) {
        this.challengeID = params.get('challengeId');
      }
      this.initResults();
      return params.get('challengeId') || [];
    }).subscribe(challengeID => {
    });
  }

  initResults() {
    this.challengeService.getAllChallenges('' + ChallengeScope.Challenge).subscribe(challenges => {
      this.challengeList = challenges;
      if (!this.challengeID || this.challengeID.length < 1) {
        this.processing = false;
      }
    });
    if (this.challengeID && this.challengeID.length > 0) {
      this.challengeService.getChallengeByID(this.challengeID).subscribe(challenge => {
        this.currentChallenge = challenge;
        this.resultService.getResultsByChallengeId(this.currentChallenge.id)
          .subscribe((resultViewModels: ResultViewModel[])=> {
            const startDateTS = getDateFromEpoch(this.currentChallenge.startDate);
            const endDateTS = getDateFromEpoch(this.currentChallenge.endDate);
            
            // console.log(startDateTS);
            // console.log(endDateTS);
            // console.log(endDateTS-startDateTS);
            // resultViewModels.forEach(r => {
              // let submissions = r.submissions.sort((a,b) => b.score === a.score
                      // ? a.date['epochSecond'] - b.date['epochSecond']
                    // : b.score - a.score).map(submission => {
                        // const date = getDateFromEpoch(submission.date);
                        // const dtCoeff = (1000 - (date - startDateTS) / 1000);
                        // const dtScore = (submission.score * 100 * dtCoeff);
                        // const value = Math.round(submission.score * 10000 * +r.exercice.coeff + dtScore);
                        // if (submission.score==1) {
                          //// console.log(date);
                          //// console.log(dtCoeff);
                          //// console.log(dtScore);
                          //// console.log(value);
                          //// console.log(dtCoeff);
                        // }
                        // return {
                          // "date": date,
                          // "value": value
                          // };
                      // });
              // console.log(submissions);
              
            // });
            this.results = resultViewModels.map(result => {
              return {
                "challenge": {
                  "id": this.currentChallenge.id,
                  "name": this.currentChallenge.name
                },
                "exercice": {
                  "id": result.exercice.id,
                  "title": result.exercice.title
                },
                "team": {
                  "id": result.team.id,
                  "name": result.team.name,
                  "members": "Members: \n- " + result.team.members.map(m => m.login).join(' \n- ')
                },
                "score": (result.submissions && result.submissions.length > 0
                    ? result.submissions.sort((a,b) => b.score === a.score
                      ? b.date['epochSecond'] - a.date['epochSecond']
                    : b.score - a.score).map(submission => {
                        return {
                          "date": +submission.date['epochSecond'] + +submission.date['nano'] / 1000000,
                          "value": Math.round(submission.score * 10000 * +result.exercice.coeff)
                          };
                      })[0]
                  : { "date": null, "value": 'N/A' } )
              };
            });

            let tmpResult = this.results;

            // Array.prototype.push.apply(this.results, tmpResult);
            // Array.prototype.push.apply(this.results, tmpResult);

            let exercices = this.results.map(result => result.exercice);
            let exercicesSet = groupBy(exercices, 'id');
            this.exercicesList = Object.keys(exercicesSet).map(o => exercicesSet[o][0]);
            let teams = this.results.map(result => result.team);
            let teamsSet = groupBy(teams, 'id');
            this.teamsList = Object.keys(teamsSet).map(o => teamsSet[o][0]);

            let resultByTeam = getGroupByTeam(this.results);
            this.allData = new ResultDatabase(this.results);
            this.dataByTeam = new ResultDatabase(resultByTeam);
            this.database = this.allData;
            this.dataSourceAll = new ResultDataSource(this.allData,
              this.paginator,
              this.sort);
            this.dataSourceByExercice = new ResultDataSource(this.dataByTeam,
              this.paginator,
              this.sort);
            this.dataSource = this.dataSourceAll;
            Observable.fromEvent(this.filter.nativeElement, 'keyup')
              .debounceTime(150)
              .distinctUntilChanged()
              .subscribe(this.updateView);
            this.processing = false;
        });
      });
    }
  }

  paginationChange(e) {
    this.updateView();
  }

  updateView() {
    // console.log(this.dataSource);
    if (this.dataSource) {
      this.dataSource.filter = this.filter.nativeElement.value;
    }
  }

  ngDoCheck() {
    if (this.filter && this.paginator && this.sort) {
      this.dataSourceAll!._paginator = this.paginator;
      this.dataSourceByExercice!._paginator = this.paginator;
      this.dataSourceAll!._sort = this.sort;
      this.dataSourceByExercice!._sort = this.sort;
      this.dataSource!._sort = this.sort;
      this.dataSource!._paginator = this.paginator;
      this.updateView();
    }
  }

  getDataSource(): ResultDataSource {
    // If exercice and team are selected
    if (this.currentExerciceID && this.currentTeamID) {
      let mixedCache: DataCacheElement[] = this.dataCache.mixed;
      let exerciceView: boolean = this.displayedColumns === this.displayedColumnsForExercice;
      let cacheID = exerciceView ? this.currentExerciceID + '-' + this.currentTeamID
        : this.currentTeamID + '-' + this.currentExerciceID;
      let miCache: DataCacheElement = mixedCache.find((e) => e.id === cacheID);
      let cache: DataCacheElement;
      if (!miCache) {
        let miDatabase: ResultDatabase = new ResultDatabase(this.getCurrentMixedData(exerciceView));
        let miDatasource: ResultDataSource = new ResultDataSource(miDatabase,
            this.paginator,
            this.sort);
        cache = this.newDataCache(cacheID, miDatasource, miDatabase);
        mixedCache.push(cache);
      } else {
        cache = miCache;
      }
      this.database = cache.database;
      this.dataSource = cache.datasource;
    // If only exercice is selected
    } else if (this.currentExerciceID) {
      this.displayedColumns = this.displayedColumnsForExercice;
      let exercicesCache: DataCacheElement[] = this.dataCache.exercices;
      let exCache: DataCacheElement = exercicesCache.find((e) => e.id === this.currentExerciceID);
      let cache: DataCacheElement;
      if (!exCache) {
        let exDatabase: ResultDatabase = new ResultDatabase(this.getCurrentExerciceData());
        let exDatasource: ResultDataSource = new ResultDataSource(exDatabase,
            this.paginator,
            this.sort);
        cache = this.newDataCache(this.currentExerciceID, exDatasource, exDatabase);
        exercicesCache.push(cache);
      } else {
        cache = exCache;
      }
      this.database = cache.database;
      this.dataSource = cache.datasource;
    // If only team is selected
    } else if (this.currentTeamID) {
      this.displayedColumns = this.displayedColumnsForTeam;
      let teamsCache: DataCacheElement[] = this.dataCache.teams;
      let teCache: DataCacheElement = teamsCache.find((e) => e.id === this.currentTeamID);
      let cache: DataCacheElement;
      if (!teCache) {
        let teDatabase: ResultDatabase = new ResultDatabase(this.results.filter((r) =>
          r.team.id === this.currentTeamID).map((o) => {
            return {
              'team': {
                id: '',
                name: ''
              },
              'challenge': {
                id: '',
                name: ''
              },
              'exercice': o.exercice,
              'score': o.score
            }
          }));
        let teDatasource: ResultDataSource = new ResultDataSource(teDatabase,
            this.paginator,
            this.sort);
        cache = this.newDataCache(this.currentTeamID, teDatasource, teDatabase);
        teamsCache.push(cache);
      } else {
        cache = teCache;
      }
      this.database = cache.database;
      this.dataSource = cache.datasource;
    // If nothing is selected
    } else {
      this.displayedColumns = this.displayedColumnsForExercice;
      this.database = this.dataByTeam;
      this.dataSource = this.dataSourceByExercice;
      // if (this.dataSource && this.dataSource._sort) {
        // if (!this.dataSource._sort.active && !this.dataSource._sort.direction) {
          // // // console.log(this.dataSource._sort);
          // this.dataSource._sort.active = "score";
          // this.dataSource._sort.direction = "desc";
        // }
      // }
    }
    return this.dataSource;
  }

  newDataCache(id: string, datasource: ResultDataSource, database: ResultDatabase): DataCacheElement {
    return {
      id: id,
      datasource: datasource,
      database: database
    };
  }

  getCurrentExerciceData(): Data[] {
    return this.results.filter((r) =>
      r.exercice.id === this.currentExerciceID).map((o) => {
        return {
          'team': o.team,
          'challenge': {
            id: '',
            name: ''
          },
          'exercice': {
            id: '',
            title: ''
          },
          'score': o.score
        }
      });
  }

  getCurrentMixedData(hideTeam: boolean): Data[] {
    return this.results.filter((r) =>
      r.exercice.id === this.currentExerciceID
        && r.team.id === this.currentTeamID).map((o) => {
        return {
          'team': hideTeam ? o.team : {
            id: '',
            name: ''
          },
          'challenge': {
            id: '',
            name: ''
          },
          'exercice': hideTeam ? {
            id: '',
            title: ''
          } : o.exercice,
          'score': o.score
        }
      });
  }

  clearFilters() {
    this.currentExerciceID = null;
    this.currentTeamID = null;
    this.filter.nativeElement.value = '';
    this.updateView();
  }
}


let groupBy = (allData, dataField) => {
  return allData.reduce((array, object) => {
    (array[object[dataField]] = array[object[dataField]] || []).push(object);
    return array;
  }, {});
};

let groupByOnID = (allData, dataField, dataSubField) => {
  return allData.reduce((array, object) => {
    (array[object[dataField][dataSubField]] =
        array[object[dataField][dataSubField]] || []).push(object);
    return array;
  }, {});
};

let getGroupByTeam = (data) => {
  let dataGroupByTeam = groupByOnID(data, 'team', 'id');
  return Object.keys(dataGroupByTeam).map(o => {
    return {
      'team': dataGroupByTeam[o][0].team,
      'challenge': {
        id: '',
        name: ''
      },
      'exercice': {
        id: '',
        title: ''
      },
      'score': {
        'date': dataGroupByTeam[o].sort((a,b) => b.score.date['epochSecond']
                  - a.score.date['epochSecond'])[0].score.date,
        'value': dataGroupByTeam[o].length < 2
                  ? dataGroupByTeam[o][0].score.value
                  : dataGroupByTeam[o].map(o => +o.score.value).reduce((a,b) => a + b)
      }
    };
  });
}

export interface DataCacheElement {
  id: string,
  datasource: ResultDataSource,
  database: ResultDatabase
}

export interface Data {
  challenge : {
    id: string ,
    name: string
  }
  exercice : {
    id: string ,
    title: string
  }
  team : {
    id: string,
    name: string
  }
  score : {
    date: number,
    value: any
  }
}

export class ResultDatabase {
  dataChange: BehaviorSubject<Data[]> = new BehaviorSubject<Data[]>([]);
  get data(): Data[] {
    return this.dataChange.value;
  }

  constructor(private resultData: Data[]) {
    const copiedData = this.data.slice();
    Array.prototype.push.apply(copiedData, this.resultData);
    this.dataChange.next(copiedData);
  }
}

export class ResultDataSource extends DataSource<any> {
  private _filterChange = new BehaviorSubject('');
  get filter(): string { return this._filterChange.value; }
  set filter(filter: string) { this._filterChange.next(filter); }
  constructor(public _resultDatabase: ResultDatabase,
              public _paginator: MatPaginator,
              public _sort: MatSort) {
    super();
  }

  connect(): Observable<Data[]> {
   let displayDataChanges;
    if (this._paginator) {
      displayDataChanges = [
        this._resultDatabase.dataChange,
        this._sort.sortChange,
        this._filterChange,
        this._paginator.page,
      ];
    } else {
      displayDataChanges = [
        this._resultDatabase.dataChange,
        this._sort.sortChange,
        this._filterChange,
      ];
    }

    return Observable.merge(...displayDataChanges).map(() => {
      let data = this.getSortedData().slice().filter((item: Data) => {
        let searchStr = (item.challenge.name + item.team.name + item.score.value + item.exercice.title).toLowerCase();
        return searchStr.indexOf(this.filter.toLowerCase()) != -1;
      });
      if (this._paginator) {
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
      } else {
        return data;
      }
    });
  }

  disconnect() {}

  getSortedData(): Data[] {
    // console.log('getSortedData');
    const data = this._resultDatabase.data.slice();
    if (!this._sort.active || this._sort.direction == '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number|string = '';
      let propertyB: number|string = '';

      switch (this._sort.active) {
        case 'challenge': [propertyA, propertyB] =
                          [a.challenge.name, b.challenge.name]; break;
        case 'team':      [propertyA, propertyB] =
                          [a.team.name, b.team.name]; break;
        case 'score':     [propertyA, propertyB] =
                          [a.score.value, b.score.value]; break;
        case 'exercice':  [propertyA, propertyB] =
                          [a.exercice.title, b.exercice.title]; break;
      }

      let valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      let valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction == 'asc' ? 1 : -1);
    });
  }
}