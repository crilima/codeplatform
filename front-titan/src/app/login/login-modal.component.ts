import { Component, Inject }                        from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { LoginViewModel }                           from '../swagger-generated-API/model/loginViewModel';
import { UserAccessActivationService }              from '../swagger-generated-API/api/userAccessActivation.service';
import { TokenService }                             from '../token.service';

@Component({
  selector: 'login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.scss']
})
export class LoginModal {
  private login : string = "";
  private password : string = "";
  private msgError : string;

  constructor (
    public dialogRef: MatDialogRef<LoginModal>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private userAccessActivationService: UserAccessActivationService,
    private tokenService : TokenService
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  submitLogin(): void {
    if (this.login==="" || this.password==="") {
      this.msgError = "You must enter your credidentials first";
    } else {
      var user : LoginViewModel = {
        password: this.password,
        username: this.login
      };
      this.userAccessActivationService.authenticate(user).subscribe(
        (response) => {
          this.msgError = null;
          this.tokenService.setToken(this.login, response.headers.get('Authorization'));
          this.dialogRef.close(response.body);
        },
        (err) => {
          this.msgError = "Incorrect credentials"
        }
      );
    }
  }

}
