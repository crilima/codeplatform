#!/bin/bash

EXCLUDES_BASE="target/
bin/
node_modules/"
COMPARATOR="${DST_PATH}/.git"

touch ${COMPARATOR}

GIT_RESET_INTERVAL=20
COUNTER=0

synchronize()
{
    while true ; do
        newer="$(find ${DST_PATH} -cnewer ${COMPARATOR} -type f | sed 's!^'${DST_PATH}'/!!')"
        EXCLUDES_NEW="${EXCLUDES_BASE}
    ${newer}"
        EXCLUDES="--exclude $(echo ${EXCLUDES_NEW} | sed 's/ / --exclude /g')"
        if [ ${COUNTER} -ge ${GIT_RESET_INTERVAL} ] ; then
            rm -rf ${DST_PATH}/.git/*
            echo "Reset destination '.git' directory"
            COUNTER=0
        fi
        rsync -rtu ${EXCLUDES} ${SRC_PATH}/ ${DST_PATH} --delete-delay
        EXCLUDES="--exclude .git/ --exclude $(echo ${EXCLUDES_BASE} | sed 's/ / --exclude /g')"
        [ ! -z "${newer}" ] && touch ${COMPARATOR}
        rsync -rtu ${EXCLUDES} ${DST_PATH}/ ${SRC_PATH}
        COUNTER=$[COUNTER+1]
        sleep 1
    done
}

synchronize &
pid="$!"

echo "RSYNC container is running."

trap "echo 'Stopping RSYNC container'; kill -SIGTERM ${pid} 2>/dev/null" SIGINT SIGTERM

while kill -0 ${pid} > /dev/null 2>&1; do
    wait
done

exit 0
