#!/bin/bash

wait_cmd()
{
	while [ -f /root/running ] ; do sleep 1 ; done
}


touch /root/running
echo -e "
\033[0;32mBuilder is started"
wait_cmd &
pid="$!"
trap "echo -e '
\033[0;31mBuilder is stopped';rm -f /root/running;" SIGINT SIGTERM

while kill -0 ${pid} > /dev/null 2>&1; do
	wait
done

exit 0
