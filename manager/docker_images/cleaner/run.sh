#!/bin/bash

target_dir="$1"

if [ -z "${target_dir}" -o ! -r "${BASE_PATH}/${target_dir}" ] ; then
	echo "ERROR: Cannot find target directory" 1>&2
	exit 1
fi

echo "Removing target path: ${BASE_PATH}/${target_dir}"

rm -rf ${BASE_PATH}/${target_dir}

exit $?
