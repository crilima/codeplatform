#!/bin/bash

TIME_OUT=${EXEC_TIMEOUT:-5}
DEFAULT_REPORTING="0"
ERROR_REPORTING=${ERROR_REPORTING:-${DEFAULT_REPORTING}}

[ ! -z "${OUTPUTS_DIR}" ] && rm -rf ${OUTPUTS_DIR}/*

NO_RUN=false
RUN_ERR_FILE=${OUTPUTS_DIR}/run.err
if [ ! -f ${CODE_PATH}/main.php ] ; then
	echo "ERROR: Cannot find main file" > ${RUN_ERR_FILE}
	NO_RUN=true
fi

PID_DIR=/var/run/codeplatform

mkdir -p ${PID_DIR}

run_test() {
	local filein="$1"
	local fileout="$2"
	local fileerr="$3"
	local str_result=""
	if ${NO_RUN} ; then
		cat ${RUN_ERR_FILE} > "${fileerr}"
		touch "${fileout}"
		ret_code=$?
	else
		str_result="${str_result}
File ${filein} to ${fileout} (err: ${fileerr})"
		cat "${filein}" | timeout ${TIME_OUT} php -d error_reporting="${ERROR_REPORTING}" -f ${CODE_PATH}/main.php 1>"${fileout}" 2>"${fileerr}"
		ret_code=$?
		str_result="${str_result}
Result: $ret_code"
	fi
	if [ ${ret_code} -eq 124 ] ; then
		touch "${fileout}"
		echo "${TIMEOUT_MSG}" > "${fileerr}"
	fi
	if [ -z "$(cat "${fileerr}")" ] ; then
		rm -f "${fileerr}"
	fi
	echo "${str_result}"
	return ${ret_code}
}

find ${INPUTS_DIR} -iname "*.in" | while read -r file_name ; do
	file_in="$(echo "${file_name}" | sed 's!'${INPUTS_DIR}'/\?!!')"
	file_out="$(echo "${file_in}" | sed 's/\.in$/.out/')"
	file_err="$(echo "${file_in}" | sed 's/\.in$/.err/')"
	IN_FILE="${INPUTS_DIR}/${file_in}"
	OUT_FILE="${OUTPUTS_DIR}/${file_out}"
	ERR_FILE="${OUTPUTS_DIR}/${file_err}"
	run_test "${IN_FILE}" "${OUT_FILE}" "${ERR_FILE}" &
	job=$!
	echo ${job} > ${PID_DIR}/${job}.pid
done

pids=`echo $(cd ${PID_DIR} ; ls *.pid | sed 's!.\+/!!g;s/.pid//')`

while kill -0 ${pids} 2>/dev/null ; do
	sleep 0.000001
done

rm -rf ${PID_DIR}/*.pid

${NO_RUN} && exit 2

exit 0

