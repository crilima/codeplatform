#include <iostream>
#include "./sum.cpp"

using namespace std;

int main()
{
        int n;
        cin >> n; cin.ignore();
        int l[n];
        int t;
        for (int i = 0; i < n; i++) {
                cin >> t; cin.ignore();
                l[i] = t;
        }
        cout << sum(l, n) << endl;
}
