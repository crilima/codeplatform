#include <stdio.h>

int main()
{
    int n;
    scanf("%d", &n);
    int s = 0;
    int t;
    for (int i = 0; i < n; i++) {
        scanf("%d", &t);
        s += t;
    }
    printf("%d\n", s);
    return 0;
}
