package fr.nemolovich.apps.codeplatform.manager.env;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;


/**
 *
 * @author bgohier
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public final class EnvHelperTest {

    @Test
    public void testPropFile() {
        Assert.assertTrue(Boolean.valueOf(EnvHelper.getEnv("TEST")));
    }
}
