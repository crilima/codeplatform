package fr.nemolovich.apps.codeplatform.manager.docker;

import com.spotify.docker.client.DockerClient.LogsParam;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.Info;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author bgohier
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DockerClientTest {

    private static String CONTAINER_ID;

    @Test
    public void test01Client() throws DockerException, InterruptedException {
        Info info = DockerHelper.getDockerInfo();
        Assert.assertNotNull(info);
        System.out.println(info.toString());
    }

    @Test
    public void test02Images() throws DockerException, InterruptedException {
        DockerHelper.getImages().stream().forEach(
            image -> {
                String imageName = DockerHelper.getImageName(image);
                Assert.assertNotNull(imageName);
                System.out.println(imageName);
            });
    }

    @Test
    public void test03CreateImage() throws DockerException,
        InterruptedException, IOException {
        Assert.assertNotNull(DockerHelper
            .createImage(new File(DockerHelper.class.getResource("")
                .getPath()).getAbsolutePath(), DockerHelper.getImageName(
                "test")));
    }

    @Test
    public void test04RunContainer() throws DockerException, InterruptedException {
        String name = String.format("%s_%s_test2",
            ContainerTest.TEAM, ContainerTest.EX);
        ExerciceContainer container = ContainerTest.buildContainer(
            ContainerTest.EX, ContainerTest.TEAM, "test", "", new HashMap<>(),
            new HashMap<>(), name, new ConcurrentLinkedQueue<>());

        CONTAINER_ID = DockerHelper.buildContainer(container);
        Assert.assertNotNull(CONTAINER_ID);
        System.out.println(CONTAINER_ID);
        DockerHelper.startContainer(CONTAINER_ID);
        DockerHelper.stopContainer(CONTAINER_ID);
        Assert.assertEquals("Hello World!", DockerHelper.logContainer(
            CONTAINER_ID, LogsParam.stdout()).trim());
        Assert.assertTrue(DockerHelper.getContainers().stream().map(
            c -> c.names().get(0).substring(1))
            .collect(Collectors.toList()).contains(name));
    }

    @Test
    public void test05RemoveContainer() throws DockerException, InterruptedException {
        Assert.assertNotNull(CONTAINER_ID);
        System.out.println(CONTAINER_ID);
        DockerHelper.removeContainer(CONTAINER_ID);
        Assert.assertFalse(DockerHelper.getContainers().stream().map(
            c -> c.names().get(0).substring(1))
            .collect(Collectors.toList()).contains("test"));
    }

    @Test
    public void test06RemoveImage() throws DockerException,
        InterruptedException {
        Assert.assertTrue(DockerHelper.removeImage(DockerHelper.getImageName(
            "test")));
    }
}
