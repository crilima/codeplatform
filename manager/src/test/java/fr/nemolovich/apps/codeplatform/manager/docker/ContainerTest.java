package fr.nemolovich.apps.codeplatform.manager.docker;

import fr.nemolovich.apps.codeplatform.manager.resolver.ExerciceResolver;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.mockito.Mockito;

/**
 *
 * @author bgohier
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ContainerTest {

    static String TEAM = "team1";
    static String EX = "simple-test";
    static String LANGUAGE = "python3";
    static String SUFFIX = "test1";
    static File BASE_DIR;

    static ExerciceContainer buildContainer() throws URISyntaxException {
        BASE_DIR = new File(ContainerTest.class.getResource("/python3")
        .toURI());
        Assert.assertTrue(BASE_DIR.isDirectory());

        return new ExerciceContainer(TEAM, EX, LANGUAGE, BASE_DIR,
                                     p -> p.toString().endsWith(".py"), SUFFIX);
    }

    public static ExerciceContainer buildContainer(String team,
                                                   String exercice,
                                                   String language,
                                                   String baseDirPath,
                                                   Map<String, String> ioPath,
                                                   Map<String, String> volumes,
                                                   String containerName,
                                                   Queue<File> files) {

        final File baseDir = new File(baseDirPath);

        ExerciceContainer container = Mockito.mock(ExerciceContainer.class);

        Mockito.when(container.getIo()).thenReturn(ioPath);
        Mockito.when(container.getVolumes()).thenReturn(volumes);
        Mockito.when(container.getBaseDir()).thenReturn(baseDir);
        Mockito.when(container.getConfig()).thenReturn(DockerHelper
            .getConfig(exercice));
        Mockito.when(container.getExercice()).thenReturn(exercice);
        Mockito.when(container.getTeam()).thenReturn(team);
        Mockito.when(container.getLanguage()).thenReturn(language);
        Mockito.when(container.getFiles()).thenReturn(files);
        Mockito.when(container.getImage()).thenReturn(String.format(
            "%s/%s", DockerHelper.IMAGES_PREFIX, language));
        Mockito.when(container.getName()).thenReturn(containerName);
        String toString = containerToString(container);
        Mockito.when(container.toString()).thenReturn(toString);

        return container;
    }

    public static String containerToString(ExerciceContainer container) {
        return String.format("%s:" + String.join("",
                                                 Collections.nCopies(9,
                                                                     "\n\t%-20s: %s")),
                             ExerciceContainer.class.getSimpleName(),
                             "name", container.getName(),
                             "team", container.getTeam(),
                             "exercice", container.getExercice(),
                             "baseDir", container.getBaseDir(),
                             "language", container.getLanguage(),
                             "image", container.getImage(),
                             "volumes", container.getVolumes(),
                             "ioPath", container.getIo(),
                             "config", container.getConfig()
        );
    }

    @Test
    public void test01Create() throws URISyntaxException {

        ExerciceContainer c = buildContainer();
        Assert.assertNotNull(c);
        System.out.println(c);
        Assert.assertEquals(String.format("%s_%s_%s", TEAM, EX, SUFFIX),
                            c.getName());

    }

    @Test
    public void test02ResolveOK() throws URISyntaxException, IOException {
        ExerciceContainer c = buildContainer();

        Files.createDirectories(new File(
            new File(ContainerTest.class.getResource("/")
                .getPath()).getAbsolutePath().concat(
                c.getVolumes().get(DockerHelper.ENV_LOGS_PATH))).toPath());
        Files.createDirectories(new File(
            new File(ContainerTest.class.getResource("/")
                .getPath()).getAbsolutePath().concat(
                c.getVolumes().get(DockerHelper.ENV_INPUTS_PATH))).toPath());
        Files.createDirectories(new File(
            new File(ContainerTest.class.getResource("/")
                .getPath()).getAbsolutePath().concat(
                c.getVolumes().get(DockerHelper.ENV_OUTPUTS_PATH))).toPath());

        Files.walk(new File(ContainerTest.class.getResource(c.getIo().get(
            DockerHelper.ATTR_OUTPUTS)).getPath()).toPath(), 1)
            .filter(p -> p.toFile().isFile())
            .forEach((p) -> {
                try {
                    Files.copy(p, new File(
                               String.format("%s/%s", ContainerTest.class
                                             .getResource(
                                                 c.getVolumes().get(
                                                     DockerHelper.ENV_OUTPUTS_PATH)).
                                             getPath(),
                                             p.getFileName().toString()
                               )).toPath(), StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException ex) {
                    Logger.getLogger(ContainerTest.class.getName()).log(
                        Level.SEVERE, String.format(
                            "Cannot copy file '%s' to '%s'", p.getFileName(),
                            c.getVolumes().get(DockerHelper.ENV_OUTPUTS_PATH)),
                        ex);
                }
            });

        Map<String, String> inputFiles = ExerciceResolver.getInputsMapping(
                            ContainerTest.class.getResource(c.getIo().get(
                                DockerHelper.ATTR_OUTPUTS)).getPath(),
                            ContainerTest.class.getResource(c.getVolumes().get(
                                DockerHelper.ENV_INPUTS_PATH)).getPath()
                        );
        Map<String, String> outputFiles = ExerciceResolver.getOutputsMapping(
                            ContainerTest.class.getResource(c.getIo().get(
                                DockerHelper.ATTR_OUTPUTS)).getPath(),
                            ContainerTest.class.getResource(c.getVolumes().get(
                                DockerHelper.ENV_OUTPUTS_PATH)).getPath()
                        );
        Map<String, String> logs = ExerciceResolver.getLogsMapping(
                            ContainerTest.class.getResource(c.getIo().get(
                                DockerHelper.ATTR_OUTPUTS)).getPath(),
                            ContainerTest.class.getResource(c.getVolumes().get(
                                DockerHelper.ENV_LOGS_PATH)).getPath()
                        );

        float result = outputFiles.entrySet().stream()
              .map((Entry<String, String> entry) -> ExerciceResolver.resolve(
                  ContainerTest.class.getResource(c.getIo().get(
                      DockerHelper.ATTR_RESOLVER)).getPath(),
                  logs.get(entry.getKey()),
                  inputFiles.get(entry.getKey()),
                  entry.getKey(),
                  entry.getValue()))
              .map(rr -> rr.getResult())
              .reduce((a, b) -> a + b).get() / outputFiles.size();
        Assert.assertNotNull(result);
        System.out.println(result);
        Assert.assertEquals(100f, result * 100, 0);
    }

    @Test
    public void test03ResolveFail() throws URISyntaxException, IOException {
        ExerciceContainer c = buildContainer();

        Files.createDirectories(new File(
            new File(ContainerTest.class.getResource("/")
                .getPath()).getAbsolutePath().concat(
                c.getVolumes().get(DockerHelper.ENV_LOGS_PATH))).toPath());
        Files.createDirectories(new File(
            new File(ContainerTest.class.getResource("/")
                .getPath()).getAbsolutePath().concat(
                c.getVolumes().get(DockerHelper.ENV_INPUTS_PATH))).toPath());
        Files.createDirectories(new File(
            new File(ContainerTest.class.getResource("/")
                .getPath()).getAbsolutePath().concat(
                c.getVolumes().get(DockerHelper.ENV_OUTPUTS_PATH))).toPath());

        Files.walk(new File(ContainerTest.class.getResource(c.getIo().get(
            DockerHelper.ATTR_OUTPUTS).concat("_fail")).getPath()).toPath(), 1)
            .filter(p -> p.toFile().isFile())
            .forEach((p) -> {
                try {
                    Files.copy(p, new File(
                               String.format("%s/%s",
                                             ContainerTest.class.getResource(
                                                 c.getVolumes().get(
                                                     DockerHelper.ENV_OUTPUTS_PATH)).
                                             getPath(),
                                             p.getFileName().toString()
                               )).toPath(), StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException ex) {
                    Logger.getLogger(ContainerTest.class.getName()).log(
                        Level.SEVERE, String.format(
                            "Cannot copy file '%s' to '%s'", p.getFileName(),
                            c.getVolumes().get(DockerHelper.ENV_OUTPUTS_PATH)),
                        ex);
                }
            });

        Map<String, String> inputFiles = ExerciceResolver.getInputsMapping(
                            ContainerTest.class.getResource(c.getIo().get(
                                DockerHelper.ATTR_OUTPUTS)).getPath(),
                            ContainerTest.class.getResource(c.getVolumes().get(
                                DockerHelper.ENV_INPUTS_PATH)).getPath()
                        );
        Map<String, String> outputFiles = ExerciceResolver.getOutputsMapping(
                            ContainerTest.class.getResource(c.getIo().get(
                                DockerHelper.ATTR_OUTPUTS)).getPath(),
                            ContainerTest.class.getResource(c.getVolumes().get(
                                DockerHelper.ENV_OUTPUTS_PATH)).getPath()
                        );
        Map<String, String> logs = ExerciceResolver.getLogsMapping(
                            ContainerTest.class.getResource(c.getIo().get(
                                DockerHelper.ATTR_OUTPUTS)).getPath(),
                            ContainerTest.class.getResource(c.getVolumes().get(
                                DockerHelper.ENV_LOGS_PATH)).getPath()
                        );

        float result = outputFiles.entrySet().stream()
              .map((Entry<String, String> entry) -> ExerciceResolver.resolve(
                  ContainerTest.class.getResource(c.getIo().get(
                      DockerHelper.ATTR_RESOLVER)).getPath(),
                  logs.get(entry.getKey()),
                  inputFiles.get(entry.getKey()),
                  entry.getKey(),
                  entry.getValue()))
              .map(rr -> rr.getResult())
              .reduce((a, b) -> a + b).get() / outputFiles.size();
        Assert.assertNotNull(result);
        System.out.println(result);
        Assert.assertEquals(20f, result * 100, 0.001f);
    }
}
