package fr.nemolovich.apps.codeplatform.manager.docker;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.mockito.Mockito;


/**
 * Test Docker Manager
 *
 * @author bgohier
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ManagerTest {

    private static ExerciceContainer CONTAINER;
    private static final String EXERCICES_DIR;
    private static final String BASE_DIR;

    static {
        String exercicesDir;
        try {
            exercicesDir = new File(ManagerTest.class.getResource("/exercices")
            .toURI()).getAbsolutePath();
        } catch (URISyntaxException ex) {
            Logger.getLogger(ManagerTest.class.getName()).log(Level.SEVERE,
                                                              null, ex);
            exercicesDir = "./exercices";
        }
        EXERCICES_DIR = exercicesDir;
        String baseDir;
        try {
            baseDir = new File(ManagerTest.class.getResource(
            String.format("/%s", ContainerTest.LANGUAGE))
            .toURI()).getAbsolutePath();
        } catch (URISyntaxException ex) {
            Logger.getLogger(ManagerTest.class.getName()).log(Level.SEVERE,
                                                              null, ex);
            baseDir = String.format("./%s", ContainerTest.LANGUAGE);
        }
        BASE_DIR = baseDir;
    }
    private static final Map<String, String> IO_PATH = new HashMap();
    private static final Map<String, String> VOLUMES = new HashMap();

    @BeforeClass
    public static void prepare() throws URISyntaxException {
        final String containerName = String.format("%s_%s_%s",
                                                   ContainerTest.TEAM,
                                                   ContainerTest.EX,
                                                   ContainerTest.SUFFIX);
        final String tmpDir = String.format("%s/tmp/%s/%s/%s",
                                            EXERCICES_DIR, ContainerTest.TEAM,
                                            ContainerTest.EX, containerName);
        final String resolverDir = String.format("%s/resolver/%s",
                                                 EXERCICES_DIR, ContainerTest.EX);
        final String inputsDir = String.format("%s/in/%s", EXERCICES_DIR,
                                               ContainerTest.EX);
        final String outputsDir = String.format("%s/out/%s", EXERCICES_DIR,
                                                ContainerTest.EX);

        IO_PATH.put(DockerHelper.ATTR_RESOLVER, resolverDir);
        IO_PATH.put(DockerHelper.ATTR_INPUTS, inputsDir);
        IO_PATH.put(DockerHelper.ATTR_OUTPUTS, outputsDir);

        VOLUMES.put(DockerHelper.ENV_INPUTS_PATH, String.format("%s/%s", tmpDir,
                                                                DockerHelper.VOLUME_INPUTS));
        VOLUMES.put(DockerHelper.ENV_OUTPUTS_PATH, String.format("%s/%s",
                                                                 tmpDir,
                                                                 DockerHelper.VOLUME_OUTPUTS));
        VOLUMES.put(DockerHelper.ENV_LOGS_PATH, String.format("%s/%s", tmpDir,
                                                              DockerHelper.VOLUME_LOGS));
        final String codeDir = String.format("%s/%s", tmpDir,
                                             DockerHelper.VOLUME_CODE);
        VOLUMES.put(DockerHelper.ENV_CODE_PATH, codeDir);
        VOLUMES.put(DockerHelper.ENV_CONFIG_PATH, String.format("%s/%s", tmpDir,
                                                                DockerHelper.VOLUME_CONFIG));

        Queue<File> files = new ConcurrentLinkedQueue<>(Arrays.asList(
                    new File(String.format("%s/%s", codeDir, "main.py"))));

        CONTAINER = ContainerTest.buildContainer(ContainerTest.TEAM,
                                                 ContainerTest.EX,
                                                 ContainerTest.LANGUAGE,
                                                 BASE_DIR, IO_PATH,
                                                 VOLUMES, containerName, files);
    }

    @Test
    public void testBuildEnv() {
        Assert.assertTrue(Manager.buildLocalEnv(CONTAINER));
        Assert.assertTrue(Manager.cleanLocalEnv(CONTAINER));
    }

    /**
     * <b>WARNING</b>: This require to have existing folders on remote host:
     * <b>/opt/volumes/codeplatform/exercices/tmp</b>
     */
//    @Test
    public void testBuildEnvRemote() {

        final File baseDir = new File(
                   String.format("/opt/volumes/codeplatform_tests/%s",
                                 ContainerTest.LANGUAGE));
        final String containerName = String.format("%s_%s_%s",
                                                   ContainerTest.TEAM,
                                                   ContainerTest.EX,
                                                   ContainerTest.SUFFIX);
        final String tmpDirBase = String.format(
                     "/opt/volumes/codeplatform/exercices/tmp/%s/%s",
                     ContainerTest.TEAM, ContainerTest.EX);
        final String tmpDir = String.format("%s/%s", tmpDirBase, containerName);
        final Map<String, String> volumes = new HashMap();
        volumes.put(DockerHelper.ENV_INPUTS_PATH, String.format("%s/%s", tmpDir,
                                                                DockerHelper.VOLUME_INPUTS));
        volumes.put(DockerHelper.ENV_OUTPUTS_PATH, String.format("%s/%s",
                                                                 tmpDir,
                                                                 DockerHelper.VOLUME_OUTPUTS));
        volumes.put(DockerHelper.ENV_LOGS_PATH, String.format("%s/%s", tmpDir,
                                                              DockerHelper.VOLUME_LOGS));
        volumes.put(DockerHelper.ENV_CODE_PATH, String.format("%s/%s", tmpDir,
                                                              DockerHelper.VOLUME_CODE));
        volumes.put(DockerHelper.ENV_CONFIG_PATH, String.format("%s/%s", tmpDir,
                                                                DockerHelper.VOLUME_CONFIG));
        Mockito.when(CONTAINER.getBaseDir()).thenReturn(baseDir);
        Mockito.when(CONTAINER.getVolumes()).thenReturn(volumes);
        String containerID = Manager.buildContainer(CONTAINER);
        Assert.assertNotNull(containerID);
        Assert.assertTrue(Manager.startContainer(containerID));
        Assert.assertTrue(Manager.stopContainer(containerID));
        Assert.assertTrue(Manager.removeContainer(containerID));

        String rootPath = String.format("%s", tmpDirBase);
        String containerBasePath = String.format("%s/%s",
                                                 DockerHelper.ENV_BASE_PATH,
                                                 "tmp_docker");
        CleanerContainer cleanerContainer = new CleanerContainer(
                         CONTAINER.getName(), containerBasePath, rootPath);
        String cleanerContainerID = Manager.buildCleanerContainer(
               cleanerContainer);
        Assert.assertNotNull(cleanerContainerID);
        Assert.assertTrue(Manager.startContainer(cleanerContainerID));
        Assert.assertTrue(Manager.stopContainer(cleanerContainerID));
        Assert.assertTrue(Manager.removeContainer(cleanerContainerID));

    }

    @Test
    public void testRemoveBuilderImage() {
        Assert.assertTrue(Manager.stopContainer(
            Manager.getContainerBuilderID()));
        Assert.assertTrue(Manager.removeContainer(
            Manager.getContainerBuilderID()));
    }
}
