package fr.nemolovich.apps.codeplatform.manager.docker;

import com.google.common.base.Optional;
import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DefaultDockerClient.Builder;
import com.spotify.docker.client.DockerCertificates;
import com.spotify.docker.client.DockerCertificatesStore;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.DockerClient.ListContainersParam;
import com.spotify.docker.client.DockerClient.ListImagesParam;
import com.spotify.docker.client.DockerClient.LogsParam;
import com.spotify.docker.client.DockerHost;
import com.spotify.docker.client.LogStream;
import com.spotify.docker.client.ProgressHandler;
import com.spotify.docker.client.exceptions.DockerCertificateException;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.Container;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.ContainerCreation;
import com.spotify.docker.client.messages.ExecCreation;
import com.spotify.docker.client.messages.ExecState;
import com.spotify.docker.client.messages.HostConfig;
import com.spotify.docker.client.messages.Image;
import com.spotify.docker.client.messages.Info;

import fr.nemolovich.apps.codeplatform.manager.env.EnvConstants;
import fr.nemolovich.apps.codeplatform.manager.env.EnvHelper;
import fr.nemolovich.apps.codeplatform.manager.props.ExerciceProperty;
import fr.nemolovich.apps.codeplatform.manager.props.PropertyReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is used to submit docker commands.
 *
 * @author bgohier
 */
public final class DockerHelper {

    private static ListImagesParam[] IMAGE_FILTERS = {
        ListImagesParam.allImages(false), // All images
    // ListImagesParam.withLabel("maintainer"),
    // ListImagesParam.withLabel("maintainer", "bgohier")
    };

    private static ListContainersParam[] CONTAINER_FILTERS = {
        ListContainersParam.allContainers(), // All containers
    // ListContainersParam.withStatusRunning()
    };

    private DockerHelper() {
        /*
         * Utility class.
         */
    }

    private static final DockerClient DOCKER_CLIENT;

//    private static final String EXERCICES_PATH_STR = EnvHelper.getEnv(
//        EnvConstants.EXERCICES_PATH,
//        "/opt/volumes/codeplatform/exercices");
    private static final String EXERCICES_PATH = EnvHelper.getEnv(
                                EnvConstants.EXERCICES_PATH,
                                "/opt/volumes/codeplatform/exercices");

    public static final String EXERCICES_OUTER_VOLUME = EnvHelper.getEnv(
                               EnvConstants.EXERCICES_OUTER_VOLUME,
                               EXERCICES_PATH);

    private static final String defaultDockerHost() {
        final String osName = System.getProperty("os.name");
        final String os = osName.toLowerCase(Locale.ENGLISH);
        if (os.equalsIgnoreCase("linux") || os.contains("mac")) {
            return DockerHost.defaultUnixEndpoint();
        } else {
            return String.format("http://%s:%s", DockerHost.defaultAddress(),
                                 DockerHost.defaultPort());
        }
    }

    static {
        DockerClient dockerClient = null;
        try {
            Builder builder = DefaultDockerClient.builder();
            String endpoint = EnvHelper.getEnv(EnvConstants.DOCKER_HOST);
            endpoint = endpoint == null ? defaultDockerHost() : endpoint;
            final String certsPath = EnvHelper.getEnv(
                         EnvConstants.DOCKER_CERT_PATH);

            Optional<DockerCertificatesStore> certs = null;

            if (certsPath != null) {
                final Path dockerCertPath = Paths.get(certsPath);
                certs = DockerCertificates.
                builder().dockerCertPath(
                    dockerCertPath).build();
            }

            endpoint = endpoint.replaceAll("^tcp://", certs != null && certs.
                                                      isPresent() ?
                                                      "https://" :
                                                      "http://");
            URI uri = new URI(endpoint);
            builder.uri(uri.toString());
            if (certs != null && certs.isPresent()) {
                builder.dockerCertificates(certs.get());
            }
            dockerClient = builder.build();
        } catch (DockerCertificateException | URISyntaxException ex) {
            Logger.getLogger(DockerHelper.class.getName()).log(Level.SEVERE,
                                                               "Cannot instantiate docker client",
                                                               ex);
            System.exit(1);
        }
        DOCKER_CLIENT = dockerClient;

//        String exercicesPath = EXERCICES_PATH_STR;
//        try {
//            exercicesPath = Boolean.valueOf(EnvHelper.getEnv("TEST"))
//                ? new File(DockerHelper.class.getResource(EXERCICES_PATH_STR)
//                    .toURI()).getAbsolutePath()
//                : EXERCICES_PATH_STR;
//        } catch (URISyntaxException ex) {
//            Logger.getLogger(DockerHelper.class.getName()).log(Level.SEVERE,
//                null, ex);
//        }
//        EXERCICES_PATH = exercicesPath;
    }

    /**
     * The container base path.
     */
    public static final String ENV_BASE_PATH = EnvHelper.getEnv(
                               EnvConstants.ENV_BASE_PATH, "/var");
    /**
     * The container code folder path.
     */
    public static final String ENV_CODE_PATH = EnvHelper.getEnv(
                               EnvConstants.ENV_CODE_PATH, String.format(
                                   "%s/codeplatform",
                                   ENV_BASE_PATH));
    /**
     * The container logs folder path.
     */
    public static final String ENV_LOGS_PATH = EnvHelper.getEnv(
                               EnvConstants.ENV_LOGS_PATH, String.format(
                                   "%s/codeplatform_logs",
                                   ENV_BASE_PATH));
    /**
     * The container inputs folder path.
     */
    public static final String ENV_INPUTS_PATH = EnvHelper.getEnv(
                               EnvConstants.ENV_INPUTS_PATH, String.format(
                                   "%s/codeplatform_in",
                                   ENV_BASE_PATH));
    /**
     * The container outputs folder path.
     */
    public static final String ENV_OUTPUTS_PATH = EnvHelper.getEnv(
                               EnvConstants.ENV_OUTPUTS_PATH, String.format(
                                   "%s/codeplatform_out",
                                   ENV_BASE_PATH));
    /**
     * The container config folder path.
     */
    public static final String ENV_CONFIG_PATH = EnvHelper.getEnv(
                               EnvConstants.ENV_CONFIG_PATH, String.format(
                                   "%s/codeplatform_cfg",
                                   ENV_BASE_PATH));

    /**
     * The inputs volume folder name.
     */
    public static final String VOLUME_INPUTS = EnvHelper.getEnv(
                               EnvConstants.VOLUME_INPUTS, "in");
    /**
     * The outputs volume folder name.
     */
    public static final String VOLUME_OUTPUTS = EnvHelper.getEnv(
                               EnvConstants.VOLUME_OUTPUTS, "out");
    /**
     * The code volume folder name.
     */
    public static final String VOLUME_CODE = EnvHelper.getEnv(
                               EnvConstants.VOLUME_CODE, "code");
    /**
     * The logs volume folder name.
     */
    public static final String VOLUME_LOGS = EnvHelper.getEnv(
                               EnvConstants.VOLUME_LOGS, "logs");
    /**
     * The config volume folder name.
     */
    public static final String VOLUME_CONFIG = EnvHelper.getEnv(
                               EnvConstants.VOLUME_CONFIG, "config");
    /**
     * The resolver volume folder name.
     */
    public static final String VOLUME_RESOLVER = EnvHelper.getEnv(
                               EnvConstants.VOLUME_RESOLVER, "resolver");

    /**
     * The temporary folder to use to created submitted container.
     */
    public static final String TEMP_DIR = String.format("%s/tmp",
                                                        EXERCICES_OUTER_VOLUME);

    /**
     * The inputs host path.
     */
    public static final String INPUTS_DIR = String.format("%s/%s",
                                                          EXERCICES_OUTER_VOLUME,
                                                          VOLUME_INPUTS);
    /**
     * The outputs host path.
     */
    public static final String OUTPUTS_DIR = String.format("%s/%s",
                                                           EXERCICES_OUTER_VOLUME,
                                                           VOLUME_OUTPUTS);
    /**
     * The resolver host path.
     */
    public static final String RESOLVER_DIR = String.format("%s/%s",
                                                            EXERCICES_PATH,
                                                            VOLUME_RESOLVER);
    /**
     * The config host path.
     */
    public static final String CONFIG_DIR = String.format("%s/%s",
                                                          EXERCICES_PATH,
                                                          VOLUME_CONFIG);

    /**
     * The default resolver path.
     */
    public static final String DEFAULT_RESOLVER = String.format("%s/%s",
                                                                RESOLVER_DIR,
                                                                EnvHelper.
                                                                getEnv(
                                                                    EnvConstants.DEFAULT_RESOLVER_NAME,
                                                                    "default"));

    /**
     * The prefix to use for all generated image.
     */
    public static final String IMAGES_PREFIX = EnvHelper.getEnv(
                               EnvConstants.IMAGES_PREFIX,
                               "capgemininantes/codeplatform");
    /**
     * The prefix to use for all generated image.
     */
    public static final Long MEMORY_LIMIT = EnvHelper.getLongEnv(
                             EnvConstants.MEMORY_LIMIT, 256L);
    /**
     * The image to use as base image for all language images.
     */
    public static final String TEMPLATE_IMAGE = String.format("%s/template",
                                                              IMAGES_PREFIX);
    /**
     * The image to use to clean a container environment.
     */
    public static final String CLEANER_IMAGE = String.format("%s/cleaner",
                                                             IMAGES_PREFIX);
    /**
     * The image to use to build a container environment.
     */
    public static final String BUILDER_IMAGE = String.format("%s/builder",
                                                             IMAGES_PREFIX);

    /**
     * The inputs mapping key.
     */
    public static final String ATTR_INPUTS = "inputs";
    /**
     * The outputs mapping key.
     */
    public static final String ATTR_OUTPUTS = "outputs";
    /**
     * The resolver mapping key.
     */
    public static final String ATTR_RESOLVER = "resolver";
    /**
     * The temporary mapping key.
     */
    public static final String ATTR_TMP = "tmp";

    /**
     * Return a {@link Map} with following content:
     * <ul>
     * <li><b>{@link #VOLUME_INPUTS}</b>: The folders in which to put
     * inputs;</li>
     * <li><b>{@link #VOLUME_OUTPUTS}</b>: The folders in which the outputs will
     * be produced;</li>
     * <li><b>{@link #VOLUME_LOGS}</b>: The folders in which the outputs will be
     * produced;</li>
     * <li><b>{@link #VOLUME_CODE}</b>: The folders in which to put the
     * submitted code;</li>
     * <li><b>{@link #VOLUME_CONFIG}</b>: The folders in which to put
     * config.</li>
     * </ul>
     *
     * @param team {@link String}: The name of the team that submitted.
     * @param exercice {@link String}: The name of the exercice.
     * @param name {@link String}: The name of the container.
     *
     * @return {@link Map}&lt;{@link String},{@link String}&gt; - The volumes
     * map.
     */
    static Map<String, String> getVolumes(String team, String exercice,
                                          String name) {
        Map<String, String> result = new ConcurrentHashMap<>();
        String inputs_volume = String.format("%s/%s", getTemporaryFolder(team,
                                                                         exercice,
                                                                         name),
                                             VOLUME_INPUTS);
        String outputs_volume = String.format("%s/%s", getTemporaryFolder(team,
                                                                          exercice,
                                                                          name),
                                              VOLUME_OUTPUTS);
        String logs_volume = String.format("%s/%s", getTemporaryFolder(team,
                                                                       exercice,
                                                                       name),
                                           VOLUME_LOGS);
        String code_volume = String.format("%s/%s", getTemporaryFolder(team,
                                                                       exercice,
                                                                       name),
                                           VOLUME_CODE);
        String config_volume = String.format("%s/%s", getTemporaryFolder(team,
                                                                         exercice,
                                                                         name),
                                             VOLUME_CONFIG);

        result.put(ENV_INPUTS_PATH, inputs_volume);
        result.put(ENV_OUTPUTS_PATH, outputs_volume);
        result.put(ENV_LOGS_PATH, logs_volume);
        result.put(ENV_CODE_PATH, code_volume);
        result.put(ENV_CONFIG_PATH, config_volume);

        return Collections.unmodifiableMap(result);
    }

    /**
     * Returns the temporary host folder to use for container execution.
     *
     * @param team {@link String}: The name of the team that submitted.
     * @param exercice {@link String}: The name of the exercice.
     * @param name {@link String}: The name of the container.
     *
     * @return {@link String} - The temporary folder.
     */
    static String getTemporaryFolder(String team, String exercice,
                                     String name) {
        return String.format("%s/%s/%s/%s", TEMP_DIR, team, exercice, name);
    }

    /**
     * Return a {@link Map} with following content:
     * <ul>
     * <li><b>{@link #ATTR_INPUTS}</b>: The host folder containing inputs;</li>
     * <li><b>{@link #ATTR_OUTPUTS}</b>: The host folder containing
     * outputs;</li>
     * <li><b>{@link #ATTR_RESOLVER}</b>: The host folder containing
     * resolver.</li>
     * <li><b>{@link #ATTR_TMP}</b>: The host folder containing temporary
     * results.</li>
     * </ul>
     *
     * @param team {@link String}: The name of the team that submitted.
     * @param exercice {@link String}: The name of the exercice.
     * @param name {@link String}: The name of the container.
     *
     * @return {@link Map}&lt;{@link String},{@link String}&gt; - The map of
     * host folders to use for an exercice.
     */
    static Map<String, String> getIOPath(String team, String exercice,
                                         String name) {
        Map<String, String> result = new ConcurrentHashMap<>();
        String inputs = getInputsFolder(exercice);
        String outputs = getOutputsFolder(exercice);
        String resolver = getResolverFolder(exercice);
        String temp = getTempFolder(team, exercice, name);

        result.put(ATTR_INPUTS, inputs);
        result.put(ATTR_OUTPUTS, outputs);
        result.put(ATTR_RESOLVER, resolver);
        result.put(ATTR_TMP, temp);

        return Collections.unmodifiableMap(result);
    }

    /**
     * Return the host inputs folder to use for an exercice.
     *
     * @param exercice {@link String}: The exercice name.
     *
     * @return {@link String} - The host folder.
     */
    private static String getInputsFolder(String exercice) {
        return String.format("%s/%s/%s",
                             EXERCICES_PATH, VOLUME_INPUTS, exercice);
    }

    /**
     * Return the host outputs folder to use for an exercice.
     *
     * @param exercice {@link String}: The exercice name.
     *
     * @return {@link String} - The host folder.
     */
    private static String getOutputsFolder(String exercice) {
        return String.format("%s/%s/%s",
                             EXERCICES_PATH, VOLUME_OUTPUTS, exercice);
    }

    /**
     * Return the host resolver folder to use for an exercice.
     *
     * @param exercice {@link String}: The exercice name.
     *
     * @return {@link String} - The host folder.
     */
    private static String getResolverFolder(String exercice) {
        return String.format("%s/%s/%s",
                             EXERCICES_PATH, VOLUME_RESOLVER, exercice);
    }

    /**
     * Return the host resolver folder to use for an exercice.
     *
     * @param team {@link String}: The name of the team that submitted.
     * @param exercice {@link String}: The exercice name.
     * @param name {@link String}: The name of the container.
     *
     * @return {@link String} - The host folder.
     */
    private static String getTempFolder(String team, String exercice,
                                        String name) {
        return String.format("%s/tmp/%s/%s/%s", EXERCICES_PATH, team, exercice,
                             name);
    }

    /**
     * Load {@link ExerciceProperty exercice property} from YAML file.
     *
     * @param path {@link String}: The path of YAML file to load.
     *
     * @return {@link ExerciceProperty} - The properties loaded from config
     * file.
     */
    private static ExerciceProperty readConfig(String path) {
        ExerciceProperty property = null;
        String filePath = String.format(path);
        try {
            File config = new File(filePath);
            if (!config.exists()) {
                URL resource = DockerHelper.class.getResource(filePath);
                if (resource != null) {
                    config = new File(resource.getPath());
                }
            }
            if (config.exists()) {
                property = PropertyReader.readExerciceFile(
                new FileInputStream(config));
            } else {
                Logger.getLogger(DockerHelper.class.getName()).log(
                    Level.WARNING, String.format("Cannot find config file '%s'",
                                                 filePath));
            }
        } catch (IOException ex) {
            Logger.getLogger(DockerHelper.class.getName()).log(
                Level.SEVERE, String.format("Cannot read config file '%s'",
                                            filePath), ex);
        }
        return property;
    }

    /**
     * Return a specific exercice config path.
     *
     * @param exercice {@link String}: The exercice name.
     *
     * @return {@link ExerciceProperty} - The properties loaded from exercice
     * config file.
     */
    public static ExerciceProperty getConfig(String exercice) {
        return readConfig(String.format("%s/%s/config.yml",
                                        CONFIG_DIR, exercice));
    }

    /**
     * Return the global default config path.
     *
     * @return {@link ExerciceProperty} - The default properties loaded from
     * config file.
     */
    public static ExerciceProperty getGlobalConfig() {
        return readConfig(String.format("%s/config.yml",
                                        CONFIG_DIR));
    }

    /**
     * Return all the docker client images.
     *
     * @return {@link List}&lt;{@link Image}&gt; - The list of docker images.
     *
     * @throws DockerException If the docker client cannot be requeted.
     * @throws InterruptedException If the process has been stopped.
     */
    public static List<Image> getImages() throws DockerException,
                                                 InterruptedException {
        return DOCKER_CLIENT.listImages(IMAGE_FILTERS);
    }

    /**
     * Return the first docker image name found for an image layer.
     *
     * @param image {@link Image}: The image to get name.
     *
     * @return {@link String}: The name of the image.
     */
    public static String getImageName(Image image) {
        return image.repoTags() == null ?
               image.id().split(":")[1].toLowerCase() :
               image.repoTags().get(0).split(":")[0].toLowerCase();
    }

    /**
     * Get the imaage name from a language with image prefix.
     *
     * @param language {@link String}: The language.
     *
     * @return {@link String} - The goal image name.
     */
    public static String getImageName(String language) {
        return String.format("%s/%s", IMAGES_PREFIX, language);
    }

    /**
     * Create a docker image from context path.
     *
     * @param contextPath {@link String}: The context path to use to build
     * image.
     * @param imageName {@link String}: The target image name.
     *
     * @return {@link String}: The ID of the created image layer if it works,
     * <code>null</code> otherwise.
     *
     * @throws DockerException If the docker image creation failed.
     * @throws InterruptedException If the process has been stopped.
     * @throws IOException If the context path access produce some errors.
     */
    static String createImage(String contextPath, String imageName)
        throws DockerException, InterruptedException, IOException {
        ProgressHandler handler = (m) -> {
                        if (Manager.DEBUG) {
                            Logger.getLogger(DockerHelper.class.getName()).log(
                                Level.INFO,
                                m.stream());
                        }
                    };
        return DOCKER_CLIENT.build(new File(contextPath).toPath(), imageName,
                                   "Dockerfile", handler);
    }

    /**
     * Delete a docker image from its name.
     *
     * @param imageName {@link String}: The image name to remove.
     *
     * @return {@link Boolean boolean} - <code>true</code> if the image has been
     * removed successfully, <code>false</code> otherwise.
     *
     * @throws DockerException If the docker image deletion failed.
     * @throws InterruptedException If the process has been stopped.
     */
    static boolean removeImage(String imageName) throws DockerException,
                                                        InterruptedException {
        return removeImage(imageName, null);
    }

    /**
     * Return this list of docker containers.
     *
     * @return {@link List}&lt;{@link Container}&gt; - The list of docker
     * containers.
     *
     * @throws DockerException If the docker image deletion failed.
     * @throws InterruptedException If the process has been stopped.
     */
    static List<Container> getContainers() throws DockerException,
                                                  InterruptedException {
        return DOCKER_CLIENT.listContainers(CONTAINER_FILTERS);
    }

    /**
     * Build a container using {@link ExerciceContainer} properties.
     *
     * @param container {@link ExerciceContainer}: The container object tu use
     * to build docker container.
     *
     * @return {@link String} - The docker container ID.
     *
     * @throws DockerException If the docker container build failed.
     * @throws InterruptedException If the process has been stopped.
     */
    static String buildContainer(ExerciceContainer container)
        throws DockerException, InterruptedException {
        String id = null;
        String imageName = container.getImage();
        String containerName = container.getName();

        /*
         * Create docker host config for the container.
         */
        HostConfig.Builder hostBuilder = HostConfig.builder();

        /*
         * Adding all volumes bindings.
         */
        container.getVolumes().entrySet().stream()
            .forEach((e) -> {
                hostBuilder.appendBinds(HostConfig.Bind.from(e.getValue())
                    .to(e.getKey()).build());
            });

        /*
         * Set the maximum memory limit.
         */
        hostBuilder.memory(MEMORY_LIMIT * 1000000L);
        /*
         * Build the host config.
         */
        HostConfig hostConfig = hostBuilder.build();

        /*
         * Get environment variables.
         */
        String[] envs;
        if (container.getConfig() != null &&
            container.getConfig().getContainer() != null) {
            envs = container.getConfig().getContainer().getEnv();
        } else {
            envs = new String[0];
        }

        /*
         * Create the docker container builder with standard output and error 
         * handler and defined host config.
         */
        ContainerConfig.Builder builder = ContainerConfig.builder()
                                .image(imageName)
                                .attachStdout(true)
                                .attachStderr(true)
                                .env(envs)
                                .hostConfig(hostConfig);

        /*
         * Submit container creation to docker client.
         */
        ContainerCreation containerCreation = DOCKER_CLIENT.createContainer(
                          builder.build(), containerName);
        if (containerCreation != null) {
            /*
             * If the container has been created then check warning and display 
             * it.
             */
            if (containerCreation.warnings() != null) {
                containerCreation.warnings().stream().forEach((warn) -> {
                    Logger.getLogger(DockerHelper.class.getName()).log(
                        Level.WARNING, warn);
                });
            }
            id = containerCreation.id();
            /*
             * set the container ID with docker container ID.
             */
            container.setContainerID(id);
        }
        return id;
    }

    /**
     * Build a cleaner container using {@link CleanerContainer} properties.
     *
     * @param container {@link CleanerContainer}: The container object tu use to
     * build docker container.
     *
     * @return {@link String} - The docker container ID.
     *
     * @throws DockerException If the docker container build failed.
     * @throws InterruptedException If the process has been stopped.
     */
    static String buildCleanerContainer(CleanerContainer container)
        throws DockerException, InterruptedException {
        String id = null;
        String imageName = DockerHelper.CLEANER_IMAGE;
        String containerName = container.getCleanerCotnainerName();

        /*
         * Create docker host config for the container.
         */
        HostConfig.Builder hostBuilder = HostConfig.builder();

        /*
         * Adding volume binding.
         */
        hostBuilder.appendBinds(HostConfig.Bind.from(
            container.getHostVolume()).to(container.getContainerVolume())
            .build());

        /*
         * Set the maximum memory limit.
         */
        hostBuilder.memory(MEMORY_LIMIT * 1000000L);
        /*
         * Build the host config.
         */
        HostConfig hostConfig = hostBuilder.build();

        /*
         * Create the docker container builder with standard output and error 
         * handler and defined host config.
         */
        ContainerConfig.Builder builder = ContainerConfig.builder()
                                .image(imageName)
                                .attachStdout(true)
                                .attachStderr(true)
                                .cmd(container.getCotnainerName())
                                .hostConfig(hostConfig);

        /*
         * Submit container creation to docker client.
         */
        ContainerCreation containerCreation = DOCKER_CLIENT.createContainer(
                          builder.build(), containerName);
        if (containerCreation != null) {
            /*
             * If the container has been created then check warning and display 
             * it.
             */
            if (containerCreation.warnings() != null) {
                containerCreation.warnings().stream().forEach((warn) -> {
                    Logger.getLogger(DockerHelper.class.getName()).log(
                        Level.WARNING, warn);
                });
            }
            id = containerCreation.id();
            /*
             * set the container ID with docker container ID.
             */
            container.setContainerID(id);
        }
        return id;
    }

    /**
     * Build the builder container using {@link BuilderContainer} properties.
     *
     * @param container {@link BuilderContainer}: The container object tu use to
     * build docker container.
     *
     * @return {@link String} - The docker container ID.
     *
     * @throws DockerException If the docker container build failed.
     * @throws InterruptedException If the process has been stopped.
     */
    static String buildBuilderContainer(BuilderContainer container)
        throws DockerException, InterruptedException {
        String id = null;
        String imageName = DockerHelper.BUILDER_IMAGE;
        String containerName = container.getCotnainerName();

        /*
         * Create docker host config for the container.
         */
        HostConfig.Builder hostBuilder = HostConfig.builder();

        /*
         * Adding all volumes bindings.
         */
        container.getVolumes().entrySet().stream()
            .forEach((e) -> {
                hostBuilder.appendBinds(HostConfig.Bind.from(e.getValue())
                    .to(e.getKey()).build());
            });

        /*
         * Set the maximum memory limit.
         */
        hostBuilder.memory(MEMORY_LIMIT * 1000000L);
        /*
         * Build the host config.
         */
        HostConfig hostConfig = hostBuilder.build();

        /*
         * Create the docker container builder with standard output and error 
         * handler and defined host config.
         */
        ContainerConfig.Builder builder = ContainerConfig.builder()
                                .image(imageName)
                                .attachStdout(true)
                                .attachStderr(true)
                                .hostConfig(hostConfig);

        /*
         * Submit container creation to docker client.
         */
        ContainerCreation containerCreation = DOCKER_CLIENT.createContainer(
                          builder.build(), containerName);
        if (containerCreation != null) {
            /*
             * If the container has been created then check warning and display 
             * it.
             */
            if (containerCreation.warnings() != null) {
                containerCreation.warnings().stream().forEach((warn) -> {
                    Logger.getLogger(DockerHelper.class.getName()).log(
                        Level.WARNING, warn);
                });
            }
            id = containerCreation.id();
            /*
             * set the container ID with docker container ID.
             */
            container.setContainerID(id);
        }
        return id;
    }

    /**
     * Start the docker container.
     *
     * @param containerID {@link String}: The ID of the container to start.
     *
     * @throws DockerException If the docker container start failed.
     * @throws InterruptedException If the process has been stopped.
     *
     * @see #startContainer(java.lang.String, boolean)
     */
    static void startContainer(String containerID)
        throws DockerException, InterruptedException {
        startContainer(containerID, false);
    }

    /**
     * Start the docker container.
     *
     * @param containerID {@link String}: The ID of the container to start.
     * @param detachedMode {@link Boolean boolean}: Set if the container must be
     * started in detached mode.
     *
     * @throws DockerException If the docker container start failed.
     * @throws InterruptedException If the process has been stopped.
     */
    static void startContainer(String containerID, boolean detachedMode)
        throws DockerException, InterruptedException {
        if (!DOCKER_CLIENT.inspectContainer(containerID).state().running()) {
            DOCKER_CLIENT.startContainer(containerID);
        }
        if (!detachedMode) {
            DOCKER_CLIENT.waitContainer(containerID);
        }
    }

    /**
     * Stop the docker container.
     *
     * @param containerID {@link String}: The ID of the container to stop.
     *
     * @throws DockerException If the docker container stop failed.
     * @throws InterruptedException If the process has been stopped.
     */
    static void stopContainer(String containerID)
        throws DockerException, InterruptedException {
        DOCKER_CLIENT.stopContainer(containerID, 0);
    }

    static ExecResult execContainer(String containerID, List<String> cmds)
        throws DockerException, InterruptedException {
        ExecCreation exec = DOCKER_CLIENT.execCreate(containerID,
                                                     cmds.toArray(new String[0]),
                                                     DockerClient.ExecCreateParam.
                                                     attachStderr(),
                                                     DockerClient.ExecCreateParam.
                                                     attachStdout());
        LogStream logs = DOCKER_CLIENT.execStart(exec.id());
        String logContent;

        ExecState state = DOCKER_CLIENT.execInspect(exec.id());
        int tries = 0;
        while (tries < 20 && state.running()) {
            state = DOCKER_CLIENT.execInspect(exec.id());
            Thread.sleep(250);
            tries++;
        }
        logContent = logs.readFully();
        return new ExecResult(state.exitCode() != null ? state.exitCode() : 1,
                              logContent);
    }

    static void copyContainer(String containerID, String hostPath,
                              String containerPath)
        throws DockerException, InterruptedException, IOException {
        Path hostFilePath = new File(hostPath).toPath();
        DOCKER_CLIENT.copyToContainer(hostFilePath, containerID, containerPath);
    }

    /**
     * Get logs from the docker container.
     *
     * @param containerID {@link String}: The ID of the container to retrieve
     * logs.
     * @param params {@link LogsParam}: The logs types to collect.
     *
     * @return {@link String}: The logs of the container.
     *
     * @throws DockerException If the docker container logs failed.
     * @throws InterruptedException If the process has been stopped.
     */
    static String logContainer(String containerID, LogsParam... params)
        throws InterruptedException, DockerException {
        LogStream logs = DOCKER_CLIENT.logs(containerID, params);
        return logs.readFully();
    }

    /**
     * Remove the docker container.
     *
     * @param containerID {@link String}: The ID of the container to remove.
     *
     * @throws DockerException If the docker container deletion failed.
     * @throws InterruptedException If the process has been stopped.
     */
    static void removeContainer(String containerID)
        throws InterruptedException, DockerException {
        DOCKER_CLIENT.removeContainer(containerID);
    }

    /**
     * Remove a docker image.
     *
     * @param imageName {@link String}: The name of the image to remove.
     * @param version {@link String}: The version of the image to remove. If the
     * version is <code>null</code> then using <code>latest</code> version.
     *
     * @return {@link Boolean boolean} - <code>true</code> if the image has been
     * deleted successfully, <code>false</code> otherwise.
     *
     * @throws DockerException If the docker image deletion failed.
     * @throws InterruptedException If the process has been stopped.
     */
    static boolean removeImage(String imageName, String version)
        throws DockerException, InterruptedException {
        String image = version == null ? imageName :
                       String.format("%s:%s", imageName, version);
        return !DOCKER_CLIENT.removeImage(image).isEmpty();
    }

    /**
     * Return the docker client information.
     *
     * @return {@link Info} - The docker client information.
     *
     * @throws DockerException If the docker info retrieving failed.
     * @throws InterruptedException If the process has been stopped.
     */
    static Info getDockerInfo() throws DockerException,
                                       InterruptedException {
        return DOCKER_CLIENT.info();
    }

}
