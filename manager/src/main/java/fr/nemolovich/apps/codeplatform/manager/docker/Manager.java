package fr.nemolovich.apps.codeplatform.manager.docker;

import com.spotify.docker.client.exceptions.DockerException;

import fr.nemolovich.apps.codeplatform.manager.env.EnvConstants;
import fr.nemolovich.apps.codeplatform.manager.env.EnvHelper;
import fr.nemolovich.apps.codeplatform.manager.props.LanguagesProperty;
import fr.nemolovich.apps.codeplatform.manager.props.PropertyReader;
import fr.nemolovich.apps.codeplatform.manager.resolver.ResolverResult;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * This manager is used to control all docker actions.
 *
 * @author bgohier
 */
public final class Manager {

    private Manager() {
        /*
         * Utility class.
         */
    }

    private static final int MAX_THREADS = EnvHelper.getIntegerEnv(
                             EnvConstants.THREAD_MAX_SIZE, 50);
    private static final int MAX_TASKS = EnvHelper.getIntegerEnv(
                             EnvConstants.MAX_QUEUE_SIZE, 500);
    private static final int CORE_THREADS = EnvHelper.getIntegerEnv(
                             EnvConstants.THREAD_CORE_SIZE, MAX_THREADS);
    private static final long TIME_THREAD = EnvHelper.getLongEnv(
                              EnvConstants.THREAD_TIMEOUT, 60000L);

    private static final ThreadPoolExecutor RUNNER =
                                            new ThreadPoolExecutor(CORE_THREADS,
                                                                   MAX_THREADS,
                                                                   TIME_THREAD,
                                                                   TimeUnit.MILLISECONDS,
                                                                   new ArrayBlockingQueue<>(
                                                                       MAX_TASKS,
                                                                       true),
                                                                   new ManagerThreadFactory(
                                                                       "mgr-runner-%03d",
                                                                       () ->
                                                                       ContainerRunnerTask.
                                                                       getNextThreadNumber()));
    private static final ThreadPoolExecutor CLEANER =
                                            new ThreadPoolExecutor(CORE_THREADS,
                                                                   MAX_THREADS,
                                                                   TIME_THREAD,
                                                                   TimeUnit.MILLISECONDS,
                                                                   new ArrayBlockingQueue<>(
                                                                       MAX_TASKS,
                                                                       true),
                                                                   new ManagerThreadFactory(
                                                                       "mgr-cleaner-%03d",
                                                                       () ->
                                                                       ContainerCleanerTask.
                                                                       getNextThreadNumber()));

    private static final String MANAGER_PATH = EnvHelper.getEnv(
                                EnvConstants.MANAGER_PATH,
                                "/src/codeplatform");
    private static final String IMAGES_PATH = EnvHelper.getEnv(
                                EnvConstants.IMAGES_PATH, String.format(
                                    "%s/docker_images",
                                    new File(MANAGER_PATH)));
    private static final List<String> LANGUAGES;

    /**
     * The display name of the manager.
     */
    public static final String MANAGER_NAME = EnvHelper.getEnv(
                               EnvConstants.MANAGER_NAME, "CodeplatformManager");

    /**
     * The manager version.
     */
    public static final String MANAGER_VERSION = EnvHelper.getEnv(
                               EnvConstants.MANAGER_VERSION, Manager.class.
                               getPackage()
                               .getImplementationVersion());
    private static final Logger LOGGER = Logger.getLogger(
                                Manager.class.getName());

    private static final Thread MONITOR = new Thread(
                                new ManagerMonitor("Runner", RUNNER).add(
                                    "Cleaner", CLEANER));

    static final BuilderContainer BUILDER = BuilderContainer.getInstance();
    private static final Function<ExerciceContainer, Boolean> BUILDER_TASK;

    static {
        List<String> languages;
        try {
            final LanguagesProperty prop = PropertyReader.readLanguagesFile(
                                    EnvHelper.getLanguagesConfig());
            languages = prop.getOrderedLanguages();
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE,
                       "Cannot read language config file", ex);
            languages = Arrays.asList("bash");
        }
        LANGUAGES = languages;

        try {
            if (!DockerHelper.getImages().stream()
                .map((i) -> DockerHelper.getImageName(i))
                .collect(Collectors.toList())
                .contains(DockerHelper.BUILDER_IMAGE)) {
                LOGGER.log(Level.INFO, "Create builder image");
                if (!Manager.dockerBuildImage(DockerHelper.BUILDER_IMAGE)) {
                    LOGGER.log(Level.SEVERE, "Builder image creation failed");
                }
            }
        } catch (DockerException | InterruptedException ex) {
            LOGGER.log(Level.SEVERE, "Cannot create builder image", ex);
        }

        try {
            if (!DockerHelper.getImages().stream()
                .map((i) -> DockerHelper.getImageName(i))
                .collect(Collectors.toList())
                .contains(DockerHelper.CLEANER_IMAGE)) {
                LOGGER.log(Level.INFO, "Create cleaner image");
                if (!Manager.dockerBuildImage(DockerHelper.CLEANER_IMAGE)) {
                    LOGGER.log(Level.SEVERE, "Cleaner image creation failed");
                }
            }
        } catch (DockerException | InterruptedException ex) {
            LOGGER.log(Level.SEVERE, "Cannot create cleaner image", ex);
        }

        Function<ExerciceContainer, Boolean> builderTask =
                                             (ec) -> Manager.buildLocalEnv(ec);
        try {
            BUILDER.setContainerID(DockerHelper.getContainers().stream().
                filter((c) -> BUILDER.getCotnainerName().equals(
                    c.names().get(0).substring(1)))
                .findFirst()
                .map((c) -> c.id())
                .orElse(null)
            );
            if (BUILDER.getContainerID() == null) {
                LOGGER.log(Level.INFO, "Create builder container");
                if (DockerHelper.buildBuilderContainer(BUILDER) == null) {
                    LOGGER.log(Level.SEVERE, "Builder container build failed");
                }
            }
            DockerHelper.startContainer(BUILDER.getContainerID(), true);
            builderTask = (ExerciceContainer ec) -> BUILDER.builEnv(ec);
        } catch (DockerException | InterruptedException ex) {
            LOGGER.log(Level.SEVERE, "Cannot use builder container", ex);
        }
        BUILDER_TASK = builderTask;
    }

    /**
     * Initialize manager.
     */
    public static void init() {
        Thread.currentThread().setName("titan-manager");
        try {
            LOGGER.log(Level.FINE, DockerHelper.getDockerInfo().toString());
        } catch (DockerException | InterruptedException ex) {
            Logger.getLogger(Manager.class.getName()).log(Level.SEVERE,
                                                          "Cannot get docker info",
                                                          ex);
            System.exit(1);
        }
        MONITOR.setDaemon(true);
        MONITOR.setName("mgr-monitor");
        MONITOR.start();
    }

    /**
     * Return the ID of the builder container.
     *
     * @return {@link String} - The builder container docker ID.
     */
    static String getContainerBuilderID() {
        return BUILDER.getContainerID();
    }

    /**
     * Set if debug mod is enabled.
     */
    public static boolean DEBUG = Boolean.valueOf(EnvHelper.getEnv(
                          EnvConstants.VERBOSE, "false"));

    /**
     * Process to the container execution then retrive results without callback
     * and do not preserve environment.
     *
     * @param team {@link String}: The team name.
     * @param exercice {@link String}: The exercice name.
     * @param language {@link String}: The language to use.
     * @param codePath {@link String}: The base directory path to take code.
     *
     * @return {@link Integer int} - <code>0</code> if the process succeed,
     * <code>1</code> if there were some failures.
     *
     * @see #process(java.lang.String, java.lang.String, java.lang.String,
     * java.lang.String, boolean, java.util.function.Consumer)
     */
    public static int process(String team, String exercice, String language,
                              String codePath) {
        return process(team, exercice, language, codePath, false, null);
    }

    /**
     * Process to the container execution then retrive results without callback
     * method.
     *
     * @param team {@link String}: The team name.
     * @param exercice {@link String}: The exercice name.
     * @param language {@link String}: The language to use.
     * @param codePath {@link String}: The base directory path to take code.
     * @param preserveEnv {@link Boolean boolean}: Set if the environment
     * volumes must be preserved or not (then remove).
     *
     * @return {@link Integer int} - <code>0</code> if the process succeed,
     * <code>1</code> if there were some failures.
     *
     * @see #process(java.lang.String, java.lang.String, java.lang.String,
     * java.lang.String, boolean, java.util.function.Consumer)
     */
    public static int process(String team, String exercice, String language,
                              String codePath, boolean preserveEnv) {
        return process(team, exercice, language, codePath, preserveEnv, null);
    }

    /**
     * Process to the container execution then retrive results and do not
     * preserve environment.
     *
     * @param team {@link String}: The team name.
     * @param exercice {@link String}: The exercice name.
     * @param language {@link String}: The language to use.
     * @param codePath {@link String}: The base directory path to take code.
     * @param callBack
     * {@link Consumer}&lt;{@link List}&lt;{@link ResolverResult}&gt;&gt;: The
     * callback method to call on te results once the container has been
     * executed.
     *
     * @return {@link Integer int} - <code>0</code> if the process succeed,
     * <code>1</code> if there were some failures.
     *
     * @see #process(java.lang.String, java.lang.String, java.lang.String,
     * java.lang.String, boolean, java.util.function.Consumer)
     */
    public static int process(String team, String exercice, String language,
                              String codePath,
                              Consumer<List<ResolverResult>> callBack) {
        return process(team, exercice, language, codePath, false, callBack);
    }

    /**
     * Process to the container execution then retrive results.
     *
     * @param team {@link String}: The team name.
     * @param exercice {@link String}: The exercice name.
     * @param language {@link String}: The language to use.
     * @param codePath {@link String}: The base directory path to take code.
     * @param preserveEnv {@link Boolean boolean}: Set if the environment
     * volumes must be preserved or not (then remove).
     * @param callBack
     * {@link Consumer}&lt;{@link List}&lt;{@link ResolverResult}&gt;&gt;: The
     * callback method to call on te results once the container has been
     * executed.
     *
     * @return {@link Integer int} - <code>0</code> if the process succeed,
     * <code>1</code> if there were some failures.
     */
    public static int process(String team, String exercice, String language,
                              String codePath, boolean preserveEnv,
                              Consumer<List<ResolverResult>> callBack) {
        int retCode = 0;
        /*
         * Initialize the exercice container.
         */
        ExerciceContainer container = Manager.newExerciceContainer(team,
                                                                   exercice,
                                                                   language,
                                                                   codePath);
        List<ResolverResult> results = null;
        if (container != null) {
            try {
                /*
                 * Execute cotnainer.
                 */
                results = Manager.submitContainer(container);
                if (!preserveEnv) {
                    /*
                     * Clean container.
                     */
                    Manager.cleanContainer(container);
                }
                LOGGER.log(Level.FINE, String.format("Result: %s",
                                                     Arrays.toString(results.
                                                         toArray())));
            } catch (InterruptedException | ExecutionException ex) {
                LOGGER.log(Level.SEVERE, "Execution failed", ex);
                retCode = 1;
            }
            if (callBack != null) {
                /*
                 * Execute callback method on results.
                 */
                callBack.accept(results);
            }
        }
        return retCode;
    }

    /**
     * Return the list of docker images names.
     *
     * @return {@link List}&lt;{@link String}&gt; - The images name list.
     */
    public static List<String> dockerGetImages() {
        List<String> images = new ArrayList<>();
        try {
            images = DockerHelper.getImages().stream()
            .map(i -> DockerHelper.getImageName(i))
            .collect(Collectors.toList());
        } catch (DockerException | InterruptedException ex) {
            Logger.getLogger(Manager.class.getName()).log(Level.SEVERE,
                                                          "Cannot get Images name",
                                                          ex);
        }
        return images;
    }

    /**
     * Check if a docker images exists.
     *
     * @param imageName {@link String}: The image name to check.
     *
     * @return {@link Boolean boolean} - <code>true</code> if the image exists,
     * <code>false</code> otherwise.
     */
    public static boolean dockerImageExists(String imageName) {
        return dockerGetImages().contains(imageName.toLowerCase());
    }

    /**
     * Return all requied images for the manager.
     *
     * @return {@link List}&lt;{@link String}&gt; - The list of expected images.
     */
    public static List<String> dockerExpectedImages() {
        return LANGUAGES.stream().map(l -> DockerHelper.getImageName(l))
            .collect(Collectors.toList());
    }

    /**
     * Construct a docker image from image name using language name as context
     * path.
     *
     * @param imageName {@link String}: The image name to build.
     *
     * @return {@link Boolean boolean} - <code>true</code> if the image has been
     * created, <code>false</code> otherwise.
     */
    public static boolean dockerBuildImage(String imageName) {
        /*
         * Retrieve the directory containg images.
         */
        File basePath = new File(Manager.IMAGES_PATH);
        if (!basePath.exists()) {
            URL url = Manager.class.getClassLoader().getResource("");
            if (url != null) {
                basePath = new File(url.getPath().concat(Manager.IMAGES_PATH));
                if (!basePath.exists()) {
                    basePath = new File(url.getPath().concat("../").concat(
                    Manager.IMAGES_PATH));
                }
            }
        }
        /*
         * The directory containg expected language image.
         */
        String imagePath = String.format("%s/%s", basePath,
                                         imageName.substring(imageName.
                                             lastIndexOf("/")));
        LOGGER.log(Level.INFO,
                   String.format("Trying to build %s -> %s\n", imagePath,
                                 imageName));
        String imageId = null;
        try {
            /*
             * Create the docker image using language image context.
             */
            imageId = DockerHelper.createImage(imagePath, imageName);
            LOGGER.log(Level.INFO,
                       String.format("Image [%s] built-> ID = %s\n", imageName,
                                     imageId));
        } catch (DockerException | InterruptedException | IOException ex) {
            LOGGER.log(Level.SEVERE, "Cannot build Image", ex);
        }
        return imageId != null && !imageId.isEmpty();
    }

    /**
     * Create environment container volumes with docker builder container.
     *
     * @param container {@link ExerciceContainer}: The container for wich to
     * create an environment.
     *
     * @return {@link Boolean boolean} - <code>true</code> if the environement
     * has been created successfully, <code>false</code> otherwise.
     */
    static boolean buildEnv(ExerciceContainer container) {
        return BUILDER_TASK.apply(container);
    }

    /**
     * Construct all required folders and files to use for container execution.
     *
     * @param container {@link ExerciceContainer}: The container for wich to
     * create an environment.
     *
     * @return {@link Boolean boolean} - <code>true</code> if the environement
     * has been created successfully, <code>false</code> otherwise.
     */
    static boolean buildLocalEnv(ExerciceContainer container) {
        /*
         * Create all require directory for volumes.
         */
        boolean result = container.getVolumes().entrySet().stream()
                .map(e -> new File(e.getValue()).toPath())
                .map((p) -> {
                    boolean created = true;
                    try {
                        Files.createDirectories(p);
                    } catch (IOException ex) {
                        LOGGER.log(
                            Level.SEVERE, String.format(
                                "Cannot create directory '%s'", p.getFileName()),
                            ex);
                        created = false;
                    }
                    return created;
                }).allMatch((r) -> r);

        File targetCodePath = new File(container.getVolumes().get(
             DockerHelper.ENV_CODE_PATH));
        File targetInputPath = new File(container.getVolumes().get(
             DockerHelper.ENV_INPUTS_PATH));

        if (result) {
            try {
                /*
                 * Copy all code files into container volume.
                 */
                result &= copyFiles(container.getBaseDir().toPath(),
                                    targetCodePath.toPath());
            } catch (IOException ex) {
                LOGGER.log(
                    Level.SEVERE, String.format(
                        "Cannot copy files to directory '%s'",
                        targetCodePath.getAbsolutePath()), ex);
                result = false;
            }
        }
        if (result) {
            try {
                /*
                 * Copy all inputs files into container volume.
                 */
                result &= copyFiles(new File(container.getIo().get(
                DockerHelper.ATTR_INPUTS)).toPath(),
                                    targetInputPath.toPath());
            } catch (IOException ex) {
                LOGGER.log(
                    Level.SEVERE, String.format(
                        "Cannot copy files to directory '%s'",
                        targetInputPath.getAbsolutePath()), ex);
                result = false;
            }
        }

        return result;
    }

    /**
     * Construct a new {@link ExerciceContainer} for a submission.
     *
     * @param team {@link String}: The team name.
     * @param exercice {@link String}: The exercice name.
     * @param language {@link String}: The language to use.
     * @param codePath {@link String}: The base directory path to take code
     * files.
     *
     * @return {@link ExerciceContainer} - The built exercice container.
     */
    static final ExerciceContainer newExerciceContainer(String team,
                                                        String exercice,
                                                        String language,
                                                        String codePath) {

        File baseDir = new File(codePath);
        ExerciceContainer container = null;

        /*
         * Check if the code path exists.
         */
        if (!baseDir.exists() || !baseDir.isDirectory()) {
            LOGGER.log(Level.SEVERE, String.format(
                       "Base dir '%s' cannot be used", baseDir.getAbsolutePath()));
        } else {
            container = new ExerciceContainer(team, exercice, language,
                                              baseDir);
        }

        return container;
    }

    /**
     * Build the docker container from exercice container.
     *
     * @param container {@link ExerciceContainer}: The exercice container to
     * create docker container.
     *
     * @return {@link String} - The docker container ID.
     */
    static String buildContainer(ExerciceContainer container) {
        LOGGER.log(Level.FINE, String.format("Building container: %s",
                                             container));

        String containerId = null;
        try {
            containerId = DockerHelper.buildContainer(container);
        } catch (DockerException | InterruptedException ex) {
            LOGGER.log(Level.SEVERE, "Cannot build container", ex);
        }

        return containerId;
    }

    /**
     * Build the docker container from cleaner container.
     *
     * @param container {@link CleanerContainer}: The cleaner container to
     * create docker container.
     *
     * @return {@link String} - The docker container ID.
     */
    static String buildCleanerContainer(CleanerContainer container) {
        LOGGER.log(Level.FINE, String.format("Building cleaner container: %s",
                                             container));

        String containerId = null;
        try {
            containerId = DockerHelper.buildCleanerContainer(container);
        } catch (DockerException | InterruptedException ex) {
            LOGGER.log(Level.SEVERE, "Cannot build cleaner container", ex);
        }

        return containerId;
    }

    /**
     * Start a docker container.
     *
     * @param containerID {@link String}: The ID of the container to start.
     *
     * @return {@link Boolean boolean} - <code>true</code> if the container has
     * been started successfully, <code>false</code> otherwise.
     */
    public static boolean startContainer(String containerID) {
        LOGGER.log(Level.FINE, String.format("Starting container: %s",
                                             containerID));
        boolean started = false;
        try {
            DockerHelper.startContainer(containerID);
            started = true;
        } catch (DockerException | InterruptedException ex) {
            LOGGER.log(Level.SEVERE, String.format("Cannot start container %s",
                                                   containerID), ex);
        }
        return started;
    }

    /**
     * Stop a docker container.
     *
     * @param containerID {@link String}: The ID of the container to stop.
     *
     * @return {@link Boolean boolean} - <code>true</code> if the container has
     * been stopped successfully, <code>false</code> otherwise.
     */
    public static boolean stopContainer(String containerID) {
        LOGGER.log(Level.FINE, String.format("Stopping container: %s",
                                             containerID));
        boolean stopped = false;
        try {
            DockerHelper.stopContainer(containerID);
            stopped = true;
        } catch (DockerException | InterruptedException ex) {
            LOGGER.log(Level.SEVERE, String.format("Cannot start container %s",
                                                   containerID), ex);
        }
        return stopped;
    }

    /**
     * Remove local volumes directory used for a container on host.
     *
     * @param container {@link ExerciceContainer}: The container to remove
     * volumes directories.
     *
     * @return {@link Boolean boolean} - <code>true</code> if the volumes have
     * been successfully removed, <code>false</code> otherwise.
     */
    static boolean cleanLocalEnv(ExerciceContainer container) {
        LOGGER.log(Level.FINE, String.format("Cleaning env container: %s",
                                             container));
        boolean cleaned = container.getVolumes().entrySet().stream()
                .map((e) -> {
                    boolean removed;
                    try {
                        /*
                     * Remove files first then folders.
                         */
                        removed = Files.walk(new File(e.getValue()).toPath())
                        .sorted((p1, p2) -> -p1.compareTo(p2))
                        .map(Path::toFile).map(f -> f.delete())
                        .allMatch((r) -> r);
                    } catch (IOException ex) {
                        LOGGER.log(Level.SEVERE, "Cannot delete file", ex);
                        removed = false;
                    }
                    return removed;
                }).allMatch((r) -> r);

        /*
         * Then remove parent folder.
         */
        String rootPath = DockerHelper.getTemporaryFolder(
               container.getTeam(), container.getExercice(), container.getName()
           );
        File rootFile = new File(rootPath);
        if (!rootFile.exists()) {
            rootFile = new File(Manager.class.getClassLoader().getResource("")
            .getPath().concat(rootPath));
        }
        cleaned &= rootFile.delete();
        return cleaned;
    }

    /**
     * Remove a docker container.
     *
     * @param containerID {@link String}: The ID of the container to remove.
     *
     * @return {@link Boolean boolean} - <code>true</code> if the container has
     * been successfully removed, <code>false</code> otherwise.
     */
    public static boolean removeContainer(String containerID) {
        boolean started = false;
        try {
            DockerHelper.removeContainer(containerID);
            started = true;
        } catch (DockerException | InterruptedException ex) {
            LOGGER.log(Level.SEVERE, String.format("Cannot start container %s",
                                                   containerID), ex);
        }
        return started;
    }

    /**
     * Submit an {@link ExerciceContainer} submission to docker client.
     *
     * @param container {@link ExerciceContainer}: The exercice container to
     * run.
     *
     * @return {@link List}&lt;{@link ResolverResult}]&gt; - The list of results
     * get from container.
     *
     * @throws ExecutionException If the docker client submission failed.
     * @throws InterruptedException If the process has been stopped.
     */
    private static List<ResolverResult> submitContainer(
        ExerciceContainer container) throws InterruptedException,
                                            ExecutionException {
        LOGGER.log(Level.FINE, String.format("Submitting container: %s",
                                             container));

        ContainerRunnerTask runner = new ContainerRunnerTask(container);
        /*
         * Allow to terminate if no task is running.
         */
        Future<List<ResolverResult>> value = RUNNER.submit(runner);

        List<ResolverResult> result = value.get();
        LOGGER.log(Level.FINE, String.format("Container executed: %s",
                                             container));
        return result;
    }

    /**
     * Submit the container cleaning for container and its volumes.
     *
     * @param container {@link ExerciceContainer}: The exercice container to
     * clean.
     */
    static void cleanContainer(ExerciceContainer container) {
        ContainerCleanerTask cleaner = new ContainerCleanerTask(container);
        /*
             * Allow to terminate if no task is running.
         */
        CLEANER.submit(cleaner);
        LOGGER.log(Level.FINE, String.format(
                   "Container clean submitting: %s", container));
    }

    /**
     * Return the list of available languages of the manager.
     *
     * @return {@link List}&lt;{@link String}&gt; - The list of languages.
     */
    public static List<String> availableLanguages() {
        return LANGUAGES.stream()
            .filter(l -> !"template".equalsIgnoreCase(l) &&
                           !"cleaner".equalsIgnoreCase(l) &&
                           !"builder".equalsIgnoreCase(l))
            .collect(Collectors.toList());
    }

    /**
     * Get container name based on team, the and specific suffix.
     *
     * @param team {@link String}: The team name.
     * @param exercice {@link String}: The exercice name.
     * @param suffix {@link String}: The container name suffix. If the suffix is
     * <code>null</code> then create a random suffix.
     *
     * @return {@link String} - The container name.
     */
    public static String contructName(String team, String exercice,
                                      String suffix) {
        return String.format("%s_%s_%s", team, exercice,
                             suffix == null ? UUID.randomUUID().toString().
                             replaceAll("-", "") :
                             suffix);
    }

    /**
     * Get container name based on team and exercice.
     *
     * @param team {@link String}: The team name.
     * @param exercice {@link String}: The exercice name.
     *
     * @return {@link String} - The container name.
     */
    public static String contructName(String team, String exercice) {
        return contructName(team, exercice, null);
    }

    /**
     * Copy files recursivly from a path to another.
     *
     * @param source {@link Path}: The source path to get files.
     * @param target {@link Path}: The target path to copy files.
     *
     * @return {@link Boolean boolean} - <code>true</code> if the files have
     * copied, <code>false</code> otherwise.
     *
     * @throws IOException If a copy failed.
     */
    private static boolean copyFiles(Path source, Path target)
        throws IOException {

        return Files.walk(source).filter((p) -> p.toFile().isFile())
            .map((p) -> {
                boolean copied = true;
                try {
                    String targetFileName = p.toFile().getAbsolutePath()
                           .replaceAll("^(.*)/?".concat(source.toFile()
                               .getAbsolutePath().replaceAll("\\\\", "\\\\\\\\")).
                               concat("/?"), "");
                    File targetFile = new File(String.format("%s/%s",
                                                             target.toFile().
                                                             getAbsolutePath(),
                                                             targetFileName));
                    Files.createDirectories(targetFile.getParentFile()
                        .toPath());
                    Files.copy(p, targetFile.toPath(),
                               StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException ex) {
                    LOGGER.log(
                        Level.SEVERE, String.format(
                            "Cannot copy file '%s' to '%s'",
                            p.toFile().getAbsolutePath(),
                            target.toFile().getAbsolutePath()), ex);
                    copied = false;
                }
                return copied;
            }).allMatch((r) -> r);

    }

    /**
     * Stop the manager.
     *
     * @throws InterruptedException If a thread process has been interrupted.
     */
    public static void stopProcesses() throws InterruptedException {
        LOGGER.log(Level.INFO, "Manager is shutting down...");

        while (RUNNER.getTaskCount() > RUNNER.getCompletedTaskCount()) {
            RUNNER.awaitTermination(100, TimeUnit.MILLISECONDS);
        }
        RUNNER.setKeepAliveTime(1L, TimeUnit.NANOSECONDS);
        RUNNER.shutdownNow();
        RUNNER.shutdown();
        LOGGER.log(Level.INFO, "Runner tasks terminated");

        /*
         * Wait for tasks init.
         */
        Thread.sleep(1000);
        while (CLEANER.getTaskCount() > CLEANER.getCompletedTaskCount()) {
            RUNNER.awaitTermination(100, TimeUnit.MILLISECONDS);
        }
        CLEANER.setKeepAliveTime(1L, TimeUnit.NANOSECONDS);
        CLEANER.shutdownNow();
        CLEANER.shutdown();
        LOGGER.log(Level.INFO, "Cleaner tasks terminated");

        MONITOR.interrupt();
        LOGGER.log(Level.INFO, "Manager has been shut down");
    }
}
