package fr.nemolovich.apps.codeplatform.manager.docker;

import java.util.Collections;

/**
 * A model that represents a docker cleaner container used to clean an execution
 * environment.
 *
 * @author bgohier
 */
class CleanerContainer {

    public static final String CLEANER_NAME_PREFIX = "cleaner_";

    private String containerID;
    private final String cotnainerName;
    private final String containerVolume;
    private final String hostVolume;

    /**
     * Create cleaner container.
     *
     * @param cotnainerName {@link String}: The name of the container to clean.
     * @param containerVolume {@link String}: The volume path in the container.
     * @param hostVolume {@link String}: The volume path on the host.
     */
    CleanerContainer(String cotnainerName, String containerVolume,
        String hostVolume) {
        this.cotnainerName = cotnainerName;
        this.containerVolume = containerVolume;
        this.hostVolume = hostVolume;
    }

    /**
     * The docker container ID.
     *
     * @return {@link String} - The docker container ID.
     */
    String getContainerID() {
        return containerID;
    }

    /**
     * Set the docker container ID.
     *
     * @param containerID {@link String}: The docker container ID.
     */
    void setContainerID(String containerID) {
        this.containerID = containerID;
    }

    /**
     * Return the name of the container to clean.
     *
     * @return {@link String} - The container name.
     */
    public String getCotnainerName() {
        return cotnainerName;
    }

    /**
     * Return the name of the cleaner container.
     *
     * @return {@link String} - The cleaner container name.
     */
    public String getCleanerCotnainerName() {
        return CLEANER_NAME_PREFIX.concat(cotnainerName);
    }

    /**
     * Return the name of the container volume.
     *
     * @return {@link String} - The container volume.
     */
    public String getContainerVolume() {
        return containerVolume;
    }

    /**
     * Return the name of the host volume.
     *
     * @return {@link String} - The host volume.
     */
    public String getHostVolume() {
        return hostVolume;
    }

    @Override
    public String toString() {
        return String.format("%s:" + String.join("", Collections.nCopies(4,
            "\n\t%-20s: %s")),
            CleanerContainer.class.getSimpleName(),
            "containerName", this.cotnainerName,
            "cleanerContainerName", this.getCleanerCotnainerName(),
            "containerVolume", this.containerVolume,
            "hostVolume", this.hostVolume,
            "containerID", this.containerID
        );
    }

}
