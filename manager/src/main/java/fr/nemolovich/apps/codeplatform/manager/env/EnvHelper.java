package fr.nemolovich.apps.codeplatform.manager.env;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * This class is used to load an environment variables and properties from
 * properties file "manager.properties".
 *
 * @author bgohier
 */
public final class EnvHelper {

    private static final Properties PROP = new Properties();
    private static final String PROP_FILE_PATH = "manager.config.path";
    private static final String LANGUAGES_FILE_PATH = "languages.config.path";

    private EnvHelper() {
        /*
         * Utility class.
         */
    }

    static {

        PROP.putAll(System.getenv());
        PROP.putAll(System.getProperties());
        
        File f = null;
        if (System.getProperty(PROP_FILE_PATH) != null) {
            f = new File(System.getProperty(PROP_FILE_PATH));
        }
        InputStream is = EnvHelper.class.getResourceAsStream(
                    "/manager.properties");
        if (f != null && f.exists()) {
            try {
                is = new FileInputStream(f);
            } catch (IOException ex) {
                Logger.getLogger(EnvHelper.class.getName()).log(
                    Level.SEVERE, "Cannot load properties file in parameter",
                    ex);
            }
        }

        if (is != null) {
            try {
                Properties prop = new Properties();
                prop.load(is);
                PROP.putAll(prop);
                Logger.getLogger(EnvHelper.class.getName()).log(
                    Level.FINE, String.format("Config loaded: %s",
                                              PROP.toString()));
                PROP.entrySet().stream().forEach((e) -> System.setProperty(
                    e.getKey().toString(), e.getValue().toString()));
                is.close();
            } catch (IOException ex) {
                Logger.getLogger(EnvHelper.class.getName()).log(
                    Level.SEVERE, "Cannot load properties file", ex);
            }
        }
    }

    /**
     * The default array elements separator.
     */
    public static final String DEFAULT_ARRAY_SEPARATOR = ",";

    /**
     * Get a {@link String string} environment variable value or default value
     * if not found.
     *
     * @param key {@link String}: The environment variable name.
     * @param defaultValue {@link String}: The default value to return if the
     * variable has not been found.
     *
     * @return {@link String} - The environment variable value.
     */
    public static String getEnv(String key, String defaultValue) {
        return PROP.getProperty(key) != null ? PROP.getProperty(key) :
               defaultValue;
    }

    /**
     * Get a {@link String} environment variable value or <code>null</code> if
     * not found.
     *
     * @param key {@link String}: The environment variable name.
     *
     * @return {@link String} - The environment variable value.
     */
    public static String getEnv(String key) {
        return getEnv(key, null);
    }

    /**
     * Get an {@link Integer integer} environment variable value or default
     * value if not found.
     *
     * @param key {@link String}: The environment variable name.
     * @param defaultValue {@link Integer}: The default value to return if the
     * variable has not been found.
     *
     * @return {@link Integer} - The environment variable value.
     */
    public static Integer getIntegerEnv(String key, Integer defaultValue) {
        return PROP.getProperty(key) != null ? Integer.valueOf(
               PROP.getProperty(key)) : defaultValue;
    }

    /**
     * Get an {@link Integer integer} environment variable value or
     * <code>null</code> if not found.
     *
     * @param key {@link String}: The environment variable name.
     *
     * @return {@link Integer} - The environment variable value.
     */
    public static Integer getIntegerEnv(String key) {
        return getIntegerEnv(key, null);
    }

    /**
     * Get a {@link Long long} environment variable value or default value if
     * not found.
     *
     * @param key {@link String}: The environment variable name.
     * @param defaultValue {@link Long}: The default value to return if the
     * variable has not been found.
     *
     * @return {@link Long} - The environment variable value.
     */
    public static Long getLongEnv(String key, Long defaultValue) {
        return PROP.getProperty(key) != null ? Long.valueOf(
               PROP.getProperty(key)) : defaultValue;
    }

    /**
     * Get a {@link Long long} environment variable value or <code>null</code>
     * if not found.
     *
     * @param key {@link String}: The environment variable name.
     *
     * @return {@link Long} - The environment variable value.
     */
    public static Long getLongEnv(String key) {
        return getLongEnv(key, null);
    }

    /**
     * Get a {@link List list} environment variable values or default values if
     * not found.
     *
     * @param key {@link String}: The environment variable name.
     * @param separator {@link String}: The array separator to use to split
     * values.
     * @param defaultValues {@link List}&lt;{@link String}&gt;: The default
     * values to return if the variable has not been found.
     *
     * @return {@link List}&lt;{@link String}&gt; - The environment variable
     * values.
     */
    public static List<String> getArrayEnv(String key, String separator,
                                           List<String> defaultValues) {
        String value = getEnv(key);
        List<String> values = defaultValues;
        if (value != null) {
            values = Arrays.asList(value.split(separator));
        }
        return values;
    }

    /**
     * Get a {@link List list} environment variable values or <code>null</code>
     * if not found. This use {@link #DEFAULT_ARRAY_SEPARATOR} as elements
     * separator.
     *
     * @param key {@link String}: The environment variable name.
     *
     * @return {@link List}&lt;{@link String}&gt; - The environment variable
     * values.
     */
    public static List<String> getArrayEnv(String key) {
        return getArrayEnv(key, DEFAULT_ARRAY_SEPARATOR, (List) null);
    }

    /**
     * Get a {@link List list} environment variable values or <code>null</code>
     * if not found.
     *
     * @param key {@link String}: The environment variable name.
     * @param separator {@link String}: The array separator to use to split
     * values.
     *
     * @return {@link List}&lt;{@link String}&gt; - The environment variable
     * values.
     */
    public static List<String> getArrayEnv(String key, String separator) {
        return getArrayEnv(key, separator, (List) null);
    }

    /**
     * Get a {@link List list} environment variable values or default values if
     * not found. This use {@link #DEFAULT_ARRAY_SEPARATOR} as elements
     * separator.
     *
     * @param key {@link String}: The environment variable name.
     * @param defaultValues {@link List}&lt;{@link String}&gt;: The default
     * values to return if the variable has not been found.
     *
     * @return {@link List}&lt;{@link String}&gt; - The environment variable
     * values.
     */
    public static List<String> getArrayEnv(String key,
                                           List<String> defaultValues) {
        return getArrayEnv(key, DEFAULT_ARRAY_SEPARATOR, defaultValues);
    }

    /**
     * Get a {@link List list} environment variable values or default values if
     * not found.
     *
     * @param key {@link String}: The environment variable name.
     * @param separator {@link String}: The array separator to use to split
     * values.
     * @param defaultValues {@link String}[]: The default values to return if
     * the variable has not been found.
     *
     * @return {@link List}&lt;{@link String}&gt; - The environment variable
     * values.
     */
    public static List<String> getArrayEnv(String key, String separator,
                                           String[] defaultValues) {
        return getArrayEnv(key, separator,
                           Arrays.asList(defaultValues));
    }

    /**
     * Get a {@link List list} environment variable values or default values if
     * not found. This use {@link #DEFAULT_ARRAY_SEPARATOR} as elements
     * separator.
     *
     * @param key {@link String}: The environment variable name.
     * @param defaultValues {@link String}[]: The default values to return if
     * the variable has not been found.
     *
     * @return {@link List}&lt;{@link String}&gt; - The environment variable
     * values.
     */
    public static List<String> getArrayEnv(String key, String[] defaultValues) {
        return getArrayEnv(key, DEFAULT_ARRAY_SEPARATOR, defaultValues);
    }

    /**
     * Return all values of loaded properties as {@link String}.
     *
     * @return {@link Map}&lt;{@link String},{@link String}&gt; - The variables
     * names and their values.
     */
    static Map<String, String> getProperties() {
        return PROP.entrySet().stream().collect(Collectors.toMap(
            e -> String.valueOf(e.getKey()),
            e -> String.valueOf(e.getValue())));
    }

    /**
     * Return the stream of the languages properties file. Take the system
     * parameter file given by {@link #LANGUAGES_FILE_PATH} if found, the
     * packaged file otherwise.
     *
     * @return {@link InputStream}: YAML file stream.
     */
    public static InputStream getLanguagesConfig() {

        File f = null;
        if (System.getProperty(LANGUAGES_FILE_PATH) != null) {
            f = new File(System.getProperty(LANGUAGES_FILE_PATH));
        }
        InputStream is = EnvHelper.class.getResourceAsStream(
                    "/languages.yml");
        if (f != null && f.exists()) {
            try {
                is = new FileInputStream(f);
            } catch (IOException ex) {
                Logger.getLogger(EnvHelper.class.getName()).log(
                    Level.SEVERE, "Cannot load languages file in parameter",
                    ex);
            }
        }

        return is;
    }
}
