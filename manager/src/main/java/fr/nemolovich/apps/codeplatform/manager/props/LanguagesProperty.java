package fr.nemolovich.apps.codeplatform.manager.props;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The model of list of languages properties.
 *
 * @author bgohier
 */
public class LanguagesProperty {

    private LanguageProperty[] languages;

    /**
     * Return the list of languages properties.
     *
     * @return {@link LanguageProperty}[] - The list of languages properties.
     */
    public LanguageProperty[] getLanguages() {
        return this.languages;
    }

    /**
     * Set the list of languages properties.
     *
     * @param languages {@link LanguageProperty}[] - The list of languages
     * properties.
     */
    public void setLanguages(LanguageProperty[] languages) {
        this.languages = languages;
    }

    /**
     * Return the list of languages properties sorted by priority.
     *
     * @return {@link LanguageProperty}[] - The list of languages properties.
     */
    public List<String> getOrderedLanguages() {
        return Arrays.asList(this.languages).stream()
            .collect(Collectors.toConcurrentMap(lp -> lp.getLanguage(),
                lp -> lp.getPriority())).entrySet().stream().sorted(
                (a, b) -> a.getValue().equals(b.getValue())
                ? a.getKey().compareTo(b.getKey())
                : a.getValue() - b.getValue())
            .map(lp -> lp.getKey()).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "LanguagesProperty{" + "languages="
            + Arrays.toString(this.getOrderedLanguages().toArray()) + '}';
    }
}
