package fr.nemolovich.apps.codeplatform.manager.env;

/**
 * Environment valiables names.
 *
 * @author bgohier
 */
public final class EnvConstants {

    private EnvConstants() {
        /*
         * Utility class.
         */
    }

    /**
     * Docker host uri.
     */
    public static final String DOCKER_HOST = "DOCKER_HOST";
    /**
     * Docker certificates path.
     */
    public static final String DOCKER_CERT_PATH = "DOCKER_CERT_PATH";

    /**
     * The path to find the manager.
     */
    public static final String MANAGER_PATH = "MANAGER_PATH";
    /**
     * The path to find all directories to create language images.
     */
    public static final String IMAGES_PATH = "IMAGES_PATH";
    /**
     * The name of the manager.
     */
    public static final String MANAGER_NAME = "MANAGER_NAME";
    /**
     * The version of the manager.
     */
    public static final String MANAGER_VERSION = "MANAGER_VERSION";

    /**
     * The container base path for volumes.
     */
    public static final String ENV_BASE_PATH = "ENV_BASE_PATH";
    /**
     * The path of the code folder.
     */
    public static final String ENV_CODE_PATH = "ENV_CODE_PATH";
    /**
     * The path of the logs folder.
     */
    public static final String ENV_LOGS_PATH = "ENV_LOGS_PATH";
    /**
     * The path of the inputs folder.
     */
    public static final String ENV_INPUTS_PATH = "ENV_INPUTS_PATH";
    /**
     * The path of the outputs folder.
     */
    public static final String ENV_OUTPUTS_PATH = "ENV_OUTPUTS_PATH";
    /**
     * The path of the config folder.
     */
    public static final String ENV_CONFIG_PATH = "ENV_CONFIG_PATH";

    /**
     * The name of the inputs volume folder.
     */
    public static final String VOLUME_INPUTS = "VOLUME_INPUTS";
    /**
     * The name of the outputs volume folder.
     */
    public static final String VOLUME_OUTPUTS = "VOLUME_OUTPUTS";
    /**
     * The name of the code volume folder.
     */
    public static final String VOLUME_CODE = "VOLUME_CODE";
    /**
     * The name of the logs volume folder.
     */
    public static final String VOLUME_LOGS = "VOLUME_LOGS";
    /**
     * The name of the config volume folder.
     */
    public static final String VOLUME_CONFIG = "VOLUME_CONFIG";
    /**
     * The name of the resolver volume folder.
     */
    public static final String VOLUME_RESOLVER = "VOLUME_RESOLVER";

    /**
     * The default resolver folder name.
     */
    public static final String DEFAULT_RESOLVER_NAME = "DEFAULT_RESOLVER_NAME";

    /**
     * The host base path that contains exercices.
     */
    public static final String EXERCICES_PATH = "EXERCICES_PATH";
    /**
     * The docker host server base path that contains exercices if running
     * inside a container.
     */
    public static String EXERCICES_OUTER_VOLUME = "EXERCICES_OUTER_VOLUME";

    /**
     * The prefix of the image to create for languages.
     */
    public static final String IMAGES_PREFIX = "IMAGES_PREFIX";
    /**
     * Set the manager verbosity (<code>"true"</code> of <code>"false"</code>).
     */
    public static final String VERBOSE = "MANAGER_VERBOSE";
    /**
     * The memory limit to use for each container in MB.
     */
    public static final String MEMORY_LIMIT = "MEMORY_LIMIT";

    /**
     * The maximum of simultaneous threads to execute by executor.
     */
    public static final String THREAD_MAX_SIZE = "THREAD_MAX_SIZE";
    /**
     * The minimum of simultaneous threads to execute by executor.
     */
    public static final String THREAD_CORE_SIZE = "THREAD_CORE_SIZE";
    /**
     * The maximum time to execute a thread in executor.
     */
    public static final String THREAD_TIMEOUT = "THREAD_TIMEOUT";
    /**
     * The maximum tasks queue capacity of executor.
     */
    public static final String MAX_QUEUE_SIZE = "MAX_QUEUE_SIZE";

}
