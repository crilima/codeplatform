package fr.nemolovich.apps.codeplatform.manager.docker;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import fr.nemolovich.apps.codeplatform.manager.resolver.ExerciceResolver;
import fr.nemolovich.apps.codeplatform.manager.resolver.ResolverResult;

/**
 * This {@link Callable} implementation is used to build all required
 * files/folders for the container execution. Then it create the container and
 * runs it. Once the container has been executed the {@link ExerciceResolver}
 * associated to the exercice will be run. The list of {@link ResolverResult}
 * will be return then.
 *
 * @author bgohier
 */
class ContainerRunnerTask implements Callable<List<ResolverResult>> {

    private final ExerciceContainer container;
    private static long nbThread = 0;

    /**
     * Default contructor using {@link ExerciceContainer}.
     *
     * @param container {@link ExerciceContainer}: The container to process.
     */
    ContainerRunnerTask(ExerciceContainer container) {
        this.container = container;
    }

    /**
     * Get the next thread number to use.
     *
     * @return {@link Long long} - Next thread number.
     */
    public static long getNextThreadNumber() {
        return nbThread++;
    }

    @Override
    public List<ResolverResult> call() throws Exception {
        List<ResolverResult> results = new ArrayList<>();
        if (Manager.buildEnv(this.container)) {
            /*
             * If The environnement has been successfully built then build the 
             * container.
             */
            String containerID = Manager.buildContainer(this.container);

            if (containerID != null && Manager.startContainer(containerID)) {

                /*
                     * Once container started then map all outputs and logs files
                     * from container to host volumes.
                 */
                Map<String, String> inputFiles = ExerciceResolver
                                    .getInputsMapping(container.getIo()
                                        .get(DockerHelper.ATTR_OUTPUTS),
                                                      String.format(
                                                          "%s/%s", container.
                                                          getIo().get(
                                                              DockerHelper.ATTR_TMP),
                                                          DockerHelper.VOLUME_INPUTS));
                Map<String, String> outputFiles = ExerciceResolver
                                    .getOutputsMapping(container.getIo().get(
                                        DockerHelper.ATTR_OUTPUTS),
                                                       String.format(
                                                           "%s/%s", container.
                                                           getIo().get(
                                                               DockerHelper.ATTR_TMP),
                                                           DockerHelper.VOLUME_OUTPUTS));
                Map<String, String> logs = ExerciceResolver
                                    .getLogsMapping(container.getIo().get(
                                        DockerHelper.ATTR_OUTPUTS),
                                                    String.format(
                                                        "%s/%s", container.
                                                        getIo().get(
                                                            DockerHelper.ATTR_TMP),
                                                        DockerHelper.VOLUME_LOGS));

                /*
                     * Resolve each of the outputs.
                 */
                results = outputFiles.entrySet().stream()
                .map((Entry<String, String> entry) ->
                    ExerciceResolver.resolve(
                        container.getIo().get(DockerHelper.ATTR_RESOLVER),
                        logs.get(entry.getKey()),
                        inputFiles.get(entry.getKey()),
                        entry.getKey(),
                        entry.getValue())
                ).collect(Collectors.toList());
            }
        }
        return results;
    }

}
