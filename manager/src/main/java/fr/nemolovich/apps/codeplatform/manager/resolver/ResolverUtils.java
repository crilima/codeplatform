package fr.nemolovich.apps.codeplatform.manager.resolver;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticListener;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import fr.nemolovich.apps.codeplatform.manager.docker.DockerHelper;

/**
 * Utility class used for resolver cache.
 *
 * @author bgohier
 */
public final class ResolverUtils {

    private ResolverUtils() {
        /*
         * Utility class.
         */
    }
    private static final Logger LOGGER = Logger.getLogger(
        ResolverUtils.class.getName());
    private static final Map<String, IResolver> RESOLVERS_CACHE
        = new ConcurrentHashMap<>();
    private static final DiagnosticListener DIAGNOSTIC_LISTENER
        = (DiagnosticListener) (Diagnostic diagnostic) -> {
            LOGGER.fine(String.format(
                "Line Number: %d\nCode: %s\nMessage: %s\nSource: %s\n",
                diagnostic.getLineNumber(),
                diagnostic.getCode(),
                diagnostic.getMessage(Locale.ENGLISH),
                diagnostic.getSource()));
        };

    /**
     * Return an implementation of the resolver from path.
     *
     * @param resolverPath {@link String}: The path of the resolver to compile.
     *
     * @return {@link IResolver} - An implementation of the resolver.
     */
    public static final IResolver getResolver(final String resolverPath) {

        IResolver resolver;

        File resolverFileFolder = new File(resolverPath).getAbsoluteFile();
        URL url;

        if (!resolverFileFolder.exists()) {
            File defaultResolverFolder = new File(DockerHelper.DEFAULT_RESOLVER)
                .getAbsoluteFile();
            if (!resolverFileFolder.getAbsolutePath().equals(
                defaultResolverFolder.getAbsolutePath())) {
                LOGGER.log(Level.WARNING, String.format(
                    "ERROR: Cannot find path '%s' => Trying with default resolver '%s'",
                    resolverFileFolder, DockerHelper.DEFAULT_RESOLVER));
                resolverFileFolder = defaultResolverFolder;
            } else {
                LOGGER.log(Level.SEVERE, String.format(
                    "ERROR: Cannot find default resolver from path '%s'",
                    resolverFileFolder));
                return null;
            }
        }

        try {
            url = resolverFileFolder.toURI().toURL();
        } catch (MalformedURLException ex) {
            LOGGER.log(Level.SEVERE, String.format(
                "ERROR: Cannot find path '%s'", resolverFileFolder), ex);
            return null;
        }

        if (RESOLVERS_CACHE.containsKey(resolverPath)) {
            /*
             * Use resolver cache for resolver class to use.
             */
            LOGGER.fine(String.format(
                "Using existing Resolver instance for '%s'", resolverPath));
            resolver = RESOLVERS_CACHE.get(resolverPath);
        } else {
            /*
             * Load java resolver class file.
             */
            LOGGER.log(Level.INFO, String.format("Loading Resolver from '%s'",
                resolverPath));

            /*
             * Check if the compiled class exists.
             */
            File resolverClass = new File(String.format("%s/Resolver.class",
                resolverFileFolder.getAbsolutePath()));
            if (!resolverClass.exists()) {
                /*
                 * Compile java file.
                 */
                compileResolver(resolverFileFolder);
            }

            /*
             * Using ExerciceResolver ClassLoader for inheritance.
             */
            URLClassLoader classLoader = new URLClassLoader(new URL[]{url},
                ExerciceResolver.class.getClassLoader());
            /*
             * Load resolver in ClassLoader.
             */
            Class<? extends IResolver> clazz;
            try {
                clazz = (Class<? extends IResolver>) classLoader
                    .loadClass("Resolver");
            } catch (ClassNotFoundException ex) {
                LOGGER.log(Level.SEVERE, String.format(
                    "ERROR: Cannot load Resolver from '%s'", resolverPath), ex);
                return null;
            }
            /*
             * Create a new instance of resolver.
             */
            try {
                LOGGER.log(Level.INFO, String.format(
                    "Create new instance of Resolver from '%s'", resolverPath));
                resolver = clazz.newInstance();
                /*
                 * Save resolver in cache if no error.
                 */
                RESOLVERS_CACHE.put(resolverPath, resolver);
            } catch (InstantiationException | IllegalAccessException ex) {
                LOGGER.log(Level.SEVERE, String.format(
                    "ERROR: Cannot instantiate Resolver from '%s'",
                    resolverPath), ex);
                return null;
            }
        }

        return resolver;
    }

    private static void compileResolver(File resolverParentFile) {

        /*
         * Look for source class.
         */
        File resolverFile = new File(String.format("%s/Resolver.java",
            resolverParentFile.getAbsolutePath()));
        if (!resolverFile.exists()) {
            LOGGER.log(Level.SEVERE, String.format(
                "ERROR: Cannot find path '%s'", resolverFile));
            return;
        }

        LOGGER.log(Level.INFO, String.format("Compiling file '%s'",
            resolverFile.getAbsolutePath()));
        /*
         * Get system compiler.
         */
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        StandardJavaFileManager fileManager
            = compiler.getStandardFileManager(null, null, null);

        /*
         * Adding classpath option.
         */
        List<String> options = new ArrayList<>();
        options.add("-cp");
        StringBuilder sb = new StringBuilder();
        /*
         * Append current classpath for class extends.
         */
        sb.append(System.getProperty("java.class.path"))
            .append(File.pathSeparatorChar)
            .append(resolverParentFile.getAbsolutePath());
        options.add(sb.toString());

        /*
                 * Adding resolver source file in compile list.
         */
        Iterable<? extends JavaFileObject> units
            = fileManager.getJavaFileObjectsFromFiles(
                Arrays.asList(resolverFile));

        /*
                 * Create compilation task.
         */
        JavaCompiler.CompilationTask task = compiler.getTask(null, fileManager,
            DIAGNOSTIC_LISTENER, options, null, units);

        /*
                 * Run compilation.
         */
        if (!task.call()) {
            LOGGER.warning(String.format("Cannot compile Resolver from '%s'",
                resolverFile.getAbsolutePath()));
        }
        try {
            fileManager.close();
        } catch (IOException ex) {
            Logger.getLogger(ResolverUtils.class.getName()).log(Level.FINE,
                "Cannot close file manager", ex);
        }
    }

}
